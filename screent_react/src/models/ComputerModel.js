import moment from 'moment'
import Model from './private/Model'


export default class ComputerModel extends Model{
    constructor(obj){
        super(obj)
        if (this.minDate && this.maxDate){
            this.minDate = moment.utc(this.minDate.format('YYYY-MM-DDTHH:mm:ss'))
            this.maxDate = moment.utc(this.maxDate.format('YYYY-MM-DDTHH:mm:ss'))    
        }
    }

    get pauseTitle(){
        if (this.isPause){
            return 'Remove pause'
        }
        return 'Set on pause'
    }
}



