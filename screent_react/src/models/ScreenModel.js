import moment from 'moment'
import Model from './private/Model'


export default class ScreenModel extends Model{
    constructor(obj){
        super(obj)
        this.createdAt = moment.utc(this.createdAt.format('YYYY-MM-DDTHH:mm:ss'))
    }

    get createdAtString(){
        return this.createdAt.format('YYYY-MM-DD HH:mm:ss')
    }
}
