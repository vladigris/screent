import Model from './private/Model'


export default class MessageModel extends Model{
    get createdAtString(){
        return this.createdAt.format('YYYY-MM-DD HH:mm:ss')
    }
}
