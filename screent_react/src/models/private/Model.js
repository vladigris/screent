import moment from 'moment'


export default class Model{
    constructor(obj){
        for (let key in obj){
            this.set(key, obj[key])
        }    
    }

    set(key, value){
        if (value === '0001-01-01T00:00:00Z'){
            value = null
        }

        if (this.isDate(value)){
            value = moment(value, 'YYYY-MM-DDTHH:mm:ss')
        }
        this[key] = value
    }

    isDate(value){
        var ptn = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/
        return ptn.test(value)
    }
}
