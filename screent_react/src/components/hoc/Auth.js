import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import { Redirect } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'




@inject('user')
@observer
export default class Auth extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillMount() {
        this.props.user.checkToken()
    }

    render(){
        const { user } = this.props
        
        if (user.isAuthenticated === null){
            return <Loading />
        }

        if (user.isAuthenticated === false){
            return <Redirect to="/sign-in" />
        }

        

        return {...this.props.children}
    }
}


Auth.propTypes = {
    children: PropTypes.node,
    user: MPropTypes.observableObject
}
