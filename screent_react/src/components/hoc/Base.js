import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import { Redirect } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import {
    Alerts,
    FilterPanel,
    Header,
    ImageCacher,
    Loading } from 'components'


let notificationTimer = null
let messageTimer = null


@inject('computer', 'message', 'notification', 'program', 't', 'user')
@observer
export default class Base extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }
    
    componentWillMount() {
        this.props.user.load()
        this.props.computer.load()
        this.props.program.load()
        this.props.notification.load()
        this.props.message.loadNotReadCount()
    }

    componentDidMount(){
        const { location } = this.props
        
        notificationTimer = setInterval(
            () => this.props.notification.load(), 60000)   
        
        messageTimer = setInterval(
            () => this.props.message.loadNotReadCount(), 60000)   

        if (location.pathname === '/'){
            this.props.computer.setControlsVisibility(true)
        }
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.location.pathname !== nextProps.location.pathname){
            const value = nextProps.location.pathname === '/' ? true : false
            this.props.computer.setControlsVisibility(value)
        }
    }

    componentWillUnmount(){
        clearInterval(notificationTimer)   
        clearInterval(messageTimer)   
    }

    componentWillReact() {
    }

    showFilterPanel(){
        const { computer } = this.props
        
        if (computer.activeComputer && computer.activeComputer.totalScreens > 0){
            if (!document.body.classList.contains('control-panel')){
                document.body.classList.add('control-panel')
                document.body.classList.add('control-panel-compact')
            }
            return true
        }
        return false
    }
    
    render(){
        const { computer, program, t, user } = this.props
        
        if (!t.data || !program.items || !user.me || computer.isLoading){
            return <Loading />
        }

        if (!user.me.isConfirmed){
            return <Redirect to="/not_confirmed" />
        }

        return (
            <div>
                <ImageCacher />
                <Header />

                <div className="page-content">
                    <div className="container-fluid">
                        <Alerts />

                        {this.props.children}
                    </div>
                        
                </div>

                {this.showFilterPanel() && <FilterPanel />} 
            </div>
        )
    }
}

Base.propTypes = {
    alert: MPropTypes.observableObject,
    children: PropTypes.node,
    computer: MPropTypes.observableObject,
    location: PropTypes.object,
    message: MPropTypes.observableObject,
    notification: MPropTypes.observableObject,
    program: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
