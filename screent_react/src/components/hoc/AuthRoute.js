import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import { Route, Redirect } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'


@inject('user')
@observer
export default class AuthRoute extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillMount() {
        this.props.user.checkToken()
    }

    render(){
        const {component: Component, ...rest} = this.props
        const { user } = this.props
        
        if (user.isAuthenticated === null){
            return <Loading />
        }

        return (
            <Route {...rest} render={props => (
                user.isAuthenticated === false ? (
                  <Redirect to={{pathname: '/sign-in', state: {from: props.location}}}/>
                ) : (
                  <Component {...props}/>
                )
            )}/>
        )
    }
}


AuthRoute.propTypes = {
    component: PropTypes.func,
    user: MPropTypes.observableObject
}
