import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Alert } from 'components'


/**
 * Global Alerts.
 * Works with AlertStore.
 * To add alert from any other component:
 * 1) inject('alert')
 * 2) thos.props.alert.add('success', 'some message')
 */
@inject('alert')
@observer
class Alerts extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    onClickClose(guid){
        const { alert } = this.props
        alert.remove(guid)
    }

    render(){
        const { alert } = this.props

        return (
            <wrap>
                {Array.from(alert.map.keys()).map(guid => {
                    const obj = alert.map.get(guid)

                    return (
                        <Alert
                            key={guid}
                            guid={guid}
                            type={obj.type}
                            message={obj.message}
                            onClickClose={this.onClickClose}
                            showClose />
                    )
                })}
            </wrap>
        )
    }
}


Alerts.propTypes = {
    alert: MPropTypes.observableObject
}

export default Alerts