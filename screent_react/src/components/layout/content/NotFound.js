import React from 'react'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'


export default class NotFound extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentDidMount(){
        for (let name of Array.from(document.body.classList)){
            document.body.classList.remove(name)
        }
    }   

    render(){
        return (
            <div className="page-center">
                <div className="page-center-in">
                    <div className="container-fluid">

                        <div className="page-error-box">
                            <div className="error-code">
                                {'404'}
                            </div>
                            <div className="error-title">
                                {'Page not found'}
                            </div>
                            <Link to="/" className="btn btn-rounded">
                                {'Home page'}
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
