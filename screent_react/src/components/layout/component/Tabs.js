import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import classNames from 'classnames'



/**
 * @param {array} titles
 * @param {number} [activeTab=0]
 *
 * Tabs content passed via children nodes.
 * The quantity of nodes should be equal the quantity of titles.
 */
export default class Tabs extends React.Component{
    static get defaultProps() {
        return {
            activeTab: 0,
        }
    }

    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            activeTab: this.props.activeTab
        }
    }

    onClick(index){
        this.setState({activeTab: index})
    }

    render(){
        const { titles, children } = this.props
        const { activeTab } = this.state

        return (
            <section className="tabs-section">
                <div className="tabs-section-nav tabs-section-nav-icons">
                    <div className="tbl">
                        <ul className="nav">
                            {titles.map((title, index) => {
                                let active = index === activeTab ? true : false
                                let cn = classNames('nav-link', {active: active})
                                
                                return (
                                    <li
                                        onClick={this.onClick.bind(null, index)}
                                        key={index}
                                        className="nav-item">
                                            <a className={cn}>
                                                <span className="nav-link-in">
                                                    {title}
                                                </span>
                                            </a>
                                    </li>
                                )

                            })}
                        </ul>
                    </div>

                </div>

                <div className="tab-content">
                    {React.Children.toArray(children).map((child, index) => {
                        let active = index === activeTab ? true : false
                        let cn = classNames('tab-pane fade', {'in active': active})

                        return (
                            <div key={index} className={cn}>
                                {child}
                            </div>
                        )

                    })}
                </div>
                
            </section>
        )
    }
}


Tabs.propTypes = {
    activeTab: PropTypes.number,
    children: PropTypes.arrayOf(PropTypes.node),
    titles: PropTypes.arrayOf(PropTypes.string)
}

