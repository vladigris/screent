import React, {Component, PropTypes} from 'react'
import { apiCall3 } from 'services/api'


/**
 * Input auto suggest with table as result.
 * rows - array of objects with any properties.
 * columns - mapping object of row properties to column names.
 * 
 * Send to server "query" property
 */
export default class AutoSuggestTable extends Component {
    constructor(props){
        super(props)
        this.getSuggestions = this.getSuggestions.bind(this)
        this.onKeyDown = this.onKeyDown.bind(this)
        
         this.state = {
            value: '',
            rows: [],
            isLoading: false
        }
    }

    onKeyDown(e){
        if (e.keyCode === 13){
            let value = e.target.value
            if (value.length < 2){
                return
            }
            this.getSuggestions(value)            
        }
    }

    async getSuggestions(value) {
        this.setState({isLoading: true})
        
        const url = this.props.url
        const data = {query: value}
        let response = await apiCall3(url, data, true)
        let result = await response.json()
        
        this.setState({isLoading: false})
        if (result.hasOwnProperty('non_field_errors')){
            // process errors
            return
        }
        this.setState({rows: result})
    }
 
    render() {
        const { title, placeholder, columns, tableClassName} = this.props
        const { value, rows, isLoading } = this.state

        let columnsArr = []
        for (let key in columns){
            columnsArr.push(columns[key])
        }

        // Reformat rows
        // They should be in the same order as columns.
        let rowsArr = []
        for (let row of rows){
            let newRow = {}
            newRow.cols = []
            for (let key in columns){
                newRow.cols.push({value: row[key]})
            }
            rowsArr.push(newRow)
        }

        return (
            <section className="card">
                <header className="card-header card-header-lg">{title}</header>
                <div className="card-block">
                    <div className="form-control-wrapper form-control-icon-right">
                        <input 
                            onKeyDown={this.onKeyDown}
                            className="form-control"
                            type="text"
                            placeholder={placeholder} />
                        {isLoading && <i className="fa fa-spinner fa-spin" />}
                    </div>
                        

                    {rows.length > 0 ?
                        <div className="table-responsive">
                            <table className={tableClassName}>
                                <thead>
                                    <tr>
                                        {columnsArr.map((name, index) => {
                                            return <th key={index}>{name}</th>
                                        })}
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    {rowsArr.map((row, index) => {
                                        return (
                                            <tr key={index}>
                                                {row.cols.map((col, index) => {
                                                    return (
                                                        <td key={index}>
                                                            {col.value}
                                                        </td>
                                                    )
                                                })}
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div> : null
                    }

                </div>
            </section>

        )
    }
}


AutoSuggestTable.propTypes = {
    columns: PropTypes.object,
    errors: PropTypes.object,
    placeholder: PropTypes.string,
    tableClassName: PropTypes.string,
    title:PropTypes.string,
    url: PropTypes.string.isRequired,
    value: PropTypes.any,
}
