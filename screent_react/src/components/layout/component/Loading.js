import React from 'react'
import autoBind from 'react-autobind'

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    width: '120px',
    height: '120px',
    marginTop: '-60px',
    marginLeft: '-60px'
}


export default class Loading extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        return (
            <img src="/img/loading.gif" style={style} alt="" />
        )
    }
}
