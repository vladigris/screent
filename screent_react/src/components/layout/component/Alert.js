import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import classNames from 'classnames'
import uuid from 'node-uuid'


export default class Alert extends React.Component{
    static get defaultProps() {
        return {
            guid: uuid.v4(),
            onClickClose: null,
            showClose: false,
            type: 'success'
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
    }

    onClick(){
        const { guid, onClickClose } = this.props
        if (onClickClose){
            onClickClose(guid)
        }
    }

    render(){
        const {type, message, showClose} = this.props
        const classType = 'alert-' + type

        return (
            <div className={classNames('alert', classType, 'alert-close', 'alert-dismissible')} role="alert">
                {showClose &&
                    <button onClick={this.onClick} type="button" className="close" aria-label="Close">
                        <span aria-hidden="true">{'\u00d7'}</span>
                    </button>}
                {message}
            </div>
        )
    }
}

Alert.propTypes = {
    guid: PropTypes.string,
    message: PropTypes.string.isRequired,
    onClickClose: PropTypes.func,
    showClose: PropTypes.bool,
    type: PropTypes.oneOf(['info', 'success', 'warning', 'danger'])
}
