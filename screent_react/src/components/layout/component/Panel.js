import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import classNames from 'classnames'


/**
 * Panel component properties:
 * title and showBodyDefault.
 * Clicking on title: show/hide component.
 */
export default class Panel extends React.Component{
    static get defaultProps() {
        return {
            isLoading: false
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillMount(){
        const { showBodyDefault } = this.props
        if (showBodyDefault === false){
            this.setState({showBody: false})
        } else {
            this.setState({showBody: true})
        }
    }

    onTitleClick(){
        this.setState({showBody: !this.state.showBody})
    }

    render(){
        const { children, title, isLoading } = this.props
        const { showBody } = this.state
        const bodyClassName = classNames('box-typical-body', { hidden: !showBody })
        const style = isLoading ? {color: '#999'} : {}

        return (
            <section className="box-typical">
                <header className="box-typical-header">
                        <div className="tbl-row">
                            <div className="tbl-cell tbl-cell-title">
                                <h3
                                    style={style}
                                    onClick={this.onTitleClick}
                                    className="cursor-pointer">
                                    
                                    {title}
                                </h3>
                            </div>
                        </div>
                </header>
                
                <div className={bodyClassName}>
                    {!isLoading && children}
                </div>
            </section>
            
        )
    }
}


Panel.propTypes = {
    children: PropTypes.node,
    isLoading: PropTypes.bool,
    showBodyDefault: PropTypes.bool,
    title: PropTypes.string
}




























