import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import classNames from 'classnames'


export default class Modal extends React.Component{
    static get defaultProps() {
        return {
            size: null,
            visible: false
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
    }

    onClickClose(){
        this.props.onClickClose()
    }

    render(){
        const {title, visible, children} = this.props

        let style
        if (visible){
            style = {'display': 'block', 'overflowY': 'scroll'}
        } else {
            style = {'display': 'none'}
        }

        let style1 = {}
        if (this.props.size){
            style1 = {
                'minWidth': this.props.size
            }
        }
        
        let cn = classNames('modal fade', {'in': visible})

        return (
            <div className={cn} role="dialog" aria-labelledby="myModalLabel" style={style}>
                <div className="modal-dialog" role="document" style={style1}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <button onClick={this.onClickClose} type="button" className="modal-close" data-dismiss="modal" aria-label="Close">
                                <i className="font-icon-close-2" />
                            </button>
                            <h4 className="modal-title" id="myModalLabel">{title}</h4>
                        </div>

                        <div style={{padding: 20}}>
                            {children}
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    children: PropTypes.node,
    content: PropTypes.element,
    onClickClose: PropTypes.func,
    size: PropTypes.string,
    title: PropTypes.string,
    visible: PropTypes.bool
}
