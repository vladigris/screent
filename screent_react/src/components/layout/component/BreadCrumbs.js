import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import autoBind from 'react-autobind'


export default class BreadCrumbs extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        let { items } = this.props

        return (
            <ol className="breadcrumb breadcrumb-simple">
                <li><Link to="/">{'Home'}</Link></li>
                
                {items.map((tup, index) => {
                    if (tup.length === 2){
                        return <li key={index}><Link to={tup[1]}>{tup[0]}</Link></li>
                    }
                    return <li key={index}>{tup[0]}</li>
                })}
            </ol>
        )
    }
}


BreadCrumbs.propTypes = {
    items: PropTypes.array
}






