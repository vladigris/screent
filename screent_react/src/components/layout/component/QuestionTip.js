import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import ReactTooltip from 'react-tooltip'
import './css/question-tip.css'


export default class QuestionTip extends React.Component{
    static get defaultProps() {
        return {
            place: 'left',
            type: 'info'
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const { place, type, text } = this.props

        return (
            <wrap>
                <ReactTooltip />
                <i
                    data-place={place}
                    data-type={type}
                    data-tip={text}
                    className="fa fa-question-circle-o question-tip" /> 
            </wrap>
        )
    }
}


QuestionTip.propTypes = {
    place: PropTypes.string,
    text: PropTypes.string.isRequired,
    type: PropTypes.string
}