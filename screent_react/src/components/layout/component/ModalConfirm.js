import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'


/**
 * params is object, that passed to onClickConfirm and onClickCancel
 * inputValidator - function that validates input value and returns bool.
 */
export default class ModalConfirm extends React.Component{
    static get defaultProps() {
        return {
            buttonCancelTitle: 'Cancel',
            buttonOKTitle: 'OK',
            hasInput: false,
            header: 'Are you sure?',
            id: 'modalConfirm',
            inputValue: '',
            inputValidator: null,
            inputPlaceholder: '',
            isVisible: false,
            params: {},
        }
    }

    constructor(props){
        super(props)
        this.onClickCancel = this.onClickCancel.bind(this)
        this.onClickConfirm = this.onClickConfirm.bind(this)
        this.onInputChange = this.onInputChange.bind(this)

        this.state = {
            inputValue: this.props.inputValue
        }
    }

    componentWillReceiveProps(newProps){
        this.setState({inputValue: newProps.inputValue})
    }


    onClickCancel(){
        this.props.onClickCancel(this.props.params)
    }

    onClickConfirm(){
        let params = this.props.params
        params.inputValue = this.state.inputValue
        this.props.onClickConfirm(params)
    }

    onInputChange(e){
        this.setState({inputValue: e.target.value})
    }

    render(){
        const { buttonCancelTitle, buttonOKTitle, header, isVisible,
                message, hasInput, inputPlaceholder,
                inputValidator  } = this.props

        let { inputValue } = this.state

        const cn = classNames('sweet-alert',
            {
                'showSweetAlert visible': isVisible,
                'hideSweetAlert': !isVisible,
                'show-input': hasInput
            }
        )

        const hasError = inputValidator && !inputValidator(inputValue) ? true : false
        const gcn = classNames('form-group', {error: hasError})
        
        const styleDisplay = isVisible ? {display: 'block'} : {display: 'none'}
        const styleOverlay = {opacity: 1.48}
        styleOverlay.display = isVisible ? 'block' : 'none'

        return (
                <div>
                   
                <div className="sweet-overlay" style={styleOverlay} />
                <div className={cn} style={styleDisplay}>
                
                    <h2>{header}</h2>
                    <p className="lead text-muted" style={styleDisplay}>{message}</p>

                    {hasInput ?
                        <div className={gcn}>
                            <input
                                onChange={this.onInputChange} 
                                className="form-control"
                                placeholder={inputPlaceholder}
                                value={inputValue} />
                           
                        </div>

                    : null}

                    <div className="sa-button-container">
                        <button
                            className="cancel btn btn-lg btn-default"
                            style={{display: 'inline-block'}} 
                            onClick={this.onClickCancel}>{buttonCancelTitle}</button>

                        {!hasError ? 
                            <div className="sa-confirm-button-container">
                                <button
                                    className="confirm btn btn-lg btn-success"
                                    style={{display: 'inline-block'}}
                                    onClick={this.onClickConfirm}>{buttonOKTitle}</button>
                            </div>
                        :null}
                        
                    </div>
                   
                </div>
            </div>
        )
    }
}


ModalConfirm.propTypes = {
    buttonCancelTitle: PropTypes.string,
    buttonOKTitle: PropTypes.string,
    hasInput: PropTypes.bool,
    header: PropTypes.string,
    id: PropTypes.string,
    inputPlaceholder: PropTypes.string,
    inputValidator: PropTypes.func,
    inputValue: PropTypes.any,
    isVisible: PropTypes.bool,
    message: PropTypes.string,
    onClickCancel: PropTypes.func,
    onClickConfirm: PropTypes.func,
    params: PropTypes.object
}
