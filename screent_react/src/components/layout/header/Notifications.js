import React from 'react'
import ReactDOM from 'react-dom'
import autoBind from 'react-autobind'
import classNames from 'classnames'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'



@inject('notification')
@observer
export default class Notifications extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            isOpen: false
        }
    }

    componentDidMount () {
        document.addEventListener('click', this.handleCilck)
    }

    componentWillUnmount () {
        document.removeEventListener('click', this.handleCilck)
    }

    handleCilck(e){
        const area = ReactDOM.findDOMNode(this)

        if (area && !area.contains(e.target)) {
            this.setState({isOpen: false})
        }
    }

    onClickOpen(e){
        e.preventDefault()
        this.setState({isOpen: !this.state.isOpen})
    }

    onClickMarkAsRead(guid){
        let form = {guid: guid, isRead: true}
        this.props.notification.edit(form)
    }

    render(){
        const { notification } = this.props
        const { isOpen } = this.state
        const cn1 = classNames('dropdown dropdown-notification notif', {open: isOpen})
        const cn2 = classNames('header-alarm', {
            active: notification.hasUnread,
            'dropdown-toggle': notification.hasUnread
        })

        if (notification.map.size === 0){
            return null
        }
       
        return (

            <div className={cn1}>
                <a href="#" className={cn2}>
                    <i className="font-icon-alarm" onClick={this.onClickOpen} />
                </a>
                <div className="dropdown-menu dropdown-menu-right dropdown-menu-notif">
                    <div className="dropdown-menu-notif-header">
                        {'Notifications'}
                        {notification.unreadCount > 0 &&
                            <span style={{marginLeft: '5px'}} className="label label-pill label-danger">
                                {notification.unreadCount}
                            </span>
                        }
                    </div>
                    <div className="dropdown-menu-notif-list">
                        {Array.from(notification.map.keys()).map(guid => {
                            let n = notification.map.get(guid)

                            return (
                                <div key={guid} className="dropdown-menu-notif-item" style={{paddingLeft: '15px'}}>
                                    {n.isRead || <div className="dot" style={{marginRight: '5px'}} />}
                                    
                                    {n.isRead ?
                                        <span>{n.title}</span>
                                    :
                                        <span
                                            onClick={this.onClickMarkAsRead.bind(null, n.guid)}
                                            style={{cursor: 'pointer'}}
                                            title="mark as read">
                                            {n.title}
                                        </span>
                                    }
                                    <div className="color-blue-grey-lighter">{n.ago}</div>
                                </div>
                            )                    
                        })}
                      
                    </div>
                   
                </div>
            </div>
        )
    }
}

Notifications.propTypes = {
    notification: MPropTypes.observableObject
}
