import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import { withRouter } from 'react-router-dom'
import classNames from 'classnames'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'



@withRouter
@inject('computer')
@observer
export default class TopMenuComputers extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            isOpen: false,
            redirectTo: null
        }
    }

    componentDidMount () {
        document.addEventListener('click', this.handleClick)
    }

    componentWillUnmount () {
        document.removeEventListener('click', this.handleClick)
    }

    handleClick(e){
        const area = ReactDOM.findDOMNode(this)

        if (area && !area.contains(e.target)) {
            this.setState({isOpen: false})
        }
    }

    onClickOpen(e){
        e.preventDefault()
        this.setState({isOpen: !this.state.isOpen})
    }

    onClickComputer(guid, e){
        e.preventDefault()
        this.props.computer.setActiveComputer(guid)
        this.setState({isOpen: false})
    }

    onClickAddComputer(e){
        e.preventDefault()
        this.props.history.push('/add_computer')
        this.setState({isOpen: false})
    }

    render(){
        const { computer } = this.props
        const { isOpen } = this.state


        const activeComputer = computer.activeComputer
        const cn = classNames(
            'dropdown dropdown-typical mobile-opened',
            {open: isOpen}
        )

        return (
            <div className={cn}>
                <a className="dropdown-toggle" href="#" onClick={this.onClickOpen}>
                    <span className="font-icon fa fa-desktop" />
                    {activeComputer ?
                        <span className="lbl">{activeComputer.name}</span>
                    : 
                        <span className="lbl">{'Computers'}</span>}
                    
                </a>

                <div className="dropdown-menu">
                    {Array.from(computer.map.keys()).map(guid => {
                        let c = computer.map.get(guid)

                        return (
                            <a
                                key={guid}
                                onClick={this.onClickComputer.bind(null, c.guid)}
                                className="dropdown-item"
                                href="#">
                                
                                {c.name}
                            </a>        
                        )
                    })}

                    {computer.map.size > 0 && <div className="dropdown-divider" />}
                    
                    <a onClick={this.onClickAddComputer} className="dropdown-item" href="#">
                        <span className="font-icon fa fa-plus"/>
                        {'Add computer...'}
                    </a>
                    
                </div>
            </div>
        )
    }
}


TopMenuComputers.propTypes = {
    computer: MPropTypes.observableObject,
    history: PropTypes.object
}
