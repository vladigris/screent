import React from 'react'
import ReactDOM from 'react-dom'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'


@inject('user')
@observer
export default class TopUserMenu extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            isOpen: false
        }
    }
    
    componentDidMount () {
        document.addEventListener('click', this.handleClick)
    }

    componentWillUnmount () {
        document.removeEventListener('click', this.handleClick)
    }

    handleClick(e){
        const area = ReactDOM.findDOMNode(this)

        if (area && !area.contains(e.target)) {
            this.setState({isOpen: false})
        }
    }
    
    toggle(){
        this.setState({isOpen: !this.state.isOpen})
    }

    logout(e){
        e.preventDefault()
        this.props.user.logout()
    }

    render(){
        const { isOpen } = this.state
        const cn = classNames('dropdown user-menu', {open: isOpen})

        return (
            <div className={cn}>
                <button onClick={this.toggle} className="dropdown-toggle" type="button">
                    <img src="img/avatar-2-64.png" alt="" />
                </button>
                <div className="dropdown-menu dropdown-menu-right">
                    
                    <Link onClick={this.toggle} className="dropdown-item" to="/profile">
                        <span className="font-icon glyphicon glyphicon-user" />
                        {'Profile'}
                    </Link>

                    <Link onClick={this.toggle} className="dropdown-item" to="/edit_computers">
                        <span className="font-icon glyphicon glyphicon-cog" />
                        {'Computers'}
                    </Link>                   

                    <Link onClick={this.toggle} className="dropdown-item" to="/faq">
                        <span className="font-icon glyphicon glyphicon-question-sign" />
                        {'Faq'}
                    </Link>

                    <Link onClick={this.toggle} className="dropdown-item" to="/support">
                        <span className="font-icon fa fa-ticket" />
                        {'Support'}
                    </Link>
                    
                    <div className="dropdown-divider" />
                    <a onClick={this.logout} className="dropdown-item" href="#">
                        <span className="font-icon glyphicon glyphicon-log-out" />
                        {'Logout'}
                    </a>
                </div>
            </div>
        )
    }
}


TopUserMenu.propTypes = {
    user: MPropTypes.observableObject
}
