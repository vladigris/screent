import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'


const style = {
    marginLeft: '10px',
    'marginRight': '10px',
    backgroundColor: '#f1f1f1',
    padding: '3px 10px 3px 10px'
}


/**
 * Custom input for DatePicker.
 */
export default class CustomInput extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        return (
            <span
                style={style}
                onClick={this.props.onClick}>
                {this.props.value}
            </span>
        )
    }
}


CustomInput.propTypes = {
    onClick: PropTypes.func,
    value: PropTypes.string
}
