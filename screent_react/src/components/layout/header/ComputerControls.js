import moment from 'moment'
import React from 'react'
import ReactDOM from 'react-dom'
import autoBind from 'react-autobind'
import classNames from 'classnames'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import DatePicker from 'react-datepicker'
import CustomInput from './CustomInput'



@inject('computer', 'screen')
@observer
export default class ComputerControls extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            lastArrowPress: moment()
        }
    }

    componentWillMount(){
        document.addEventListener('keydown', this.onKeyDown, false)
    }

    componentDidMount () {
        document.addEventListener('click', this.handleClick)        
    }

    componentWillUnmount(){
        document.removeEventListener('keydown', this.onKeyDown, false)
        document.removeEventListener('click', this.handleClick)
    }

    handleClick(e){
        const area = ReactDOM.findDOMNode(this)

        if (area && !area.contains(e.target)) {
            this.setState({isOpen: false})
        }
    }

    /**
     * Arrow right (next) = 39, arrow left (prev) = 37
     */
    onKeyDown(e){
        if (e.keyCode !== 37 && e.keyCode !== 39){
            return
        }

        const { computer, screen } = this.props

        if (!computer.activeComputer){
            return
        }

        if (e.keyCode === 39 && screen.isPlaying){
            return
        }

        // If press arrow too fast
        let now = moment()
        let ms = now.diff(this.state.lastArrowPress)
        if (ms < 500){
            return
        }

        const direction = e.keyCode === 39 ? 'next' : 'prev'
        screen.playNext('manual', direction)
        this.setState({lastArrowPress: moment()})
    }

    onChangeStartDate(dt){
        const { computer, screen } = this.props

        computer.setStartDate(dt)
        screen.stopAndClear()
        screen.reload()
    }

    onChangeEndDate(dt){
        const { computer, screen } = this.props

        computer.setEndDate(dt)
        screen.stopAndClear()
        screen.reload()
    }

    onClickPlay(){
        const { screen } = this.props
        
        screen.play()
        screen.playNext()
    }

    onClickStop(){
        this.props.screen.stop()   
    }

    render(){
        const { computer, screen } = this.props
        const cnPlay = classNames(
            'font-icon fa fa-play fa-pointer',
            {green: screen.isPlaying}
        )

        if (!computer.isControlsVisible){
            return null
        }

        if (!computer.activeComputer || computer.activeComputer.totalScreens === 0){
            return null
        }

        let minDate = computer.activeComputer.minDate
        let maxDate = computer.activeComputer.maxDate

        if (!minDate || !maxDate){
            return null
        }

        return (
            <div className="inline" style={{marginTop: '5px'}}>
                <DatePicker
                    dateFormat="DD MMM"
                    minDate={minDate}
                    maxDate={maxDate}
                    customInput={<CustomInput />}
                    selected={computer.startDate}
                    onChange={this.onChangeStartDate} />

                {'-'}

                <DatePicker
                    dateFormat="DD MMM"
                    minDate={minDate}
                    maxDate={maxDate}
                    customInput={<CustomInput />}
                    selected={computer.endDate}
                    onChange={this.onChangeEndDate} />

                <span>
                <i onClick={this.onClickPlay} className={cnPlay} /> 
                &nbsp;
                <i onClick={this.onClickStop} className="font-icon fa fa-stop fa-pointer" /> 
                </span>
                
            </div>
        )
    }
}


ComputerControls.propTypes = {
    computer: MPropTypes.observableObject,
    screen: MPropTypes.observableObject,
}


