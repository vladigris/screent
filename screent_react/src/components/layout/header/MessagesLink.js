import React from 'react'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'


@inject('message')
@observer
export default class MessagesLink extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const { message } = this.props
        if (message.map.size === null){
            return null
        }

        const cn = classNames('header-alarm', {active: message.notReadCount > 0})

        return (
            <div className="dropdown dropdown-notification messages">
                <Link to="/support" className={cn}>
                    <i className="font-icon-mail" />
                </Link>
            </div>
        )
    }
}


MessagesLink.propTypes = {
    message: MPropTypes.observableObject
}


