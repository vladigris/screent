import React from 'react'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'
import {
    Notifications,
    MessagesLink,
    TopMenu,
    TopUserMenu } from 'components'


export default class Header extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        
        return (
            
            <header className="site-header">
                <div className="container-fluid">
                    <Link to="/" className="site-logo">
                        <img className="hidden-md-down" src="/img/logo-2.png" alt="" />
                        <img className="hidden-lg-up" src="/img/logo-2-mob.png" alt="" />
                    </Link>
                    
                    
                    <div className="site-header-content">
                        <div className="site-header-content-in">
                            <div className="site-header-shown">
                               
                                <Notifications />
                                <MessagesLink />
                                <TopUserMenu />
                                
                            </div> 

                            <TopMenu />
                                        
                        </div> 
                    </div> 
                </div> 
            </header> 
        )
    }
}
