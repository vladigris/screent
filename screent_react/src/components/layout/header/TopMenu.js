import React from 'react'
import autoBind from 'react-autobind'
import { ComputerControls, TopMenuComputers } from 'components'



export default class TopMenu extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        return (
            <wrap>
                <div className="mobile-menu-right-overlay" />
                <div className="site-header-collapsed">

                    <div className="site-header-collapsed-in">
                        <TopMenuComputers />
                        <ComputerControls />

                    </div>

                </div> 
            </wrap>
        )
    }
}

