import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'
import { Loading } from 'components'




@inject('faq')
@observer
export default class Faq extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillMount(){
        const { faq } = this.props
        if (!faq.items){
            faq.load()
        }
    }

    render(){
        const { faq } = this.props
        if (!faq.items){
            return <Loading />
        }

        return (
            <div className="box-typical box-typical-padding">
                <h1 className="text-center">{'F.A.Q.'}</h1>
                <br />
                {faq.items.map((obj, index) => {
                    return (
                        <wrap key={index}>
                            <div className="row">
                                <div className="col-md-12">
                                    <article className="faq-item">
                                        <div className="faq-item-circle">{'?'}</div>
                                        <h5>{rHTML(obj.question)}</h5>
                                    </article>
                                    <article className="faq-item">
                                        <p>{rHTML(obj.answer)}</p>
                                    </article>
                                </div>
                            </div>
                            <hr className="dashed" />
                        </wrap>
                    )                
                })}

                
            </div>
        )
    }
}


Faq.propTypes = {
    faq: MPropTypes.observableObject
}
