import React from 'react'
import PropTypes from 'prop-types'
import Highcharts from 'highcharts'
import addFunnel from 'highcharts/modules/funnel'
import autoBind from 'react-autobind'


class MyChart extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentDidMount(){
        
        if (this.props.modules) {
            this.props.modules.forEach(function (module) {
                module(Highcharts)
            })
        }
        this.chart = new Highcharts[this.props.type](
            this.props.container, 
            this.props.options
        )
    }
    
    componentWillUnmount() {
        this.chart.destroy()
    }

    render(){
        return (
            <div id={this.props.container} />
        )
    }
}

MyChart.propTypes = {
    container: PropTypes.string,
    modules: PropTypes.array,
    options: PropTypes.object,
    type: PropTypes.string
}




export default class TestPage extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const options = {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Fruit Consumption'
            },
            xAxis: {
                categories: ['Apples', 'Bananas', 'Oranges']
            },
            yAxis: {
                title: {
                    text: 'Fruit eaten'
                }
            },
            series: [{
                name: 'Jane',
                data: [1, 0, 4]
            }, {
                name: 'John',
                data: [5, 7, 3]
            }]

        }

        return (
            <wrap>
                <div id="mychart" />
                <MyChart
                    container="mychart"
                    options={options}
                    type="Chart"
                    modules={[addFunnel]} />
            </wrap>
        )
    }
}