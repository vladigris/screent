import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'



export default class CheckBoxToggle extends React.Component{
    static get defaultProps() {
        return {
            isInline: false,
            disabled: false
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
    }

    onClick(){
        this.props.onClick()
    }
    
    render(){
        const { isInline } = this.props

        let style = {}
        if (isInline){
            style.display = 'inline'
            style.padding = '0 20px 0 10px'
        }

        return (
            <div className="checkbox-toggle" onClick={this.onClick} style={style}>
                <input
                    type="checkbox"
                    onChange={this.onClick}
                    checked={this.props.checked}
                    disabled={this.props.disabled} />
                
                <label>{this.props.title}</label>
            </div>
        )
    }
}


CheckBoxToggle.propTypes = {
    checked: PropTypes.bool.isRequired,
    disabled: PropTypes.bool,
    isInline: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired
}
