import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import { FieldWrapper } from 'components'



export default class InputDate extends React.Component {
    static get defaultProps() {
        return {
            date: new Date(),
            errors: null
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
        
        this.state = {
            startDate: moment(this.props.date)
        }
    }

    onChange(date){
        this.setState({startDate: date})
    }

    render() {
        const { name, ...other } = this.props

        return (
            <FieldWrapper name={name} {...other}>
                <DatePicker 
                    dateFormat="YYYY-MM-DD"
                    name={name}
                    selected={this.state.startDate}
                    onChange={this.onChange} />

            </FieldWrapper>
        )
    }
}


InputDate.propTypes = {
    date: PropTypes.object,
    errors: PropTypes.object,
    label: PropTypes.string,
    name: PropTypes.string.isRequired
}
