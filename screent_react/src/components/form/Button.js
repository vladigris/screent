import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import classNames from 'classnames'


export default class Button extends React.Component {
    static get defaultProps() {
        return {
            type: 'submit',
            isColumns: true,
            isDisabled: false,
            isLoading: false,
            isVisible: true,
            className: 'btn btn-primary',
            onClick: null,
            text: 'Submit'
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
    }

    onClick(e){
        if (this.props.onClick !== null)
            this.props.onClick(e)
    }

    render() {
        const {
            className,
            isColumns,
            isDisabled,
            isLoading,
            isVisible,
            text,
            type } = this.props

        const rcn = classNames('form-group row', {hidden: !isVisible})

        if (isColumns){
            return (
                <div className={rcn}>
                    <label className="col-sm-2 form-control-label" />
                    <div className="col-sm-10">
                        <p className="form-control-static">
                            <button 
                                disabled={isDisabled || isLoading}
                                type={type}
                                className={className}
                                onClick={this.onClick}>
                            
                                {text}
                                
                                {isLoading &&
                                    <i
                                        style={{marginLeft: '10px'}}
                                        className="fa fa-spinner fa-spin" />}
                
                            </button>
                        </p>
                    </div>
                </div>
            )    
        }

        const cn = classNames(className, {hidden: !isVisible})
        return (
            <button
                type={type}
                className={cn}
                onClick={this.onClick}>

                {text}
            </button>
        )
    }
}


Button.propTypes = {
    className: PropTypes.string,
    isColumns: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isLoading: PropTypes.bool,
    isVisible: PropTypes.bool,
    onClick: PropTypes.func,
    text: PropTypes.string,
    type: PropTypes.string
}
