import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import classNames from 'classnames'


/**
 * Reimplement behaviour of HTML MultiSelect using div.
 * Dependency css classes:
 * .multiselect-simple .item{cursor: pointer;}
 * .multiselect-simple .item.checked{background-color: #dce5ec}
 *
 * Properties: items, onUpdate
 *
 * items is array of objects: {value:..., checked:...}
 * onUpdate - callback that get array with all checked values after each change.
 */
export default class MultiSelectSimple extends React.Component {
    static get defaultProps() {
        return {
            height: 100,
            style: {}
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
        
        this.state = {
            items: []
        }
    }

    componentWillMount(){
        this.setState({items: this.props.items})
    }
    
    onItemClick(item, e){
        item.checked = !item.checked
        let items = this.state.items
        let values = []
        for (let i of items){
            if (i.value === item.value){
                i.checked = item.checked
            }
            if (i.checked){
                values.push(i.value)    
            }
        }
        this.setState({items: items})
        this.props.onUpdate(values)    
    }

    render() {
        const { items } = this.state
        const { height, label } = this.props
        
        const style = {
            height: `${height}px`,
            'overflowY': 'scroll'
        }

        return (
            <div className="form-group">
                {label && <label className="form-label">{label}</label>} 
                <div className="form-control-wrapper">
                    <div className="form-control multiselect-simple" style={style}>
                        {items.map((item, index) => {
                            const cn = classNames('item', {checked: item.checked})
                            return (
                                <div
                                    key={index}
                                    className={cn}
                                    onClick={this.onItemClick.bind(null, item)}>
                                
                                    {item.value}
                                </div>
                            )                
                        })}
                    </div>
                </div>        
            </div>
        )
    }
}


MultiSelectSimple.propTypes = {
    height: PropTypes.number,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.any,
            checked: PropTypes.bool
        }).isRequired
    ),
    label: PropTypes.string,
    onUpdate: PropTypes.func.isRequired
}


