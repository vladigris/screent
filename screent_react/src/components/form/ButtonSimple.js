import React, {Component, PropTypes} from 'react'


export default class ButtonSimple extends Component {
    static get defaultProps() {
        return {
            type: 'button',
            className: 'btn btn-primary',
            isDisabled: false,
            isLoading: false,
            text: 'Submit',
            onClick: null
        }
    }

    constructor(props){
        super(props)
        this.onClick = this.onClick.bind(this)
    }

    onClick(e){
        if (this.props.onClick !== null)
            this.props.onClick(e)
    }

    render() {
        const { isDisabled, isLoading, className, text, type } = this.props
        return (
            <button
                disabled={isDisabled || isLoading}
                type={type}
                className={className}
                onClick={this.onClick}>

                {text}
                
                {isLoading &&
                    <i style={{marginLeft: '10px'}} className="fa fa-spinner fa-spin" />}
                
            </button>
        )

    }
}


ButtonSimple.propTypes = {
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isLoading: PropTypes.bool,
    onClick: PropTypes.func,
    text: PropTypes.string,
    type: PropTypes.string
}
