import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'


/**
 * All selected values placed to hidden input splitted by separator.
 */
export default class Select2 extends React.Component{
    static get defaultProps() {
        return {
            maxItems: null,
            initialItems: [],
            options: [],
            regex: null,
            separator: ',',
            wrapperClassName: ''
        }
    }

    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            inputHeight: 0,
            inputValue: '',
            activeIndex: null,
            values: this.props.initialItems || []
        }
    }

    componentDidMount(){
        const h = this.refs.input.offsetHeight
        this.updateHeight(h)
    }

    getPlaceholder(){
        const { maxItems } = this.props
        if (maxItems && this.state.values.length >= maxItems){
            return 'Max limit exceeded'
        }

        if (this.getOptions().length > 0){
            return ''
        }
        return this.props.placeholder
    }

    updateHeight(height){
        this.setState({inputHeight: height})
    }

    onKeyDown(e){
        let { activeIndex } = this.state

        // esc
        if (e.keyCode === 27){
            this.setState({activeIndex: null, inputValue: ''})
            return
        }

        // arrow down
        if (e.keyCode === 40){
            if (activeIndex === null){
                activeIndex = 0
            } else if (activeIndex >= this.getOptions().length - 1){
                activeIndex = 0
            } else {
                activeIndex += 1
            }
            this.setState({activeIndex: activeIndex})
            return
        }

        // arrow up
        if (e.keyCode === 38){
            if (activeIndex === null){
                activeIndex = this.getOptions().length - 1
            } else if (activeIndex <= 0){
                activeIndex = this.getOptions().length - 1
            } else {
                activeIndex -= 1
            }
            this.setState({activeIndex: activeIndex})
            return
        }
        
        // enter
        if (e.keyCode === 13){
            e.preventDefault()
            let value
            if (activeIndex !== null){
                value = this.getOptions()[activeIndex]
            } else {
                value = this.state.inputValue
            }
            this.onAdd(value)
        }
    }

    onChange(e){
        this.setState({
            inputValue: e.target.value,
            activeIndex: null
        })
    }

    onAdd(value){
        value = value.trim()
        if (value.length === 0){
            return
        }

        const regex = this.props.regex

        if (this.props.maxItems && this.state.values.length >= this.props.maxItems ){
            return
        }

        let values = [...this.state.values]
        if (!values.includes(value)){
            if (regex){
                const rex = new RegExp(regex.source, regex.flags)
                if (rex.test(value)){
                    values.push(value)        
                }
            }
        }
        this.setState({
            activeIndex: null,
            inputValue: '',
            values: values
        })
    }

    onDelete(value){
        let values = [...this.state.values]
        values = values.filter((val) => val !== value)
        this.setState({values})
    }

    getOptions(){
        const { inputValue } = this.state
        
        if (inputValue.length === 0){
            return []
        }

        let options = [...this.props.options]
        return options.filter((val) => val.toString().toUpperCase().includes(inputValue.toUpperCase()) && this.state.values.indexOf(val) === -1)
    }

    render(){
        const { name, separator, wrapperClassName } = this.props
        let { activeIndex, inputHeight, inputValue, values } = this.state
        inputHeight += 15

        return (
            <div className={wrapperClassName}>
                <input type="hidden" name={name} ref="resultInput" value={values.join(separator)} />

                <span className="select2 select2-container select2-container--default select2-container--focus">
                    <span className="selection">
                        <span className="select2-selection select2-selection--multiple">
                            <ul className="select2-selection__rendered">
                                {values.map((val, index) => {
                                    return (
                                        <li key={index} className="select2-selection__choice">
                                            <span
                                                onClick={this.onDelete.bind(null, val)}
                                                className="select2-selection__choice__remove">{'×'}</span>
                                            {val}
                                        </li>        
                                    )                                                
                                })}

                                <li className="select2-search select2-search--inline">
                                    <input
                                        ref="input"
                                        className="select2-search__field"
                                        type="text"
                                        placeholder={this.getPlaceholder()}
                                        autoComplete="off"
                                        onKeyDown={this.onKeyDown}
                                        onChange={this.onChange}
                                        value={inputValue} />
                                </li>
                            </ul>
                        </span>
                    </span>

                    {this.getOptions().length > 0 &&
                        <span className="dropdown-wrapper">
                            <span style={{zIndex: 1000, top: `${inputHeight}px`, left: '0'}} className="select2-dropdown">
                                <span className="select2-results">
                                    <ul className="select2-results__options">
                                        {this.getOptions().map((val, index) => {
                                            let style = {}
                                            if (index === activeIndex){
                                                style.color = '#00a8ff'
                                            }

                                            return (
                                                <li
                                                    style={style}
                                                    key={index}
                                                    className="select2-results__option"
                                                    onClick={this.onAdd.bind(null, val)}>
                                                
                                                    {val}
                                                </li>
                                            )  
                                        })}
                                    </ul>
                                </span>
                            </span>
                        </span>
                    }
                    
                </span>
            </div>
        )
    }
}


Select2.propTypes = {
    initialItems: PropTypes.array,
    maxItems: PropTypes.number,
    name: PropTypes.string,
    options: PropTypes.array,
    placeholder: PropTypes.string,
    regex: PropTypes.object,
    separator: PropTypes.string,
    wrapperClassName: PropTypes.string
}