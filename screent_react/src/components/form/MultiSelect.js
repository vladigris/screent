import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'


/**
 * Advanced MultiSelect.
 * Properties: title, items, onUpdate
 * title is not required.
 *
 * "setMarginRight" is true, component get the width of itself
 * dynamically and sets style for margin right.
 *
 * "isReturnIds" is false: returns array of values
 * items is array of objects: {id:..., value:..., checked:...}
 * onUpdate - callback that get array with all checked values after each change.
 */
export default class MultiSelect extends React.Component {
    static get defaultProps() {
        return {
            isReturnIds: false,
            setMarginRight: false,
            title: null,
            onUpdate: null,
            style: {}
        }
    }

    constructor(props){
        super(props)
        this.onButtonClick = this.onButtonClick.bind(this)
        this.onItemClick = this.onItemClick.bind(this)
        this.onChange = this.onChange.bind(this)

        this.state = {
            isOpen: this.props.title === null,
            items: []
        }
    }

    componentWillMount(){
        this.setState({items: this.props.items})
    }

    componentDidMount(){
        if (this.props.setMarginRight){
            const width = this.refs.multiselect.offsetWidth
            const style = {
                marginRight: `${width}px`
            }
            this.setStyle(style)    
        }
        
    }

    /**
     * Do nothing.
     * Disable React warning about checked attribute.
     * checked attribute is styled by css.
     */
    onChange(){
    }

    onButtonClick(){
        this.setState({isOpen: !this.state.isOpen})
    }

    onItemClick(item, e){
        item.checked = !item.checked
        let items = this.state.items
        let values = []
        let ids = []
        for (let i of items){
            if (i.id === item.id){
                i.checked = item.checked
            }
            if (i.checked){
                ids.push(i.id)
                values.push(i.value)    
            }
        }
        this.setState({items: items})
        if (this.props.onUpdate !== null){
            if (this.props.isReturnIds){
                this.props.onUpdate(ids)                
            } else {
                this.props.onUpdate(values)    
            }
        }
    }

    setStyle(style){
        this.setState({style: style})
    }

    render() {
        const { title, style } = this.props
        const { isOpen, items } = this.state

        const bcn = classNames('dropdown', {open: isOpen})

        return (
            <div style={style}>
                <div className={bcn}>
                    {title &&
                        <button onClick={this.onButtonClick} className="btn btn-inline dropdown-toggle" type="button">
                            {title}
                        </button>}

                    <div ref="multiselect" className="dropdown-menu multiselect">
                        
                        {items.map( (item) => {
                            return (
                                <div key={item.id} onClick={this.onItemClick.bind(null, item)} className="checkbox dropdown-item">
                                    <input onChange={this.onChange} type="checkbox" checked={item.checked} />
                                    <label>{item.value}</label>
                                </div>    
                            )
                        })}
                        
                    </div>
                </div>

            </div>
        )
    }
}


MultiSelect.propTypes = {
    isReturnIds: PropTypes.bool,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.any,
            value: PropTypes.any,
            checked: PropTypes.bool
        })
    ),
    onUpdate: PropTypes.func,
    setMarginRight: PropTypes.bool,
    style: PropTypes.object,
    title: PropTypes.string
}


