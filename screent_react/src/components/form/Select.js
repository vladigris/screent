import React from 'react'
import PropTypes from 'prop-types'
import { FieldWrapper } from 'components'


/**
 * props: name, errors, label passed to FieldWrapper
 * FieldWrapper knows how to render errors.
 */
export default class Select extends React.Component {
    static get defaultProps() {
        return {
            label: null,
            onSelect: null
        }
    }

    constructor(props){
        super(props)
        this.onSelect = this.onSelect.bind(this)
    }

    onSelect(e){
        if (this.props.onSelect !== null){
            this.props.onSelect(e)    
        }
    }

    render() {
        const { data, name, selected, style, ...other } = this.props

        return (
            <FieldWrapper name={name} {...other}>
                <select
                    onChange={this.onSelect}
                    name={name}
                    defaultValue={selected}
                    style={style}>
                    
                    {data.map((item, index) => {
                        let level = item.level || 0
                        // indent by level
                        let text = '\u00a0'.repeat(level*3) + item.text
                        
                        return (
                            <option
                                key={index}
                                value={item.value}>{text}</option>    
                        )
                    })}
                </select>
            </FieldWrapper>
        )
    }
}


Select.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
        text: PropTypes.string,
        level: PropTypes.number
    })),
    errors: PropTypes.object,
    label: PropTypes.string,
    name: PropTypes.string,
    onSelect: PropTypes.func,
    selected: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
    style: PropTypes.object
}

