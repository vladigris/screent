import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes } from 'mobx-react'
import autoBind from 'react-autobind'
import classNames from 'classnames'


/**
 * onUpdate - pass to parent selected value.
 */
export default class RadioButtons extends React.Component{
    static get defaultProps() {
        return {
            isSmall: false,
            label: null,
            selected: null
        }
    }

    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            selectedValue: this.props.selected
        }
    }

    onClick(value){
        this.setState({selectedValue: value})
        this.props.onUpdate(value)
    }

    render(){
        const { selectedValue } = this.state
        const { isSmall, label, values } = this.props
        const cnGroup = classNames('btn-group', {'btn-group-sm': isSmall})

        return (
            <div className="form-group">
                {label && <label className="form-label">{label}</label>}

                <div className="btn-toolbar">
                    <div className={cnGroup}>
                        {values.map((value, index) => {
                            const cn = classNames('btn', {
                                'btn-default-outline': value !== selectedValue,
                                'btn-default-inline': value === selectedValue
                            })

                            return (
                                <button
                                    onClick={this.onClick.bind(null, value)}
                                    key={index}
                                    type="button"
                                    className={cn}>

                                    {value}
                                </button>
                            )             
                        })}
                    </div>
                </div>
            </div>
        )
    }
}


RadioButtons.propTypes = {
    isSmall: PropTypes.bool,
    label: PropTypes.string,
    onUpdate: PropTypes.func,
    selected: PropTypes.any,
    values: MPropTypes.arrayOrObservableArray.isRequired,
}

