import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import classNames from 'classnames'


export default class FieldWrapper extends React.Component {
    static get defaultProps() {
        return {
            errors: {},
            label: null,
            isVisible: true
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
    }

    get name(){
        const { name } = this.props
        return name.toLowerCase()
    }

    hasError(){
        const { errors } = this.props
        return errors !== null && errors.hasOwnProperty(this.name)
    }

    renderErrors(){
        const {errors} = this.props

        if (!this.hasError()){
            return null
        }

        // Fix if value not array
        if (typeof errors[this.name] === typeof ''){
            errors[this.name] = [errors[this.name]]
        }

        return (
            <div className="error-list" data-error-list>
                <ul>
                    {errors[this.name].map(item => {
                        return <li key={item}>{item}</li> })
                    }
                </ul>
            </div>
        ) 
    }

    renderWithLabel(rcn, gcn, lbl, childrenWithProps){
        const { isLoading, helpString } = this.props

        return (
            <div className={rcn}>
                <label className="col-sm-2 form-control-label">{lbl}</label>
                <div className="col-sm-10">
                    <div className={gcn}>
                        <div className="form-control-wrapper form-control-icon-right">
                            {childrenWithProps}
                            {isLoading && <i className="fa fa-spinner fa-spin" />}
                            
                            {helpString && <small className="text-muted">{helpString}</small>}
                            {this.renderErrors()}

                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderWithoutLabel(gcn, childrenWithProps){
        const { helpString } = this.props

        return (
            <div className={gcn}>
                {childrenWithProps}
                {this.renderErrors()}
                {helpString && <small className="text-muted">{helpString}</small>}
            </div>
        )
    }

    render() {
        const { isVisible, label, children } = this.props

        const rcn = classNames('form-group row', {hidden: !isVisible})
        const gcn = classNames('form-group', {error: this.hasError(), hidden: !isVisible})
        // icn passes to child element
        const icn = classNames('form-control', {error: this.hasError()})

        const childrenWithProps = React.Children.map(children,
            (child) => React.cloneElement(child, {className: icn})
        )

        if (label === null)
            return this.renderWithoutLabel(gcn, childrenWithProps)

        return this.renderWithLabel(rcn, gcn, label, childrenWithProps)
    }
}

FieldWrapper.propTypes = {
    children: PropTypes.node.isRequired,
    errors: PropTypes.object,
    helpString: PropTypes.string,
    isLoading: PropTypes.bool,
    isVisible: PropTypes.bool,
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
}
