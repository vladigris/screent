import React, {Component, PropTypes} from 'react'
import autoBind from 'react-autobind'
import math from 'mathjs'
import { FieldWrapper } from 'components'


/**
 * props: name, errors, label, isVisible passed to FieldWrapper
 * FieldWrapper knows how to render errors.

 * If math expression detected - then calculates value.
 *
 */
export default class Input extends Component {
    static get defaultProps() {
        return {
            type: 'text',
            placeholder: '',
            disabled: false,
            helpString: null,
            onBlur: null,
            onChange: null,
            onKeyDown: null,
            onKeyPress: null,
            onKeyUp: null,
            ref: 'input',
            style: {}
        }
    }

    constructor(props){
        super(props)
        autoBind(this)
        
        this.state = {
            value: this.props.value
        }
        
    }

    componentWillReceiveProps(newProps){
        this.setState({value: newProps.value})
    }

    isMathExpression(value){
        const ptn = /^[0-9+\-*/()\s]+$/gm
        return ptn.test(value)
    }

    onBlur(e){
        if (this.props.onBlur){
            this.props.onBlur(e)
        }
    }

    onChange(e){
        this.setState({value: e.target.value})
        if (this.props.onChange){
            this.props.onChange(e)
        }
    }

    onKeyDown(e){
        if (e.keyCode === 13){
            const value = e.target.value
            if (this.isMathExpression(value)){
                e.preventDefault()
                let newValue
                try {
                    newValue = math.eval(value)    
                } catch(e){
                    return
                }
                this.setState({value: newValue})
                this.refs.input.value = newValue
            }
        }

        if (this.props.onKeyDown){
            this.props.onKeyDown(e)
        }
    }

    onKeyPress(e){
        if (this.props.onKeyPress){
            this.props.onKeyPress(e)
        }
    }

    onKeyUp(e){
        if (this.props.onKeyUp){
            this.props.onKeyUp(e)
        }
    }

    render() {
        const { 
            disabled,
            name,
            style,
            type,
            placeholder,
            ref,
            ...other } = this.props
        const { value } = this.state
        
        let inputProps = {
            disabled,
            name,
            style,
            type,
            placeholder,
            ref,
            onBlur: this.onBlur,
            onChange: this.onChange,
            onKeyDown: this.onKeyDown,
            onKeyPress: this.onKeyPress,
            onKeyUp: this.onKeyUp
        }
        
        // if controlled component
        if (this.props.value !== null && this.props.value !== undefined){
            inputProps.value = value
        }

        return (
            <FieldWrapper name={name} {...other}>    
                <input {...inputProps}  />
            </FieldWrapper>
        )
    }
}


Input.propTypes = {
    disabled: PropTypes.bool,
    errors: PropTypes.object,
    helpString: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onKeyDown: PropTypes.func,
    onKeyPress: PropTypes.func,
    onKeyUp: PropTypes.func,
    placeholder: PropTypes.string,
    ref: PropTypes.string,
    style: PropTypes.object,
    type: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
}
