import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'


export default class ErrorBlock extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const errors = this.props.errors

        if (errors === null || !errors.hasOwnProperty('errors') || errors.errors.length === 0)
            return null

        return (

            <div className="form-error-text-block" data-error-list>
                <ul>
                   {errors.errors.map( (e, index) => {
                        return <li key={index}>{e}</li>
                   })}
                </ul>
            </div>
        )
    }
}


ErrorBlock.propTypes = {
    errors: PropTypes.object
}
