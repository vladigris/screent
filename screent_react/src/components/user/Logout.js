import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'



@inject('user')
@observer
export default class Logout extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillMount(){
        this.props.user.logout()
    }

    render(){
        return <Loading />
    }
}


Logout.propTypes = {
    user: MPropTypes.observableObject
}
