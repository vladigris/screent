import React from 'react'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { BreadCrumbs } from 'components'




@inject('user')
@observer
export default class UserProfile extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    get breadCrumbs(){
        return [
            ['User Profile']
        ]
    }

    render(){
        const { user } = this.props

        return (
            <wrap>
                <h3>{'My profile'}</h3>

                <BreadCrumbs
                    items={this.breadCrumbs} />
                
                <div className="row">
                    <div className="col-lg-6">
                        <section className="box-typical steps-numeric-block">
                            
                            <table className="table table-bordered table-hover table-sm">
                            <tbody>
                                <tr>
                                    <td  style={{width: '200px'}}>{'Name'}</td>
                                    <td>{user.me.firstName}</td>
                                    <td style={{width: '80px'}}>
                                        <div className="text-center">
                                            <Link to="/edit_profile">
                                                <i className="fa fa-pointer fa-pencil" />    
                                            </Link>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td  style={{width: '200px'}}>{'Email'}</td>
                                    <td>{user.me.email}</td>
                                    <td style={{width: '80px'}}>
                                        <div className="text-center">
                                            <Link to="/change_email">
                                                <i className="fa fa-pointer fa-pencil" />    
                                            </Link>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td colSpan="3">
                                        <div className="text-right">
                                            <Link to="/change_password">
                                                {'Change password'}
                                            </Link>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            </table>
                            
                        </section>
                    </div>
                </div>
                
            </wrap>
        )
    }   
}


UserProfile.propTypes = {
    user: MPropTypes.observableObject
}






        