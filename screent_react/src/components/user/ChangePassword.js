import React from 'react'
import autoBind from 'react-autobind'
import { Redirect } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'
import {
    Input,
    Button,
    BreadCrumbs,
    ErrorBlock } from 'components'



@inject('alert', 'user')
@observer
export default class ChangePassword extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillUnmount(){
        this.props.user.clear()   
    }

    componentWillReact(){
        const { alert, user } = this.props
        if (user.isChangePasswordSuccess){
            alert.add('success', 'Password has been changed!')
        }
    }   

    get breadCrumbs(){
        return [
            ['User Profile', '/profile'],
            ['Change password']
        ]
    }

    onSubmit(e){
        e.preventDefault()
        let form = formToObject(e.target)
        this.props.user.changePassword(form)
    }

    render(){
        const { user } = this.props

        if (user.isChangePasswordSuccess){
            return <Redirect to="/profile" />
        }

        return (
            <wrap>
                <h3>{'Change password'}</h3>

                <BreadCrumbs
                    items={this.breadCrumbs} />
                
                <div className="row">
                    <div className="col-lg-6">

                        <section className="box-typical steps-numeric-block">
                            
                            <div className="steps-numeric-inner">
                                
                                <form ref="form" onSubmit={this.onSubmit}>
                                    <Input
                                        label="Current password"
                                        name="currentPassword"
                                        type="password"
                                        errors={user.errors}
                                        placeholder="Current password" />

                                    <Input
                                        label="New password"
                                        name="newPassword1"
                                        type="password"
                                        errors={user.errors}
                                        placeholder="New password" />

                                    <Input
                                        label="Confirm"
                                        name="newPassword2"
                                        type="password"
                                        errors={user.errors}
                                        placeholder="Confirm new password" />

                                    <Button
                                        isLoading={user.isChangePasswordInProgress} />
                                
                                    <br />
                                    <ErrorBlock errors={user.errors} />
                                </form>
                            
                            </div>
                        </section>

                    </div>
                </div>
                
            </wrap>
        )
    }
}


ChangePassword.propTypes = {
    alert: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
