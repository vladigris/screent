import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'



@inject('user')
@observer
export default class ChangeEmailConfirm extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillMount(){
        const { user, match } = this.props
        user.clear()

        const p = match.params
        const form = {guid: p.guid, token: p.token}
        
        user.changeEmailConfirm(form)
    }

    componentWillUnmount(){
        this.props.user.clear()   
    }

    renderResult(){
        let { user } = this.props
        const cn = classNames(
            'error-title',
            {red: user.errors, green: !user.errors}
        )

        let message = 'Email changed.'

        if (user.errors){
            message = user.errors.errors[0]
        }

        return (
            <wrap>
                <div className={cn}>{message}</div>
                <Link
                    to="/"
                    className="btn btn-rounded">
                    {'Home page'}
                </Link>
            </wrap>
        )
        
    }

    render(){
        const { user } = this.props

        return (
            <div className="page-center">
                <div className="page-center-in">
                    <div className="container-fluid">

                        <div className="page-error-box">
                            {user.isConfirmChangeEmailFinished ?
                                this.renderResult() : <Loading />}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


ChangeEmailConfirm.propTypes = {
    match: PropTypes.object,
    user: MPropTypes.observableObject
}
