import React from 'react'
import autoBind from 'react-autobind'
import { Redirect } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'
import {
    Input,
    Button,
    BreadCrumbs,
    ErrorBlock } from 'components'



@inject('user')
@observer
export default class EditProfile extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillUnmount(){
        this.props.user.clear()   
    }

    get breadCrumbs(){
        return [
            ['User Profile', '/profile'],
            ['Edit']
        ]
    }

    onSubmit(e){
        e.preventDefault()
        let form = formToObject(e.target)
        this.props.user.editUser(form)
    }

    render(){
        const { user } = this.props

        if (user.isEditUserSuccess){
            return <Redirect to="/profile" />
        }

        return (
            <wrap>
                <h3>{'Edit profile'}</h3>

                <BreadCrumbs
                    items={this.breadCrumbs} />
                
                <div className="row">
                    <div className="col-lg-6">

                        <section className="box-typical steps-numeric-block">
                            
                            <div className="steps-numeric-inner">
                                
                                <form ref="form" onSubmit={this.onSubmit}>
                                    <Input
                                        label="Name"
                                        name="firstName"
                                        errors={user.errors}
                                        placeholder="Your name"
                                        value={user.me.firstName} />

                                    <Button
                                        isLoading={user.isEditUserInProgress} />
                                
                                <ErrorBlock errors={user.errors} />
                                </form>
                            
                            </div>
                        </section>

                    </div>
                </div>
                
            </wrap>
        )
    }
}


EditProfile.propTypes = {
    user: MPropTypes.observableObject
}
