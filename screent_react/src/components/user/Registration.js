import React from 'react'
import autoBind from 'react-autobind'
import { Link, Redirect } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { validateEmail, formToObject } from 'services/helpers'
import { Input, ErrorBlock, Alert, Button, Loading } from 'components'



@inject('t', 'user')
@observer
export default class Registration extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillUnmount(){
        this.props.user.clear()   
    }

    componentWillReact(){
        const { user } = this.props

        if (user.isSignupSuccess){
            this.refs.form.reset()
        }
    }

    onSubmit(e){
        e.preventDefault()
        
        const { t, user } = this.props

        user.clear()
        const form = formToObject(e.target)

        if (form.firstName.trim().length === 0){
            user.setErrors({firstName: [t.data['109']]})
            return
        }

        if (form.email.trim().length === 0){
            user.setErrors({email: [t.data['109']]})
            return
        }

        if (!validateEmail(form.email.trim())){
            user.setErrors({email: [t.data['110']]})
            return   
        }

        if (form.password.trim().length === 0){
            user.setErrors({password: [t.data['109']]})
            return
        }

        if (form.password.trim().length < 8){
            user.setErrors({password: [t.data['111']]})
            return
        }

        if (form.password2.trim().length === 0){
            user.setErrors({password2: [t.data['112']]})
            return
        }

        if (form.password.trim() !== form.password2.trim()){
            user.setErrors({password2: [t.data['113']]})
            return
        }
        user.signUp(form)
    }

    render(){
        const message = `
            You have successfully registered on the site and we sent  
            you welcome email! Please follow the link in the email 
            to confirm your registration.`

        const { user, t } = this.props

        if (!t.data){
            return <Loading />
        }

        if (user.isAuthenticated){
            return <Redirect to="/" />
        }

        return (
            <div className="page-center">
                <div className="page-center-in">
                    <div className="container-fluid">
                        <form ref="form" onSubmit={this.onSubmit} className="sign-box">
                            <div className="sign-avatar">
                                <img style={{borderRadius: 0}} src="img/avatar-sign.png" alt="" />
                            </div>
                            <header className="sign-title">{t.data['108']}</header>
                            
                            {!user.isSignupSuccess &&
                                <wrap>
                                    <Input
                                        name="firstName"
                                        errors={user.errors}
                                        placeholder={t.data['114']} />

                                    <Input
                                        name="email"
                                        errors={user.errors}
                                        placeholder={t.data['115']} />
                                    
                                    <Input
                                        name="password"
                                        errors={user.errors}
                                        type="password"
                                        placeholder={t.data['116']} />
                                    
                                    <Input
                                        name="password2"
                                        errors={user.errors}
                                        type="password"
                                        placeholder={t.data['117']} />
                               
                                    <Button
                                        type="submit"
                                        className="btn btn-rounded btn-success sign-up"
                                        isColumns={false}
                                        text={t.data['118']} />

                                    <ErrorBlock errors={user.errors} />

                                </wrap>
                            }
                            
                            {user.isSignupSuccess &&
                                <Alert
                                    type="success"
                                    message={message} />}
                            

                            <p className="sign-note">
                                {t.data['119']} <Link to="/sign-in">{t.data['120']}</Link>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

Registration.propTypes = {
    t: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}

            