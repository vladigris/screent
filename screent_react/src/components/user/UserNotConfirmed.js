import React from 'react'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Alert } from 'components'



@inject('t', 'user')
@observer
export default class UserNotConfirmed extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentDidMount(){
        for (let name of Array.from(document.body.classList)){
            document.body.classList.remove(name)
        }
    }   

    componentWillUnmount(){
        this.props.user.clear()   
    }

    onClickResend(e){
        e.preventDefault()
        this.props.user.resendConfirmationLink()
    }

    showButton(){
        const { user } = this.props
        if (!user.isResendConfirmationLinkInProgress && !user.resendConfirmationMessage){
            return true
        }
        return false
    }

    render(){
        const { t, user } = this.props
        
        if (!t.data){
            return null
        }

        return (
            <div className="page-center">
                <div className="page-center-in">
                    <div className="container-fluid">

                        <div className="page-error-box">
                            
                            <Link to="/">
                                <img
                                    src="/img/logo.png"
                                    className="m-b"
                                    alt="logo" />
                            </Link>
                            
                            <div className="error-title">{t.data['089']}</div>
                            <p>{t.data['090']}</p>

                            {this.showButton() &&
                                <a href="#" 
                                    onClick={this.onClickResend}
                                    className="btn">

                                    {t.data['091']}
                                </a>
                            }

                            {user.resendConfirmationMessage && 
                                <Alert
                                    type="success"
                                    message={user.resendConfirmationMessage} />}

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


UserNotConfirmed.propTypes = {
    t: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
