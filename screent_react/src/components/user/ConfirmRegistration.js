import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'



@inject('user')
@observer
export default class ConfirmRegistration extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillMount(){
        const { user, match } = this.props
        user.clear()

        const p = match.params
        const form = {guid: p.guid, confirmationToken: p.token}

        this.props.user.confirmRegistration(form)
    }

    componentWillUnmount(){
        this.props.user.clear()
    }
    
    renderResult(){
        const { user } = this.props
        let message = 'You have successfully confirmed your registration.'

        if (user.errors){
            message = user.errors.errors[0]
        }

        const cn = classNames(
            'error-title',
            {red: user.errors, green: !user.errors}
        )

        return (
            <wrap>
                <div className={cn}>{message}</div>
                <Link
                    to="/sign-in"
                    className="btn btn-rounded">
                    {'Sign in'}
                </Link>
            </wrap>
        )
        
    }
    
    render(){
        const { user } = this.props

        return (
            <div className="page-center">
                <div className="page-center-in">
                    <div className="container-fluid">

                        <div className="page-error-box">
                            {user.isConfirmRegistrationFinished ? 
                                this.renderResult() 
                            : 
                                <Loading /> 
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


ConfirmRegistration.propTypes = {
    match: PropTypes.object,
    user: MPropTypes.observableObject
}




