import React from 'react'
import moment from 'moment'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'
import { Input, Button, BreadCrumbs, ErrorBlock } from 'components'



@inject('user')
@observer
export default class ChangeEmail extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillUnmount(){
        this.props.user.clear()   
    }

    onSubmit(e){
        const { user } = this.props
        
        e.preventDefault()
        let form = formToObject(e.target)
        user.changeEmail(form)
    }

    get breadCrumbs(){
        return [
            ['User Profile', '/profile'],
            ['Change email']
        ]
    }

    isShowForm(){
        const { user } = this.props
        
        if (user.me.newEmail && user.me.newEmailExpireAt > moment()){
            return false
        }
        return true
    }

    renderAwaitConfirm(){
        const { user } = this.props

        return (
            <wrap>
                <p>{'New email '}<b>{user.me.newEmail}</b>{' is not confirmed!'}</p>
                <p>{'To confirm email please follow the link that we sent you.'}</p>
            </wrap>
        )
    }

    renderForm(){
        const { user } = this.props

        return (
            <form ref="form" onSubmit={this.onSubmit}>
                <Input
                    label="New email"
                    name="newEmail"
                    errors={user.errors}
                    placeholder="Input new email" />

                <Button
                    isLoading={user.isChangeEmailInProgress} />
            
                <br />
                <ErrorBlock errors={user.errors} />
            </form>
        )
    }

    render(){
        return (
            <wrap>
                <h3>{'Change email'}</h3>

                <BreadCrumbs
                    items={this.breadCrumbs} />
                
                <div className="row">
                    <div className="col-lg-6">

                        <section className="box-typical steps-numeric-block">
                            
                            <div className="steps-numeric-inner">
                                {this.isShowForm() 
                                    ? this.renderForm()
                                    : this.renderAwaitConfirm()}

                            </div>
                        </section>

                    </div>
                </div>
                
            </wrap>
        )
    }
}


ChangeEmail.propTypes = {
    user: MPropTypes.observableObject
}
