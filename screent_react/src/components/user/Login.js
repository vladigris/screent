import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import { Link, Redirect } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'
import { Button, ErrorBlock, Input, Loading } from 'components'


@inject('t', 'user')
@observer
export default class Login extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }
    
    componentWillUnmount(){
        this.props.user.clear()   
    }

    onSubmit(e){
        const { user } = this.props
        e.preventDefault()
        const form = formToObject(e.target)
        user.login(form)
    }

    render(){
        const { t, user } = this.props

        if (!t.data){
            return <Loading />
        }

        if (user.isAuthenticated){
            return <Redirect to="/" />
        }
        
        return (
            <div className="page-center">
                <div className="page-center-in">
                    <div className="container-fluid">
                        <form onSubmit={this.onSubmit} className="sign-box">
                            <div className="sign-avatar">
                                <img style={{borderRadius: 0}} src="img/avatar-sign.png" alt="" />
                            </div>
                            <header className="sign-title">{t.data['097']}</header>
                            
                            <Input
                                name="email"
                                errors={user.errors}
                                placeholder={t.data['100']} />
                            
                            <Input
                                name="password"
                                errors={user.errors}
                                type="password"
                                placeholder={t.data['101']} />
                           
                            <Button
                                type="submit"
                                className="btn btn-rounded"
                                isColumns={false}
                                text={t.data['107']} />

                            <ErrorBlock errors={user.errors} />

                            <p className="sign-note">
                                {t.data['102']}{' '}
                                <Link to="/sign-up">{t.data['103']}</Link>
                            </p>

                        </form>
                    </div>
                
                </div>
            </div>
        )
    }
}


Login.propTypes = {
    t: PropTypes.object,
    user: PropTypes.object
}
