import React from 'react'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'




@inject('computer', 't')
@observer
export default class HomeNoScreens extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    replace(text){
        const { computer } = this.props
        const name = `"${computer.activeComputer.name}"`
        text = text.replace('[COMPUTERNAME]', name)
        return text
    }

    render(){
        const { computer, t } = this.props

        if (!computer.activeComputer){
            return <Loading />
        }

        return (
            <div className="box-typical box-typical-full-height">
                <div className="add-customers-screen tbl">
                    <div className="add-customers-screen-in">
                        <div className="add-customers-screen-user">
                            <i className="font-icon fa fa-desktop" />
                        </div>
                        <h2>{t.data['080']}</h2>
                        <p className="lead color-blue-grey-lighter">
                            {this.replace(t.data['081'])}
                        </p>

                        <a
                            href={computer.activeComputer.version.installerLink}
                            className="btn">
                            {t.data['084']}
                        </a>
                        <span className="p-l">
                            <Link className="btn" to="/faq">
                                {t.data['088']}
                            </Link>
                        </span>
                        
                    </div>
                </div>
            </div>
        )
    }
}


HomeNoScreens.propTypes = {
    computer: MPropTypes.observableObject,
    t: MPropTypes.observableObject
}

