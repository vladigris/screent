import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import Filter from 'services/Filter'
import {
    CheckboxToggle,
    RadioButtons,
    QuestionTip } from 'components'

/**
 * Control panel in right sidebar.
 * Control panel is not rendered, if there is no activeComputer.
 * Load initial screenshots on component mount.
 * Other loading processed during browsing or change filters.
 */
@inject('computer', 'screen')
@observer
export default class FilterPanel extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            isOpen: false
        }
    }

    componentWillMount(){
        const { computer, screen } = this.props
        computer.getFilter()
        screen.load('manual', 'next')  
    }

    clean(value){
        return value.replace('<', '').replace('>', '')
    }

    onUpdateTitles(e){
        const { computer } = this.props
        let f = new Filter(computer.activeGuid)
        f.setTitles(this.clean(e.target.value))
        f.save()
        computer.setFilter(f.toObject())
    }

    onKeyDownTitles(e){
        const { screen } = this.props
        if (e.keyCode === 13){
            screen.stopAndClear()
            screen.reload()
        }
    }

    onBlurTitles(){
        const { screen } = this.props
        screen.stopAndClear()
        screen.reload()
    }

    onUpdateKeyboards(e){
        const { computer } = this.props
        let f = new Filter(computer.activeGuid)
        f.setKeyboards(this.clean(e.target.value))
        f.save()
        computer.setFilter(f.toObject())
    }

    onKeyDownKeyboards(e){
        const { screen } = this.props
        if (e.keyCode === 13){
            screen.stopAndClear()
            screen.reload()
        }
    }

    onBlurKeyboards(){
        const { screen } = this.props
        screen.stopAndClear()
        screen.reload()
    }


    onUpdatePrograms(e){
        const { computer } = this.props
        let f = new Filter(computer.activeGuid)
        f.setPrograms(this.clean(e.target.value))
        f.save()
        computer.setFilter(f.toObject())
    }

    onKeyDownPrograms(e){
        const { screen } = this.props
        if (e.keyCode === 13){
            screen.stopAndClear()
            screen.reload()
        }
    }

    onBlurPrograms(){
        const { screen } = this.props
        screen.stopAndClear()
        screen.reload()
    }

    onClickShowTitle(){
        const { computer } = this.props
        let f = new Filter(computer.activeGuid)
        f.setIsShowTitle(!f.isShowTitle)
        f.save()
        computer.setFilter(f.toObject())
    }

    onClickShowKeyboard(){
        const { computer } = this.props
        let f = new Filter(computer.activeGuid)
        f.setIsShowKeyboard(!f.isShowKeyboard)
        f.save()
        computer.setFilter(f.toObject())
    }

    onClickShowTime(){
        const { computer } = this.props
        let f = new Filter(computer.activeGuid)
        f.setIsShowTime(!f.isShowTime)
        f.save()
        computer.setFilter(f.toObject())
    }

    onClickDelay(value){
        const { computer } = this.props
        let f = new Filter(computer.activeGuid)
        f.setDelay(value)
        f.save()
        computer.setFilter(f.toObject())   
    }

    togglePanel(e){
        e.preventDefault()
        const { isOpen } = this.state
       
        this.setState({isOpen: !isOpen})

        if (!isOpen){
            document.body.classList.add('open')    
        } else {
            document.body.classList.remove('open')
        }
    }

    /**
     * @returns {array} - array of objects {value: ..., checked: ...}
     */
    getPrograms(){
        const { computer } = this.props
        return computer.activeComputer.programs.map((prog) => {
            return {
                value: prog,
                checked: computer.filter.programs.indexOf(prog) > -1
            }
        })
    }

    render(){
        const { isOpen } = this.state
        const { computer, screen } = this.props

        if (!computer.filter){
            return null
        }

        const cnBtnTogglePanel = classNames('control-panel-toggle', {open: isOpen})

        let showTotalCount = screen.totalCount !== null ? ` (${screen.totalCount})` : null
        
        return (
            <div className="control-panel-container">
                <ul>
                    <li className="tasks">
                        <div className="control-item-header" onClick={this.togglePanel}>
                            <a href="#" className="icon-toggle">
                                <span className="icon fa fa-desktop" />
                            </a>
                            <span className="text">
                                {'Filter'} {showTotalCount}
                            </span>
                        </div>
                        <div className="control-item-content open">
                            
                            <div className="form-group">
                                <label className="form-label">
                                    {'Keyboard input '}
                                    <QuestionTip
                                        text="Filter by words or phrases that were pressed on keyboard."
                                        place="left"
                                        type="light" />
                                </label>
                                <div className="form-control-wrapper form-control-icon-right">
                                    <textarea
                                        style={{height: '140px'}}
                                        onKeyDown={this.onKeyDownKeyboards}
                                        onBlur={this.onBlurKeyboards}
                                        onChange={this.onUpdateKeyboards}
                                        className="form-control"
                                        placeholder="One word/phrase per line"
                                        value={computer.filter.keyboards.join('\n')} />
                                    <i className="font-icon fa fa-keyboard-o" />
                                </div>
                            </div>

                            <div className="form-group">
                                <label className="form-label">
                                    {'Screenshot title '}
                                    <QuestionTip
                                        text="Filter by words or phrases in screenshot title."
                                        place="left"
                                        type="light" />
                                </label>
                                <div className="form-control-wrapper form-control-icon-right">
                                    <textarea
                                        style={{height: '140px'}}
                                        onKeyDown={this.onKeyDownTitles}
                                        onBlur={this.onBlurTitles}
                                        onChange={this.onUpdateTitles}
                                        type="text"
                                        className="form-control"
                                        placeholder="One word/phrase per line"
                                        value={computer.filter.titles.join('\n')} />
                                    <i className="font-icon fa fa-search" />
                                </div>
                            </div>

                            <div className="form-group">
                                <label className="form-label">
                                    {'Program '}
                                    <QuestionTip
                                        text="Filter by program."
                                        place="left"
                                        type="light" />
                                </label>
                                <div className="form-control-wrapper form-control-icon-right">
                                    <textarea
                                        style={{height: '140px'}}
                                        onKeyDown={this.onKeyDownPrograms}
                                        onBlur={this.onBlurPrograms}
                                        onChange={this.onUpdatePrograms}
                                        type="text"
                                        className="form-control"
                                        placeholder="One program per line"
                                        value={computer.filter.programs.join('\n')} />
                                    <i className="font-icon fa fa-search" />
                                </div>
                            </div>

                            <CheckboxToggle
                                checked={computer.filter.isShowTitle}
                                title="Show title on screen"
                                onClick={this.onClickShowTitle} />

                            <CheckboxToggle
                                checked={computer.filter.isShowKeyboard}
                                title="Show keyboard on screen"
                                onClick={this.onClickShowKeyboard} />

                            <CheckboxToggle
                                checked={computer.filter.isShowTime}
                                title="Show time on screen"
                                onClick={this.onClickShowTime} />

                            <RadioButtons
                                onUpdate={this.onClickDelay}
                                label="Delay in auto play, seconds"
                                values={computer.filter.delayRange}
                                selected={computer.filter.delay}
                                isSmall />

                        </div>
                    </li>
                   
                </ul>
                <a
                    className={cnBtnTogglePanel}
                    onClick={this.togglePanel}>
                    <span className="fa fa-angle-double-left" />
                </a>
            </div>
        )
    }
}


FilterPanel.propTypes = {
    computer: MPropTypes.observableObject,
    screen: MPropTypes.observableObject
}

