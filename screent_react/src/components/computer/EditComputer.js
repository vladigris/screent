import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'
import {
    Alert,
    ButtonSimple,
    CheckboxToggle,
    ErrorBlock,
    Input,
    Modal,
    RemoveComputer,
    Select2 } from 'components'




@inject('computer', 'program', 't')
@observer
export default class EditComputer extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            name: this.props.item.name,
            interval: this.props.item.interval,
            showModal: false
        }
    }

    componentWillUnmount(){
        this.props.computer.clear()   
    }

    onChangeName(e){
        this.setState({name: e.target.value})
    }

    onChangeInterval(e){
        this.setState({interval: e.target.value})
    }

    onSubmit(e){
        const { item } = this.props

        e.preventDefault()
        let form = formToObject(e.target)
        form.guid = item.guid
        form.interval = parseInt(form.interval, 10)

       
        // Programs and phrases should be an array
        form.programs = form.programs.length > 0 ? form.programs.split(',') : []
        form.phrases = form.phrases.length > 0 ? form.phrases.split(',') : []

        this.props.computer.edit(form)
    }

    onClickPause(){
        const { item } = this.props
        const form = {
            guid: item.guid,
            isPause: !item.isPause
        }
        this.props.computer.editPause(form)
    }

    /**
     * Show modal window to remove computer.
     */
    onClickRemove(){
        this.setState({showModal: true})
    }

    onClickCloseModal(){
        this.setState({showModal: false})   
    }

    render(){
        const { name, interval, showModal } = this.state
        const { computer, item, program, t } = this.props

        return (
            <div className="row">
            <div className="col-lg-6">

                <Modal
                    visible={showModal}
                    title={t.data['093']}
                    onClickClose={this.onClickCloseModal}>
                    
                    <RemoveComputer
                        item={item}
                        onCancel={this.onClickCloseModal} />
                </Modal>

                <form ref="form" onSubmit={this.onSubmit}>
                    <Input
                        onChange={this.onChangeName}
                        name="name"
                        label="Computer name"
                        placeholder="Computer name"
                        errors={computer.errors}
                        value={name} />

                    <Input
                        name=""
                        label="Screener name"
                        placeholder="Screener name"
                        value={item.progName}
                        disabled />

                    <Input
                        name=""
                        label="Screener folder"
                        placeholder="Screener folder"
                        value={item.progDir}
                        disabled />

                    <Input
                        onChange={this.onChangeInterval}
                        name="interval"
                        label="Interval, sec"
                        placeholder="Interval between screenshots, seconds"
                        errors={computer.errors}
                        value={interval} />

                    <Select2
                        wrapperClassName="m-y"
                        name="programs"
                        options={program.options}
                        regex={/^[a-z0-9\s]+$/gi}
                        maxItems={20}
                        initialItems={item.programs}
                        placeholder="Type programs" />
                    
                    <Select2
                        wrapperClassName="m-y"
                        name="phrases"
                        regex={/^[a-z0-9\s]+$/gi}
                        maxItems={50}
                        initialItems={item.phrases}
                        placeholder="Type phrases" />

                    <div className="form-group">
                        <ButtonSimple
                            isLoading={computer.isEditComputerInProgress}
                            type="submit"
                            className="btn btn-primary btn-inline"
                            text="Edit computer" />
                        
                        <CheckboxToggle
                            disabled={computer.isEditPauseInProgress}
                            isInline
                            checked={item.isPause}
                            title={item.pauseTitle}
                            onClick={this.onClickPause} />    
                        
                        <ButtonSimple
                            type="button"
                            className="btn btn-danger btn-inline"
                            text="Remove computer"
                            onClick={this.onClickRemove} />
                    </div>


                    <br />
                    <ErrorBlock errors={computer.errors} />

                    {computer.isEditComputerSuccess &&
                        <Alert
                            type="success"
                            message="Edited!" />}


                </form>
            
            </div>
            </div>
        )
    }
}


EditComputer.propTypes = {
    computer: MPropTypes.observableObject,
    item: PropTypes.object, // ComputerModel object
    program: MPropTypes.observableObject,
    t: MPropTypes.observableObject
}

