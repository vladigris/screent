import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'


const style = {
    position: 'absolute',
    left: '-20000px',
    top: '-20000px'
}

/**
 * Preload images to hidden div.
 * Map in internal state: key: image src, value: null
 * When "screens" Map is updated, preload images 
 * that not yet in internal state.
 */
@inject('screen')
@observer
export default class ImageCacher extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const { screen } = this.props

        let m = screen.imageCacheMap

        return (
            <div style={{position: 'relative'}}>
                <div style={style}>
                    {Array.from(m.keys()).map(src => {
                        return <img key={src} src={src} alt="" />
                    })}
                </div>
            </div>
        )
    }
}


ImageCacher.propTypes = {
    screen: MPropTypes.observableObject
}

