import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'
import { logMessage } from 'services'
import Swipe from 'react-easy-swipe'


@inject('computer', 'screen', 't')
@observer
export default class HomeMain extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            screenHeight: 0
        }
    }

    componentDidMount(){
        this.checkScreenIsRendered()        
    }

    componentWillReact(){
        const { screen } = this.props

        if (!screen.activeScreenKey){
            return
        }

        logMessage(
            screen.activeScreen,
            `${screen.pagination.total}/${screen.keyboards.size}`,
            `${screen.pagination.page}/${screen.pagination.totalPages}`
        )
    }

    onSwipeLeft(e){
        if (!this.isSwipeAllowed()){
            return
        }
        this.props.screen.playNext('manual', 'next')
    }

    onSwipeRight(e){
        if (!this.isSwipeAllowed()){
            return
        }
        this.props.screen.playNext('manual', 'prev')
    }

    /**
     * @returns {bool}
     */ 
    isSwipeAllowed(){
        const { computer, screen } = this.props
        if (!computer.activeComputer || screen.isPlaying){
            return false
        }
        return true
    }

    /**
     * Recursion function. Checks that first screen has been rendered.
     * As soon as screen is rendered, get height of the screen
     * and set height to state.
     */
    checkScreenIsRendered(){
        let img = this.refs.screen
        if (!img || img.offsetHeight === 0){
            setTimeout(() => this.checkScreenIsRendered(), 300)
            return
        }
        this.setState({screenHeight: img.offsetHeight})
    }

    isShowLeftCorner(){
        let { computer } = this.props
        if (!computer.filter){
            return false
        }

        if (computer.filter.isShowTime || computer.filter.isShowTitle){
            return true
        }
        return false
    }

    isShowKeyboard(){
        const { computer, screen } = this.props
        
        if (computer.filter.isShowKeyboard && screen.activeKeyboards.length > 0){
            return true
        }
        return false
    }

    render(){
        let { computer, screen } = this.props
       
        let keyboardsStyle = {}
        if (this.state.screenHeight > 0){
            keyboardsStyle.maxHeight = this.state.screenHeight
        }

        return (
            <div className="screen_wrapper">
                {this.isShowLeftCorner() &&
                    <div className="title">
                        {computer.filter.isShowTime && 
                            screen.activeScreen.createdAt.format('DD MMM HH:mm:ss')}
                            {computer.filter.isShowTitle && <span>{screen.activeScreen.title}</span>}
                    </div>
                }

                {this.isShowKeyboard() &&
                    <div className="keyboards" style={keyboardsStyle}>
                        {screen.activeKeyboards.map(obj => {
                            return (
                                <div key={obj.guid} className="keyboard">
                                    <div className="time">
                                        {obj.createdAt.format('HH:mm')}
                                    </div>
                                    {rHTML(obj.text)}
                                </div>
                            )
                        })}
                    </div>
                }
                
                <Swipe
                    onSwipeLeft={this.onSwipeLeft}
                    onSwipeRight={this.onSwipeRight}>
                    
                    <img
                        alt=""
                        ref="screen"
                        style={{maxWidth: '100%'}}
                        src={screen.activeScreen.src} />
                </Swipe>
            </div>
        )
    }
}

HomeMain.propTypes = {
    computer: MPropTypes.observableObject,
    keyboard: MPropTypes.observableObject,
    screen: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
}
