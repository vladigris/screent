import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import Swipe from 'react-easy-swipe'
import { rHTML } from 'services/helpers'
import { Loading, HomeInfo } from 'components'



@inject('computer', 'screen', 't')
@observer
export default class HomeDefault extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    onSwipeLeft(e){
        const { computer, screen } = this.props
        if (!computer.activeComputer || screen.isPlaying){
            return
        }
        this.props.screen.playNext('manual', 'next')
    }

    replace(text){
        text = text.replace('[PLAY]', '<i class="fa fa-play"></i>')
        text = text.replace('[ARROWLEFT]', '<i class="fa fa-long-arrow-left"></i>')
        text = text.replace('[ARROWRIGHT]', '<i class="fa fa-long-arrow-right"></i>')
        return text        
    }

    render(){
        let { screen, t } = this.props

        if (screen.isLoading){
            return <Loading />
        }

        return (
            <wrap>
                <HomeInfo />

                {(screen.totalCount === null || screen.totalCount > 0) &&
                    <Swipe
                        onSwipeLeft={this.onSwipeLeft}
                        onSwipeRight={this.onSwipeRight}>
                
                        <div className="box-typical box-typical-full-height" style={{minHeight: '419px'}}>
                            
                            <div className="add-customers-screen tbl" style={{minHeight: '419px'}}>
                                <div className="add-customers-screen-in">
                                    <div className="add-customers-screen-user">
                                        <i className="font-icon fa fa-desktop" />
                                    </div>
                                    <p className="lead color-blue-grey-lighter">
                                        {rHTML(this.replace(t.data['083']))}
                                    </p>
                                </div>
                            </div>
                        </div>    
                    </Swipe>
                }
            </wrap>
        )
    }
}

HomeDefault.propTypes = {
    computer: MPropTypes.observableObject,
    screen: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
}
