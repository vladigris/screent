import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { ButtonSimple } from 'components'


/**
 * Component is loaded to Modal.
 */
@inject('computer', 't')
@observer
export default class RemoveComputer extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    onClickRemove(){
        const { item } = this.props
        const form = {guid: item.guid}
        this.props.computer.remove(form)
    }

    render(){
        const { t } = this.props

        return (
            <wrap>
                <p>{t.data['094']}</p>
                <hr />
                <ButtonSimple
                    type="button"
                    className="btn btn-danger btn-inline"
                    text={t.data['095']}
                    onClick={this.onClickRemove} />

                <ButtonSimple
                    type="button"
                    className="btn btn-default btn-inline"
                    text={t.data['096']}
                    onClick={this.props.onCancel} />
            </wrap>
        )
    }
}


RemoveComputer.propTypes = {
    computer: MPropTypes.observableObject,
    item: PropTypes.object,
    onCancel: PropTypes.func,
    t: MPropTypes.observableObject,
}




