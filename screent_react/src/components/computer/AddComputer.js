import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { AddComputerForm } from 'components'


@inject('t')
@observer
export default class AddComputer extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const { t } = this.props

        return (
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-8">
                    <div className="box-typical box-typical-padding">
                        <h5 className="m-t with-border">{t.data['058']}</h5>
                        <AddComputerForm />
                    </div>
                </div>
            </div>
        )
    }
}


AddComputer.propTypes = {
    t: MPropTypes.observableObject
}

