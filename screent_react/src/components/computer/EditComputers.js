import React from 'react'
import autoBind from 'react-autobind'
import { Redirect } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Tabs, EditComputer } from 'components'



@inject('computer')
@observer
export default class EditComputers extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const { computer } = this.props

        if (computer.map.size === 0){
            return <Redirect to="/" />
        }

        let computers = Array.from(computer.map.values())

        const titles = computers.map(obj => obj.name)

        return (
            <Tabs titles={titles}>
                {computers.map(obj => {
                    return <EditComputer key={obj.guid} item={obj} />          
                })}
            </Tabs>
        )
    }
}


EditComputers.propTypes = {
    computer: MPropTypes.observableObject
}
