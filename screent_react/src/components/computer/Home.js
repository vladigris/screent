import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import {
    HomeDefault,
    HomeMain,
    HomeNoComputers,
    HomeTrial,
    HomeNoScreens,
    Loading } from 'components'


@inject('computer', 'screen', 'user')
@observer
export default class Home extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const { computer, screen, user } = this.props
        
        if (computer.map.size === 0){
            return <HomeNoComputers />
        }

        if (!user.me.isTrial){
            return <HomeTrial />
        }

        if (!computer.activeComputer){
            return <Loading />
        }

        if (computer.activeComputer.totalScreens === 0){
            return <HomeNoScreens />
        }

        if (!screen.activeScreen){
            return <HomeDefault />
        }

        return <HomeMain />
    }
}

Home.propTypes = {
    computer: MPropTypes.observableObject,
    screen: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}








