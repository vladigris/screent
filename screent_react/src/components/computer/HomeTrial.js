import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'



@inject('computer', 't', 'user')
@observer
export default class HomeTrial extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    onClickStartTrial(e){
        e.preventDefault()
        this.props.user.startTrial()
    }

    render(){
        const { computer, t } = this.props

        if (!computer.activeComputer){
            return <Loading />
        }

        return (
            <div className="box-typical box-typical-full-height">
                <div className="add-customers-screen tbl">
                    <div className="add-customers-screen-in">
                        <div className="add-customers-screen-user">
                            <i className="font-icon fa fa-link" />
                        </div>
                        <h2>{t.data['077']}</h2>
                        
                        <p className="lead color-blue-grey-lighter">
                            {t.data['078']}
                        </p>
                        <a
                            href={computer.activeComputer.version.installerLink}
                            className="btn btn-primary">
                            {t.data['087']}
                        </a>
                        
                        <p className="lead color-blue-grey-lighter m-t-lg">
                            {t.data['085']}
                        </p>
                        <a
                            onClick={this.onClickStartTrial}
                            href="#"
                            className="btn btn-danger">
                            {t.data['079']}
                        </a>

                        <p className="lead color-blue-grey-lighter m-t-lg">
                            {t.data['086']}
                        </p>
                        
                        
                    </div>
                </div>
            </div>
        )
    }

}


HomeTrial.propTypes = {
    computer: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}





    