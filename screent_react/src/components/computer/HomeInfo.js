import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'


/**
 * Info with how many screens available.
 * Showed on HomeDefault
 */
@inject('computer', 'screen')
@observer
export default class HomeInfo extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const { screen } = this.props

        if (screen.totalCount === null){
            return null
        }

        const text = screen.totalCount > 0 ? 'Screenshots' : 'Screenshot'

        return (
            <article className="statistic-box purple">
                <div>
                    <div className="number">{screen.totalCount}</div>
                    <div className="caption">
                        <div>{`${text} available.`}</div>
                    </div>
                </div>
            </article>
        )
    }
}


HomeInfo.propTypes = {
    computer: MPropTypes.observableObject,
    screen: MPropTypes.observableObject,

}


