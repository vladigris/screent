import React from 'react'
import autoBind from 'react-autobind'
import { Redirect } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'
import {
    Input,
    Select2,
    ButtonSimple,
    ErrorBlock,
    QuestionTip } from 'components'



@inject('alert', 'computer', 'program', 't')
@observer
export default class AddComputerForm extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillUnmount(){
        this.props.computer.clear()   
    }

    componentWillReact(){
        const { alert, computer } = this.props
        if (computer.isAddComputerSuccess){
            alert.add('success', 'Computer created')   
        }
    }

    onSubmit(e){
        e.preventDefault()
        let form = formToObject(e.target)
       
        // Programs and phrases should be an array
        form.programs = form.programs.length > 0 ? form.programs.split(',') : []
        form.phrases = form.phrases.length > 0 ? form.phrases.split(',') : []

        this.props.computer.add(form)
    }

    render(){
        const { computer, program, t } = this.props

        if (computer.isAddComputerSuccess){
            return <Redirect to="/" />
        }

        return (
            <form ref="form" onSubmit={this.onSubmit}>
                <label className="form-label">
                    {t.data['059']}{' '}
                    <QuestionTip
                        text="The name of computer. Can be changed anytime later."
                        place="right"
                        type="info" />
                </label>
                <Input
                    name="name"
                    placeholder={t.data['059']}
                    errors={computer.errors} />


                <label className="form-label">
                    {t.data['060']}{' '}
                    <QuestionTip
                        text="The name of Windows program that will work on computer. Can not be changed later."
                        place="right"
                        type="info" />
                </label>
                <Input
                    name="progName"
                    placeholder={t.data['060']}
                    errors={computer.errors} />

                <label className="form-label">
                    {t.data['061']}{' '}
                    <QuestionTip
                        text="The name of folder where Screener program will work. Can not be changed later."
                        place="right"
                        type="info" />
                </label>
                <Input
                    name="progDir"
                    placeholder={t.data['061']}
                    errors={computer.errors} />
    

                <label className="form-label">
                    {'Programs '}
                    <QuestionTip
                        text="DO NOT TYPE programs, unless you want to monitor particular programs. If you omit this field, screenshots will be made for all programs. You can add multiple programs. Use Enter."
                        place="right"
                        type="info" />
                </label>
                <Select2
                    wrapperClassName="m-y"
                    name="programs"
                    options={program.options}
                    regex={/^[a-z0-9\s]+$/gi}
                    maxItems={20}
                    placeholder={t.data['062']} />


                <label className="form-label">
                    {'Phrases '}
                    <QuestionTip
                        text="This option make sense only when you typed programs. Screenshots will be made when particular phrases were pressed on keyboard, but program is not in programs list. You can add multiple phrases."
                        place="right"
                        type="info" />
                </label>
                <Select2
                    wrapperClassName="m-y"
                    name="phrases"
                    regex={/^[a-z0-9\s]+$/gi}
                    maxItems={50}
                    placeholder={t.data['063']} />

                <ButtonSimple type="submit" />

                <br />
                <ErrorBlock errors={computer.errors} />
            </form>
        )
    }
}


AddComputerForm.propTypes = {
    alert: MPropTypes.observableObject,
    computer: MPropTypes.observableObject,
    program: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
    
}
