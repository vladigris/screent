import React from 'react'
import autoBind from 'react-autobind'
import { Link } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'




@inject('t')
@observer
export default class HomeNoComputers extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    render(){
        const { t } = this.props

        return (
            <div className="box-typical box-typical-full-height">
                <div className="add-customers-screen tbl">
                    <div className="add-customers-screen-in">
                        <div className="add-customers-screen-user">
                            <i className="font-icon fa fa-desktop" />
                        </div>
                        <p className="lead color-blue-grey-lighter">
                            {rHTML(t.data['056'])}
                        </p>
                        
                        <Link to="/add_computer" className="btn">
                            {t.data['057']}
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}


HomeNoComputers.propTypes = {
    t: MPropTypes.observableObject
}
