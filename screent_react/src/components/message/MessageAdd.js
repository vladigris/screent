import React from 'react'
import autoBind from 'react-autobind'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'




@inject('message')
@observer
export default class MessageAdd extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            subject: '',
            text: ''
        }
    }

    isReady(){
        const { subject, text } = this.state
        if (subject.trim().length === 0 || text.trim().length === 0){
            return false
        }
        return true
    }

    onChangeSubject(e){
        this.setState({subject: e.target.value})
    }

    onChangeText(e){
        this.setState({text: e.target.value})
    }

    onClickClear(){
        this.setState({
            subject: '',
            text: ''
        })
    }

    onClickSend(e){
        e.preventDefault()
        const { subject, text } = this.state
        const form = {subject, text}
        this.props.message.create(form)
    }

    render(){
        const { subject, text } = this.state
        const { message } = this.props

        return (
            <div className="chat-area-bottom">
                <form className="write-message">
                    <div className="form-group">
                        <input
                            onChange={this.onChangeSubject}
                            className="form-control"
                            placeholder="Subject"
                            value={subject} />
                    </div>

                    <div className="form-group">
                        <textarea
                            onChange={this.onChangeText}
                            rows="3"
                            className="form-control"
                            placeholder="Type a message"
                            value={text} />
                    </div>
                    <button
                        disabled={!this.isReady() || message.isCreateInProgress}
                        onClick={this.onClickSend}
                        type="submit"
                        className="btn btn-rounded float-left">
                    
                        {'Send'}
                    </button>
                    
                    <button
                        onClick={this.onClickClear}
                        type="reset"
                        className="btn btn-rounded btn-default float-left">

                        {'Clear'}
                    </button>
                </form>
            </div>
        )
    }
}


MessageAdd.propTypes = {
    message: MPropTypes.observableObject
}


