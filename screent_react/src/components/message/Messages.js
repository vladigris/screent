import React from 'react'
import autoBind from 'react-autobind'
import classNames from 'classnames'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading, Message, MessageAdd } from 'components'



let style= {
    overflowY: 'scroll',
    padding: '0px',
    width: '372px',
    height: '300px'
}

// "y" coordinate of button
const btnY = 170



/**
 * Load messages from server on mount.
 * Then fetch additional messages on scroll.
 */
@inject('message')
@observer
export default class Messages extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)
    }

    componentWillMount(){
        this.loadMessages(true)
    }

    loadMessages(isFirstLoad=false){
        this.props.message.load(isFirstLoad)
    }

    onClickAdd(){
        this.props.message.setActive(null)
        this.props.message.setShowAddForm(true)
    }

    onClickMessage(guid){
        this.props.message.setActive(guid)
        this.props.message.setRead(guid)
    }

    onScrollNav(e){
        if (this.props.message.isLoading){
            return
        }

        const el = e.target
        if (el.scrollTop + el.offsetHeight > el.scrollHeight - 50){
            this.loadMessages()
        }
    }
    
    /**
     * Calculate height of the side bar.
     */
    get height(){
        let windowHeight = window.innerHeight
        const height = windowHeight - btnY - 90
        return `${height}px`
    }
    
    render(){
        const { message } = this.props
        
        if (message.isInitialLoading){
            return <Loading />
        }

        let newStyle = {...style}
        newStyle.height = this.height
        
        return (
            <wrap>
                <h3>{'Support'}</h3>
                <section className="box-typical mail-box">
                    <header className="mail-box-header">
                        <div className="mail-box-header-left">
                            <button
                                onClick={this.onClickAdd}
                                type="button"
                                className="btn write">
                            
                                {'Write new message'}
                            </button>
                        </div>
                    </header>

                    <section
                        onScroll={this.onScrollNav}
                        ref="nav"
                        className="mail-box-list"
                        style={newStyle}>

                        {Array.from(message.map.values()).map(msg => {
                            const cn = classNames('mail-box-item', {
                                'selected-line': message.activeGuid === msg.guid
                            })
                            const cn2 = !msg.isRead ? 'bold' : null
                            return (
                                <div
                                    key={msg.guid}
                                    className={cn}
                                    onClick={this.onClickMessage.bind(null, msg.guid)}>
                                    
                                    <div className={cn2}>{msg.subject}</div>
                                    <div><small>{msg.updatedAt}</small></div>
                                </div>                    
                            )
                        })}
                    </section>

                    <section className="mail-box-work-area">
                        <div className="mail-box-work-area-in">
                           {message.activeGuid && <Message />}  
                           {message.showAddForm && <MessageAdd />}
                        </div>
                    </section>
                </section>
            </wrap>
        )
    }
}


Messages.propTypes = {
    message: MPropTypes.observableObject
}
