import React from 'react'
import autoBind from 'react-autobind'
import classNames from 'classnames'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'




@inject('message')
@observer
export default class Message extends React.Component{
    constructor(props){
        super(props)
        autoBind(this)

        this.state = {
            text: ''
        }
    }

    onChange(e){
        this.setState({text: e.target.value})
    }

    onClickClear(){
        this.setState({text: ''})  
        this.refs.input.focus() 
    }

    onClickSend(e){
        e.preventDefault()
        const { text } = this.state
        const form = {text: text.trim()}

        this.props.message.addItem(form)
        this.setState({text: ''})
    }

    render(){
        const { message } = this.props
        const { text } = this.state

        if (!message.activeGuid){
            return null
        }

        return (
            <wrap>
                <div className="chat-area-bottom">
                    <form className="write-message">
                        <div className="form-group">
                            <textarea
                                ref="input"
                                onChange={this.onChange}
                                rows="3"
                                className="form-control"
                                placeholder="Type a message"
                                value={text} />
                        </div>
                        <button
                            disabled={text.trim().length === 0 || message.isAddItemInProgress}
                            onClick={this.onClickSend}
                            type="submit"
                            className="btn btn-rounded float-left">
                        
                            {'Send'}
                        </button>
                        
                        <button
                            onClick={this.onClickClear}
                            type="reset"
                            className="btn btn-rounded btn-default float-left">

                            {'Clear'}
                        </button>
                    </form>
                </div>

                <div className="messenger-dialog-area">
                    {message.groupedItems.map((arr, index) => {
                        let item = arr[0]
                        const cn = classNames('messenger-message-container', {
                            'from bg-blue': !item.is_client
                        })

                        return (
                            <div className={cn} key={index}>
                                <div className="messages">
                                    <ul>
                                        {arr.map(msg => {
                                            const style = !msg.isClient ? {backgroundColor: '#00a8ff'} : {backgroundColor: '#ac6bec'}
                                            return (
                                                <li key={msg.guid}>
                                                    {!msg.isClient && <div className="time-ago">{msg.createdAtString}</div>} 
                                                    <div className="message">
                                                        <div style={style}>
                                                            {msg.text}                                              
                                                        </div>
                                                    </div>
                                                    {msg.isClient && <div className="time-ago">{msg.createdAtString}</div>} 
                                                </li>
                                            )
                                        })}            
                                    </ul>
                                </div>
                            </div>
                        )
                    })}
                    
                </div>
            </wrap>
        )
    }
}


Message.propTypes = {
    message: MPropTypes.observableObject
}

