import React from 'react'
import ReactDOM from 'react-dom'

import './css/sweetalert.css'
import './css/style.css'
import './css/react-autosuggest.css'
import './css/react-datepicker.css'


import Routes from './routes'


const root = document.getElementById('root')

ReactDOM.render(<Routes />, root)


if (module.hot) {
  module.hot.accept('./routes', () => {
    const NextApp = require('./routes').default
    ReactDOM.render(
      <NextApp />,
      root
    )
  })
}