import moment from 'moment'
import { action, computed, observable } from 'mobx'
import { apiCall } from 'api'
import { logMessage } from 'services'
import { ComputerModel } from 'models'
import Filter from 'services/Filter'


class ComputerStore{
    @observable activeGuid = null
    @observable errors = null
    // Duplicate filter in state and local storage
    @observable filter = null
    @observable isAddComputerSuccess = false
    @observable isControlsVisible = false
    @observable isDeleteComputerSuccess = false
    @observable isEditComputerInProgress = false
    @observable isEditComputerSuccess = false
    @observable isEditPauseInProgress = false
    @observable isLoading = true
    @observable map = new Map()
    
    @observable _startDate = null
    @observable _endDate = null

    @computed
    get activeComputer(){
        if (!this.activeGuid){
            return null
        }
        return this.map.get(this.activeGuid) || null
    }

    @computed
    get startDate(){
        if (this._startDate){
            let dt = this._startDate
            return moment({y:dt.year(), M:dt.month(), d:dt.date()})
        }

        let computer = this.activeComputer
        if (computer){
            let dt = computer.minDate
            return moment({y:dt.year(), M:dt.month(), d:dt.date()})
        }
        return null
    }

    @computed
    get endDate(){
        if (this._endDate){
            let dt = this._endDate
            return moment({ y:dt.year(), M:dt.month(), d:dt.date(), h:23, m:59, s:59})
        }

        let computer = this.activeComputer
        if (computer){
            let dt = computer.maxDate
            return moment({ y:dt.year(), M:dt.month(), d:dt.date(), h:23, m:59, s:59})
        }
        return null   
    }

    @action
    clear(){
        this.errors = null
        this.isAddComputerSuccess = false
        this.isDeleteComputerSuccess = false
        this.isEditComputerSuccess = false
    }

    @action
    load = async () => {
        const url = '/v1/computers'
        let result = await apiCall(url, 'GET', {}, true)
        if (result.isError){
            logMessage('ComputerStore.load error')
            return
        }
        
        let m = new Map()
        for (let obj of result.data.items){
            m.set(obj.guid, new ComputerModel(obj))
        }
        this.map = m
        
        if (this.map.size > 0){
            let computer

            let activeGuid = window.localStorage.getItem('activeComputerGuid')
            
            if (activeGuid){
                computer = this.map.get(activeGuid)
            }

            if (!computer){
                let i = this.map.keys()
                let res = i.next()
                let key = res.value
                computer = this.map.get(key)
            }

            this.setActiveComputer(computer.guid)
        } else {
            this.activeGuid = null
        }

        this.isLoading = false
    }

    @action
    setControlsVisibility(boolValue){
        this.isControlsVisible = boolValue
    }

    @action
    setActiveComputer(guid){
        window.localStorage.setItem('activeComputerGuid', guid)
        let computer = this.map.get(guid)
        if (computer){
            this.activeGuid = computer.guid
        }
    }

    @action
    add = async (form) => {
        this.errors = null

        const url = '/v1/computers'
        let result = await apiCall(url, 'POST', form, true)

        if (result.isError){
            this.errors = result.data
            return
        }
        this.isAddComputerSuccess = true
        this.load()
    }

    @action
    edit = async (form) => {
        this.errors = null
        this.isEditComputerInProgress = true
        this.isEditComputerSuccess = false

        const url = '/v1/computers/' + form.guid
        let result = await apiCall(url, 'PUT', form, true)

        if (result.isError){
            this.errors = result.data
        } else {
            this.load()
            this.isEditComputerSuccess = true
        }
        this.isEditComputerInProgress = false
    }

    @action
    editPause = async (form) => {
        this.errors = null
        this.isEditPauseInProgress = true

        const url = '/v1/computers_edit_pause/' + form.guid
        let result = await apiCall(url, 'PUT', form, true)

        if (result.isError){
            this.errors = result.data
        } else {
            this.load()
        }
        this.isEditPauseInProgress = false
    }

    @action
    remove = async (form) => {
        const url = '/v1/computers/' + form.guid
        let result = await apiCall(url, 'DELETE', {}, true)

        if (result.isError){
            logMessage('ComputerStore.remove error')
            return
        } 
        this.load()
    }

    @action
    setStartDate(dt){
        if (this._endDate && dt > this._endDate){
            this._endDate = dt
        }
        this._startDate = dt
    }

    @action
    setEndDate(dt){
        if (this._startDate && dt < this._startDate){
            this._startDate = dt
        }

        this._endDate = dt   
    }

    @action
    getFilter(){
        const f = new Filter(this.activeGuid)
        this.filter = f.toObject()
    }

    @action
    setFilter(filter){
        this.filter = filter
    }
}

let computerStore = new ComputerStore()
export default computerStore
