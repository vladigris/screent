import { action, computed, observable } from 'mobx'
import { apiCall } from 'api'
import { logMessage } from 'services'



class MessageStore{
    @observable activeGuid
    @observable isAddItemInProgress
    @observable isCreateInProgress
    @observable isEditInProgress
    @observable isInitialLoading
    @observable isNextLoading
    @observable isMessageLoading
    @observable map
    @observable notReadCount
    @observable pagination
    @observable showAddForm

    constructor(){
        this.activeGuid = null
        this.isAddItemInProgress = false
        this.isCreateInProgress = false
        this.isEditInProgress = false
        this.isInitialLoading = false
        this.isNextLoading = false
        this.isMessageLoading = false
        this.map = new Map()
        this.notReadCount = 0
        this.pagination = null
        this.showAddForm = false
    }

    @computed
    get activeMessage(){
        if (!this.activeGuid){
            return null
        }
        return this.map.get(this.activeGuid) || null
    }

    @computed
    get nextPage(){
        if (!this.pagination){
            return 1
        }
        return this.pagination.page + 1
    }
    
    /**
     * When client sent two messages or support sent two messages
     * (one after other), they should be groupped.
     * Convert array of message items, to array of arrays.
     * [ [item1, item2], [item3, item4] ]
     */ 
    @computed
    get groupedItems(){
        if (!this.activeGuid || this.isMessageLoading){
            return []
        }

        let m = new Map()
        let key = 0
        let isClient = null
        for (let obj of this.activeMessage.items){
            if (isClient !== obj.isClient){
                key++
                m.set(key, [obj])
            } else {
                let arr = m.get(key)
                arr.push(obj)
                m.set(key, arr)
            }
            isClient = obj.isClient
        }
        return Array.from(m.values())
    }

    @action
    setActive(guid){
        this.activeGuid = guid
        this.loadActive()
    }

    /**
     * Loads messages.
     */
    @action
    load = async (isFirstLoad) => {
        let page = 1
        if (!isFirstLoad){
            if (this.pagination && this.nextPage > this.pagination.totalPages){
                return
            }    
            page = this.nextPage
        }

        this.isInitialLoading = isFirstLoad ? true : this.isInitialLoading
        this.isNextLoading = !isFirstLoad ? true : this.isNextLoading

        const url = `/v1/messages?limit=20&page=${page}`
        let result = await apiCall(url, 'GET', {}, true)
        if (result.isError){
            logMessage('MessageStore.load error')
            this.isInitialLoading = isFirstLoad ? false : this.isInitialLoading
            this.isNextLoading = !isFirstLoad ? false : this.isNextLoading
            return
        } 

        for (let msg of result.data.messages){
            this.map.set(msg.guid, msg)
        }
        this.pagination = result.data.pagination
        
        this.isInitialLoading = isFirstLoad ? false : this.isInitialLoading
        this.isNextLoading = !isFirstLoad ? false : this.isNextLoading
    }

    /**
     * Loads active message
     */
    @action
    loadActive = async () => {
        if (!this.activeGuid){
            return
        }

        this.isMessageLoading = true

        const url = '/v1/messages/' + this.activeGuid
        let result = await apiCall(url, 'GET', {}, true)
        if (result.isError){
            logMessage('MessageStore.loadActive error')
            this.isMessageLoading = false
            return
        } 

        // Message returned from server contains also message items
        this.map.set(this.activeGuid, result.data)
        this.isMessageLoading = false
    }

    @action
    loadNotReadCount = async () => {
        const url = '/v1/messages/not_read_count'
        let result = await apiCall(url, 'POST', {}, true)
        if (result.isError){
            logMessage('MessageStore.loadNotReadCount error')
            return
        }
        this.notReadCount = result.data.count
    }

    @action
    create = async (form) => {
        this.isCreateInProgress = true

        const url = '/v1/messages'
        let result = await apiCall(url, 'POST', form, true)
        if (result.isError){
            logMessage('MessageStore.create error')
            this.isCreateInProgress = false
            return
        } 
        let msg = result.data
        this.map.set(msg.guid, msg)
        this.setActive(msg.guid)
        this.isCreateInProgress = false
        this.setShowAddForm(false)
    }

    @action
    edit = async (form) => {
        if (!this.activeGuid){
            return
        }
        this.isEditInProgress = true

        const url = '/v1/messages/' + this.activeGuid
        let result = await apiCall(url, 'PUT', form, true)
        if (result.isError){
            logMessage('MessageStore.edit error')
            this.isEditInProgress = false
            return
        } 
        this.loadActive()
        this.isEditInProgress = false
    }

    @action
    addItem = async (form) => {
        if (!this.activeGuid){
            return
        }
        this.isAddItemInProgress = true

        const url = '/v1/messages_add_item/' + this.activeGuid
        let result = await apiCall(url, 'POST', form, true)
        if (result.isError){
            logMessage('MessageStore.addItem error')
            this.isAddItemInProgress = false
            return
        } 
        this.loadActive()
        this.isAddItemInProgress = false
    }

    @action
    setRead = async (guid) => {
        const url = '/v1/messages_set_read/' + guid
        let result = await apiCall(url, 'PUT', {}, true)
        if (result.isError){
            logMessage('MessageStore.setRead error')
            return
        } 
        this.loadActive()
    }

    @action
    setShowAddForm(boolValue){
        this.showAddForm = boolValue
    }
}

let messageStore = new MessageStore()
export default messageStore
