import { action, observable } from 'mobx'
import uuid from 'node-uuid'


/**
 * If "ms" passed and ms > 0
 * it means to remove alert after number of ms.
 */
class AlertStore{
    @observable map = new Map()

    @action
    add(type, message, ms=0){
        // Check for duplicate message
        for (let a of this.map.values()){
            if (a.message === message && a.type === type){
                return
            }
        }

        let guid = uuid.v4()
        this.map.set(guid, {guid, type, message})

        if (ms > 0){
            setTimeout(() => {this.remove(guid)}, ms)
        }
    }

    @action
    remove(guid){
        this.map.delete(guid)
    }
}

let alertStore = new AlertStore()
export default alertStore
