import { action, observable } from 'mobx'
import { apiCall } from 'api'


class TranslationStore{
    @observable data = null

    constructor(){
        this.load()
    }
    
    @action
    load = async () => {
        const url = '/v1/translations'
        let result = await apiCall(url, 'GET', {}, false)
        this.data = result.data
    }
}

let tStore = new TranslationStore()
export default tStore