import { action, observable } from 'mobx'
import { apiCall } from 'api'
import { logMessage } from 'services'
import { UserModel } from 'models'



class UserStore{
    @observable me = null
    @observable errors = null
    @observable isAuthenticated = null
    @observable isChangeEmailInProgress = false
    @observable isChangePasswordInProgress = false
    @observable isChangePasswordSuccess = false
    @observable isConfirmChangeEmailFinished = false
    @observable isConfirmRegistrationFinished = false
    @observable isEditUserInProgress = false
    @observable isEditUserSuccess = false
    @observable isLoading = false
    @observable isResendConfirmationLinkInProgress = false
    @observable isSignupSuccess = false
    @observable resendConfirmationMessage = null
    @observable token = null
    
    @action
    clear(){
        this.errors = null
        this.isChangePasswordSuccess = false
        this.isConfirmChangeEmailFinished = false
        this.isConfirmRegistrationFinished = false
        this.isEditUserSuccess = false
        this.isSignupSuccess = false
        this.resendConfirmationMessage = null
    }

    @action
    setErrors(obj){
        this.errors = obj
    }

    @action
    load = async () => {
        const url = '/v1/user'
        let result = await apiCall(url, 'GET', {}, true)
        if (result.isError){
            logMessage('UserStore.load error')
            return
        }
        this.me = new UserModel(result.data)
    }

    @action
    checkToken(){
        let token = window.localStorage.getItem('token')    
        if (token === null){
            this.token = null
            this.isAuthenticated = false
            return
        }
        this.token = token
        this.isAuthenticated = true
    }

    @action
    login = async (form) => {
        this.errors = null

        const url = '/v1/auth/login'
        let result = await apiCall(url, 'POST', form, false)
        
        if (result.isError){
            this.errors = result.data
        } else {
            window.localStorage.setItem('token', result.data.token)
            this.token = result.data.token
            this.isAuthenticated = true
        }
    }

    @action
    logout(){
        window.localStorage.removeItem('token')
        this.token = null
        this.isAuthenticated = false
        this.me = null
    }

    @action
    signUp = async (form) => {
        this.errors = null

        const url = '/v1/auth/signup'
        let result = await apiCall(url, 'POST', form, false)
        
        if (result.isError){
            this.errors = result.data
        } else {
            this.isSignupSuccess = true
        }
    }

    @action
    startTrial = async () => {
        const url = '/v1/user/start_trial'
        let result = await apiCall(url, 'POST', {}, true)
        if (result.isError){
            logMessage('UserStore.startTrial error')
            return
        }
        this.load()
    }

    @action
    confirmRegistration = async (form) => {
        this.errors = null
        
        const url = '/v1/auth/signup/confirm'
        let result = await apiCall(url, 'POST', form, false)
        if (result.isError){
            this.errors = result.data
            this.isConfirmRegistrationFinished = true
            return
        }

        this.isConfirmRegistrationFinished = true
    }

    @action
    resendConfirmationLink = async () => {
        this.errors = null
        this.isResendConfirmationLinkInProgress = true

        const url = '/v1/user/resend_reg_confirmation_link'
        let result = await apiCall(url, 'POST', {}, true)
        if (result.isError){
            this.errors = result.data
            this.isResendConfirmationLinkInProgress = false
            return
        }
        
        this.resendConfirmationMessage = result.data.message        
        this.isResendConfirmationLinkInProgress = false
    }    

    @action
    changeEmail = async (form) => {
        this.errors = null
        this.isChangeEmailInProgress = true

        const url = '/v1/user/change/email'
        let result = await apiCall(url, 'POST', form, true)
        if (result.isError){
            this.errors = result.data
            this.isChangeEmailInProgress = false
            return
        }

        this.isChangeEmailInProgress = false
        // Reload user
        this.load()
    }

    @action
    changeEmailConfirm = async (form) => {
        this.errors = null

        const url = `/v1/user/change/email/confirm/${form.guid}/${form.token}`
        let result = await apiCall(url, 'POST', {}, false)
        if (result.isError){
            this.errors = result.data
            this.isConfirmChangeEmailFinished = true
            return
        }
        this.isConfirmChangeEmailFinished = true
    }

    @action
    changePassword = async (form) => {
        this.errors = null
        this.isChangePasswordInProgress = true

        const url = '/v1/user/change/password'
        let result = await apiCall(url, 'POST', form, true)
        if (result.isError){
            this.errors = result.data
            this.isChangePasswordInProgress = false
            return
        }

        this.isChangePasswordInProgress = false
        this.isChangePasswordSuccess = true
    }

    @action
    editUser = async (form) => {
        this.errors = null
        this.isEditUserInProgress = true

        const url = '/v1/user'
        let result = await apiCall(url, 'PUT', form, true)
        if (result.isError){
            this.errors = result.data
            this.isEditUserInProgress = false
            return
        }

        this.isEditUserInProgress = false
        this.isEditUserSuccess = true
        // Reload user
        this.load()
    }

}

let userStore = new UserStore()
export default userStore

