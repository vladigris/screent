import { apiCall } from 'api'
import { logMessage } from 'services'
import { action, computed, observable } from 'mobx'
import { NotificationModel } from 'models'


class NotificationStore{
    @observable map = new Map()
    
    @computed
    get hasUnread(){
        if (this.map.size === 0){
            return false
        }

        for (let v of this.map.values()){
            if (v.isRead === false){
                return true
            }
        }
        return false
    }

    @computed
    get unreadCount(){
        let count = 0
        for (let v of this.map.values()){
            if (!v.isRead){
                count++
            }
        }
        return count
    }

    @action
    load = async () => {
        const url = '/v1/notifications'
        let result = await apiCall(url, 'GET', {}, true)
        if (result.isError){
            logMessage('NotificationStore.load error')
            return
        }
        
        let m = new Map()

        for (let obj of result.data.items){
            m.set(obj.guid, new NotificationModel(obj))
        }
        this.map = m
    }

    @action
    edit = async (form) => {
        const url = '/v1/notifications/' + form.guid
        let result = await apiCall(url, 'PUT', form, true)
        if (result.isError){
            logMessage('NotificationStore.edit error')
            return
        }
        
        let n = this.map.get(form.guid)
        if (n){
            n.isRead = true
            this.map.set(form.guid, n)
        }
        this.load()
    }    
}

let notificationStore = new NotificationStore()
export default notificationStore
