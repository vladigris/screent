import moment from 'moment'
import { action, computed, toJS, observable } from 'mobx'
import { apiCall } from 'api'
import { getAlertStore, getComputerStore } from 'stores'
import { logMessage } from 'services'
import { KeyboardModel, ScreenModel } from 'models'

const LIMIT = 10


/**
 * imageCacheMap: key - image src, value - not matters
 */
class ScreenStore{
    @observable activeScreenKey = null
    @observable imageCacheMap = new Map()
    @observable isLoading = false
    @observable isPlaying = false
    @observable keyboards = new Map()
    @observable map = new Map()
    @observable pagination = null
    @observable totalCount = null
    
    @computed
    get nextPage(){
        if (!this.pagination){
            return 1
        }
        return this.pagination.page + 1
    }

    @computed
    get activeScreen(){
        if (!this.activeScreenKey){
            return null
        }
        return this.map.get(this.activeScreenKey) || null
    }

    @action
    load = async (mode, direction) => {
        let computer = getComputerStore()

        if (this.pagination && this.nextPage > this.pagination.totalPages){
            return
        }

        this.isLoading = true

        let form = {
            page: this.nextPage,
            limit: LIMIT,
            titles: toJS(computer.filter.titlesArr),
            programs: toJS(computer.filter.programsArr),
            keyboards: toJS(computer.filter.keyboardsArr),
            minDt: computer.startDate,
            maxDt: computer.endDate
        }

        const url = '/v1/computer_screens/' + computer.activeGuid
        let result = await apiCall(url, 'POST', form, true)
        if (result.isError){
            logMessage('ScreenStore.load error', result.data)
            this.isLoading = false
            return
        }

        let d = result.data

        if (d.screens && d.screens.length > 0){
            // Received result not empty
            
            for (let obj of d.screens){
                let s = new ScreenModel(obj)
                this.map.set(s.createdAtString, s)
                this.imageCacheMap.set(s.src, null)
            }

            for (let obj of d.keyboards){
                let k = new KeyboardModel(obj)
                this.keyboards.set(k.createdAtString, k)
            }

            this.pagination = d.pagination
            this.totalCount = d.total
        } else {
            this.totalCount = 0
        }
        this.isLoading = false
    }

    @action
    reload(){
        this.load('manual', 'next')
    }

    @action
    play(){
        this.isPlaying = !this.isPlaying
    }

    @action
    stop(){
        this.isPlaying = false
    }

    @action
    stopAndClear(){
        this.isPlaying = false
        this.activeScreenKey = null
        this.map = new Map()
        this.pagination = null
    }

    @action
    playNext(mode='auto', direction='next'){
        const computer = getComputerStore()
        const alert = getAlertStore()

        if (this.map.size === 0){
            this.stop()
            return
        }

        if (mode === 'auto' && !this.isPlaying){
            return
        }

        let isFinished = false

        if (!this.activeScreenKey){
            let i = this.map.values()
            this.activeScreenKey = i.next().value.createdAtString
        } else {
            let keysArr = Array.from(this.map.keys())
            
            let index = keysArr.indexOf(this.activeScreen.createdAtString)
            let newIndex = direction === 'next' ? index + 1 : index - 1

            if ((direction === 'next' && newIndex <= keysArr.length - 1) || (direction !== 'next' && newIndex >=0)){
                let newKey = keysArr[newIndex]

                this.activeScreenKey = this.map.get(newKey).createdAtString

                // Check current index and if it is near the end of queue,
                // load more screens from server to state.
                if (newIndex > keysArr.length - 8){
                    this.load(mode, direction)
                }
            } else {
                isFinished = true
            }
        }

        if (isFinished){
            this.stop()
            
            if (direction === 'next'){
                alert.add('info', 'Finished!', 3000)
            } else {
                alert.add('info', 'First screenshot!', 3000)
            }
            return
        }

        if (mode === 'auto'){
            // continue playNext
           setTimeout(
                () => {this.playNext(mode, direction)}, computer.filter.delay*1000)
        }
    }

    /**
     * Find 3 minutes back and 3 minutes forward for active screen.
     * Rounded to minutes. Look by minuteIndexes. YYYYMMDDHHmm
     */
    @computed
    get activeKeyboards(){
        if (!this.activeScreenKey){
            return []
        }

        let arr = []
        let deltas = [-3, -2, -1, 0, 1, 2, 3]
        for (let num of deltas){
            let x = moment(this.activeScreen.createdAt).add(num, 'minutes')    
            arr.push(x)
        }

        let minutes = arr.map((m) => m.format('YYYYMMDDHHmm'))

        let result = []
        let keyboards = Array.from(this.keyboards.values())

        for (let kb of keyboards){
            if (minutes.includes(kb.minuteIndex)){
                result.push(kb)
            }
        }
        return result
    }
}

let screenStore = new ScreenStore()
export default screenStore

