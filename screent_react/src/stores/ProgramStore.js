import { action, computed, observable } from 'mobx'
import { apiCall } from 'api'


class ProgramStore{
    @observable items = null
    
    @computed get options(){
        if (!this.items){
            return []
        }
        return this.items.filter(obj => obj.name)
    }

    @action
    load = async () => {
        const url = '/v1/programs'
        let result = await apiCall(url, 'GET', {}, true)
        this.items = result.data.items
    }
}

let programStore = new ProgramStore()
export default programStore