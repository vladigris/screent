import { action, observable } from 'mobx'
import { apiCall } from 'api'


class FaqStore{
    @observable items = null
    
    @action
    load = async () => {
        const url = '/v1/faq'
        let result = await apiCall(url, 'GET', {}, true)
        this.items = result.data.items
    }
}

let faqStore = new FaqStore()
export default faqStore