import { default as alertStore } from './AlertStore'
export { alertStore }

import { default as computerStore } from './ComputerStore'
export { computerStore }

import { default as faqStore } from './FaqStore'
export { faqStore }

import { default as messageStore } from './MessageStore'
export { messageStore }

import { default as notificationStore } from './NotificationStore'
export { notificationStore }

import { default as programStore } from './ProgramStore'
export { programStore }

import { default as screenStore } from './ScreenStore'
export { screenStore }

import { default as tStore } from './TranslationStore'
export { tStore }

import { default as userStore } from './UserStore'
export { userStore }


export function getAlertStore(){
    return alertStore
}

export function getComputerStore(){
    return computerStore
}

export function getFaqStore(){
    return faqStore
}

export function getMessageStore(){
    return messageStore
}

export function getNotificationStore(){
    return notificationStore
}

export function getProgramStore(){
    return programStore
}

export function getScreenStore(){
    return screenStore
}

export function getTranslationStore(){
    return tStore
}

export function getUserStore(){
    return userStore
}




