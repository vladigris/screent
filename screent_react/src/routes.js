import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import DevTools from 'mobx-react-devtools'
import {useStrict} from 'mobx'
import { Provider } from 'mobx-react'

import {
  AddComputer,
  Auth,
  AuthRoute,
  Base,
  ConfirmRegistration,
  ChangeEmail,
  ChangeEmailConfirm,
  ChangePassword,
  EditComputers,
  EditProfile,
  Faq,
  Home,
  Login,
  Logout,
  Messages,
  NotFound,
  Registration,
  UserNotConfirmed,
  UserProfile } from 'components'


import {
  alertStore,
  computerStore,
  faqStore,
  messageStore,
  notificationStore,
  programStore,
  screenStore,
  tStore,
  userStore } from 'stores'


// With useStrict(true) too much boilerplate for async actions
// Follow the strict mode manually
useStrict(false)


const stores = {
    alert: alertStore,
    computer: computerStore,
    faq: faqStore,
    message: messageStore,
    notification: notificationStore,
    program: programStore,
    screen: screenStore,
    t: tStore,
    user: userStore
}


const Routes = () => {
  return (
    <div>
      {process.env.NODE_ENV === 'development' && <DevTools />}

      <Provider {...stores}>
          <BrowserRouter>
              <Switch>
                  <Route exact path="/sign-in" component={Login} />     
                  <Route exact path="/sign-up" component={Registration} />     
                  <Route exact path="/confirm/:guid/:token" component={ConfirmRegistration} />     
                  <Route exact path="/change_email_confirm/:guid/:token" component={ChangeEmailConfirm} />     

                  <AuthRoute exact path="/logout" component={Logout} />
                  <AuthRoute exact path="/not_confirmed" component={UserNotConfirmed} />
    
                  <Route path="/" render={(props)=>{
                    return (
                      <Auth>
                        <Base {...props}>
                          <Switch>
                            <Route exact path="/" component={Home} />
                            <Route path="/add_computer" component={AddComputer} />
                            <Route path="/change_email" component={ChangeEmail} />
                            <Route path="/change_password" component={ChangePassword} />
                            <Route path="/edit_computers" component={EditComputers} />
                            <Route path="/edit_profile" component={EditProfile} />
                            <Route path="/faq" component={Faq} />
                            <Route path="/support" component={Messages} />
                            <Route path="/profile" component={UserProfile} />
                          </Switch>
                        </Base>
                      </Auth>
                    )
                  }} />
                 
                  <Route component={NotFound} />
              </Switch>
          </BrowserRouter>
      </Provider>
    </div>
  )
}


export default Routes

