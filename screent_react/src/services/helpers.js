import React from 'react'




export function validateEmail(email){
    var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
    if(!regex.test(email)) {
        return false;   
    } 
    return true;
}     


/**
 * Get all form values and return object.
 */
export function formToObject(form){
  let res = {}
  for (let el of form.elements){
    let value = el.value
    if (typeof value === 'string'){
        value = value.trim()
    }
    res[el.name] = value
  }
  return res
}


/**
 * Object to query string foo=bar&lorem=ipsum
 */
export function objToParams(obj){
    return Object.keys(obj).map(
        k => k + '=' + encodeURIComponent(obj[k])
    ).join('&')    
}


/**
 * Undescore to camelCase
 */
export function toCamelCase(str){
  return str.toLowerCase().replace(
    /(_|-)([a-z])/g,
    (str) => str[1].toUpperCase()
  )
}


/**
 * Returns filtered data.
 * @param {string} value
 * @param {Array}  data - array of objects
 * @param {Array}  fields - array of strings
 */ 
export function filterData(value, data, fields){
    return data.filter((obj) => {
        for (let field of fields){
            if (obj.hasOwnProperty(field) && obj[field].toLowerCase().includes(value.toLowerCase())){
                return true    
            }    
        }
        return false 
    })
}

export function formatMoney(n, c, d, t){
    if (typeof n !== 'number')
        return 0

    c = Math.abs(c)
    c = isNaN(c) ? 2 : c
    d = d === undefined ? '.' : d
    t = t === undefined ? ',' : t 
    let s = n < 0 ? '-' : ''
    n = Math.abs(+n || 0).toFixed(c)
    let i = parseInt(n, 10) + ''
    let j = i.length
    j = j > 3 ? j % 3 : 0
    return s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '')
}


export function zeroFill(num, size) {
    var s = num.toString()
    while (s.length < size){
        s = '0' + s
    }
    return s
}


export function createUUID() {
    let s = []
    let hexDigits = '0123456789abcdef'
    for (let i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1)
    }
    s[14] = '4'
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1)
    s[8] = s[13] = s[18] = s[23] = '-'
    let uuid = s.join('')
    return uuid
}


export function isFloat(value){
    var re = /^[0-9]+$/;
    if (re.test(value)){
        return true;
    }
    re = /^[0-9]+\.[0-9]+$/;
    return ( re.test(value) ) ? true : false;
}


/**
 * Empty row for table.
 */
export function getEmptyRow(numColumns){
    let row = {className: 'blank_row'}
    row.cols = [{value: '', colspan: numColumns}]
    return row
}


export function rHTML(html){
    return <wrap dangerouslySetInnerHTML={{__html: html}} />
}

