
export function logMessage(...args){
    if (process.env.NODE_ENV === 'development') {
        console.log('_____________________')
        for (let message of args){
            console.log(message)    
        }
    }
}