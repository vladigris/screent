
const MAX_KEYBOARDS = 5
const MAX_TITLES = 5
const MAX_PROGRAMS = 5


/**
 * key: computer<guid>
 * value: object
 */
export default class Filter{
    constructor(guid){
        this.st = window.localStorage
        this.key = 'computer' + guid
        this.keys = Object.keys(this.getInitial()) 
        
        let value = this.st.getItem(this.key)
        if (!value){
            value = this.getInitial()
        } else {
            value = JSON.parse(value)
        }
        
        for (let key in value){
            Object.defineProperty(this, key, {
                value: value[key],
                enumerable: true,
                configurable: true,
                writable: true
            })
            
        }
    }

    /**
     * titlesArr, programsArr, keyboardsArr are the same array as
     * titles, programs, keyboards, but without empty strings.
     */
    getInitial(){
        return {
            titlesArr: [],
            programsArr: [],
            keyboardsArr: [],
            titles: [],
            programs: [],
            keyboards: [],
            isShowTitle: true,
            isShowKeyboard: true,
            isShowTime: true,
            delay: 2,
            delayRange: [0.5, 1, 2, 3, 4, 5]
        }
    }

    save(){
        let obj = {}
        for (let key of this.keys){
            obj[key] = this[key]
        }
        this.st.setItem(this.key, JSON.stringify(obj))
    }
    
    /**
     * @returns {object}
     */
    toObject(){
        let obj = {}
        for (let key of this.keys){
            obj[key] = this[key]
        }
        return obj
    }

    strToArr(value){
        if (Array.isArray(value)){
            return value
        }

        if (value.trim() === ''){
            return []
        }
        let arr = value.split('\n')
        arr = arr.map((line) => line.replace(/\s{2,}/g, ' '))
        return arr
    }

    setTitles(value){
        let arr = this.strToArr(value)
        if (arr.length <= MAX_TITLES){
            this.titles = arr
            this.titlesArr = this.titles.filter(s => s !== '')
        }
    }

    setPrograms(value){
        let arr = this.strToArr(value)
        if (arr.length <= MAX_PROGRAMS){
            this.programs = arr   
            this.programsArr = this.programs.filter(s => s !== '')
        }
    }

    setKeyboards(value){
        let arr = this.strToArr(value)
        if (arr.length <= MAX_KEYBOARDS){
            this.keyboards = arr    
            this.keyboardsArr = this.keyboards.filter(s => s !== '')
        }
    }

    setIsShowTitle(value){
        this.isShowTitle = value
    }

    setIsShowKeyboard(value){
        this.isShowKeyboard = value
    }

    setIsShowTime(value){
        this.isShowTime = value
    }

    setDelay(value){
        if (this.delayRange.includes(value)){
            this.delay = value    
        }
    }
}


