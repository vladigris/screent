from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer

schema_view = get_schema_view(
    title='screent API',
    renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer])


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^docs/', schema_view, name='docs'),
    url(r'^', include('core.urls')),
    url(r'^', include('msg.urls')),
    url(r'^', include('web.urls')),
]

admin.site.site_header = 'screent'
admin.site.site_title = 'screent'
