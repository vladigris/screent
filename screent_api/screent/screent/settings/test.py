from .base import *

DEBUG = True
PRODUCTION = False
TESTING = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': '5432'
    },
}

REDIS_DB = redis.StrictRedis(host='127.0.0.1',
                             port=6379, db=REDIS_DB_TEST_NUM)
