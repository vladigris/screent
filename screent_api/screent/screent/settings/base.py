import json
import redis
from os.path import abspath, join, dirname

POSTGRESQL_VERSION = '9.5'

BASE_DIR = abspath(join(dirname(abspath(__file__)), '..', '..', '..'))

scdata_path = '/home/vlad/.scdata'
data = json.loads(open(scdata_path).read())

LOGIN_URL = '/auth/login/'

ALLOWED_HOSTS = ['*']

BUCKET_NAME = 'screent'

SECRET_KEY = data['SecretKey']
DB_NAME = data['DbName']
DB_USER = data['DbUser']
DB_PASSWORD = data['DbPassword']
DB_HOST = data['DbHost']
SERVER_APP_IP = data['ServerAppIp']
SERVER_STORAGE_IP = data['ServerStorageIp']
SERVER_APP_IPS = data['ServerAppIps']
VERSIONS = data['Versions']

BACKEND_DOMAIN = data['ApiDomain']
BACKEND_URL = data['ApiUrl']
FRONTEND_DOMAIN = data['AppDomain']
FRONTEND_URL = data['AppUrl']
LANDING_DOMAIN = data['LandingDomain']
LANDING_URL = data['LandingUrl']

AUTH_USER_MODEL = 'core.User'
APPEND_SLASH = False

DEFAULT_FROM_EMAIL = data['EmailFrom']
EMAIL_HOST = data['EmailHost']
EMAIL_HOST_PASSWORD = data['EmailPassword']
EMAIL_HOST_USER = data['EmailUser']
EMAIL_PORT = data['EmailPort']
EMAIL_USE_TLS = True

DEMO_USER_EMAIL = data['DemoUserEmail']
DEMO_USER_PASSWORD = data['DemoUserPassword']
DEMO_COMPUTER_GUID = data['DemoComputerGuid']

TEST_USER_EMAIL = data['TestUserEmail']
TEST_USER_PASSWORD = data['TestUserPassword']

TEST_USER2_EMAIL = data['Test2UserEmail']
TEST_USER2_PASSWORD = data['Test2UserPassword']

# Computer with fixtures
TEST_COMPUTER_GUID = data['TestComputerGuid']

REDIS_KEY_COMPUTER_CONFIG = data['RedisKeyComputerConfig']
REDIS_KEY_COMPUTER_CONFIG_SNAPSHOT = data['RedisKeyComputerConfigSnapshot']
REDIS_KEY_COMPUTER = data['RedisKeyComputer']
REDIS_KEY_USER_TOKEN = data['RedisKeyUserToken']
REDIS_KEY_LOG_DATA = data['RedisKeyLogData']
REDIS_KEY_KEYBOARD_DATA = data['RedisKeyKeyboardData']
REDIS_KEY_SCREEN_DATA = data['RedisKeyScreenData']
REDIS_DB_NUM = data['RedisDb']
REDIS_DB_TEST_NUM = data['RedisDbTest']

SLACK_ALERTS_URL = data['SlackAlertsUrl']

SPACES_KEY = data['SpacesKey']
SPACES_SECRET = data['SpacesSecret']
SPACES_REGION = data['SpacesRegion']
SPACES_BUCKET = data['SpacesBucket']

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_filters',
    'rest_framework',
    'rest_framework_swagger',
    'corsheaders',
    'core',
    'msg',
    'payment',
    'web',
)

MIDDLEWARE = (
    'web.middleware.AccessControlMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'web.middleware.ActiveUserMiddleware'
)

ROOT_URLCONF = 'screent.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [join(BASE_DIR, 'screent/screent/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'screent.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': '5432'
    },
    'testgo_screent': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'testgo_' + DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': '5432'
    },
}

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = BASE_DIR + '/static/media/'
STATIC_URL = '/static/'
MEDIA_URL = '/static/media/'

FILE_UPLOAD_HANDLERS = (
    'django.core.files.uploadhandler.MemoryFileUploadHandler',
    'django.core.files.uploadhandler.TemporaryFileUploadHandler',
)

FILE_UPLOAD_TEMP_DIR = MEDIA_ROOT + 'tmp/'

STATICFILES_DIRS = [join(BASE_DIR, 'static')]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


BROKER_URL = 'redis://127.0.0.1:6379/0'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'


REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework.filters.DjangoFilterBackend',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer'
    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser'
    ),
    'DEFAULT_PERMISSION_CLASSES': (),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'screent.authentication.BasicAuthentication',
        'screent.authentication.SessionAuthentication',
        'screent.authentication.TokenAuthentication'
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'DATETIME_FORMAT': '%Y-%m-%dT%H:%M:%S',
    'EXCEPTION_HANDLER': 'web.exceptions.custom_exception_handler',
    'PAGE_SIZE': 20,
    'NON_FIELD_ERRORS_KEY': 'errors'
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,

    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },

    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d \
            %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },

    'handlers': {
        'console': {
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'db': {
            'class': 'screent.loggers.MyDbLogHandler',
            'formatter': 'verbose'
        }
    },

    'loggers': {
        'django': {
            'level': 'INFO',
            'handlers': ['console'],
            'propagate': True,
        },

        'django.request': {
            'handlers': ['db'],
            'level': 'ERROR',
            'propagate': False,
        },

        'app': {
            'level': 'DEBUG',
            'handlers': ['console', 'db'],
            'propagate': False,
        }
    }
}

# Custom token auth.
TOKEN_EXPIRE_MINUTES = 1400

# Num days
TRIAL_PERIOD = 14

MAX_INTERVAL = 300
