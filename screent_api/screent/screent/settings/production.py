from .base import *

DEBUG = False
PRODUCTION = True
TESTING = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': SERVER_STORAGE_IP,
        'PORT': '5432'
    }
}

REDIS_DB = redis.StrictRedis(host=SERVER_STORAGE_IP,
                             port=6379, db=REDIS_DB_NUM)
