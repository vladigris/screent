import json
import pytest

from screent.tests.web import base
from .data import get_contact_landing_data

from msg.models import ContactLanding


class TestContactLanding(base.Mixin):
    @pytest.mark.django_db
    def test_01(self, client):
        """
        Should get error with empty name
        """
        url = self.get_url('contact_landing')
        dic = get_contact_landing_data()
        dic['name'] = ''
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_02(self, client):
        """
        Should get error with empty email
        """
        url = self.get_url('contact_landing')
        dic = get_contact_landing_data()
        dic['email'] = ''
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_03(self, client):
        """
        Should get error with incorrect email
        """
        url = self.get_url('contact_landing')
        dic = get_contact_landing_data()
        dic['email'] = 'aaa'
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_04(self, client):
        """
        Should get error with empty text
        """
        url = self.get_url('contact_landing')
        dic = get_contact_landing_data()
        dic['text'] = ''
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_05(self, client):
        """
        Should get success
        """
        url = self.get_url('contact_landing')
        dic = get_contact_landing_data()
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        assert ContactLanding.objects.first() is not None
