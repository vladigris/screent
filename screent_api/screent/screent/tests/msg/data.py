def get_contact_data():
    """
    Returns data for contact form
    """
    return {
        'subject': 'some subject',
        'text': 'some text',
    }


def get_contact_landing_data():
    """
    Returns data for landing contact form
    """
    return {
        'name': 'some name',
        'email': 'some@email.com',
        'text': 'some text',
    }
