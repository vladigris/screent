import json
import pytest

from screent.tests.web import base
from screent.tests.core.factories import UserFactory
from .data import get_contact_data

from msg.models import Contact


class TestContact(base.Mixin):
    @pytest.mark.django_db
    def test_01(self, client):
        """
        Should get error with empty subject
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('contact')
        dic = get_contact_data()
        dic['subject'] = ''
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_02(self, client):
        """
        Should get error with empty text
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('contact')
        dic = get_contact_data()
        dic['text'] = ''
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_03(self, client):
        """
        Should get success
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('contact')
        dic = get_contact_data()
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        assert Contact.objects.first() is not None
