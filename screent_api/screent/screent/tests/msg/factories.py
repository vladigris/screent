import factory
from faker import Faker


from msg.models import Notification


from screent.tests.core.factories import UserFactory


class NotificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Notification

    user = factory.SubFactory(UserFactory)
    text = factory.lazy_attribute(lambda _: Faker().sentence())
    is_read = False

    @staticmethod
    def get(user=None, is_read=False):
        if user is None:
            user = UserFactory.get()
        return NotificationFactory(user=user, is_read=is_read)
