import pytest

from screent.tests.web import base
from screent.tests.core.factories import UserFactory
from .factories import NotificationFactory


class TestNotification(base.Mixin):
    @pytest.mark.django_db
    def test_get_notifications(self, client):
        """
        User should get list of notification
        """
        user = UserFactory.get()

        client = self.get_auth_client(user)
        url = self.get_url('notifications')

        NotificationFactory.get(user=user)
        NotificationFactory.get(user=user)
        NotificationFactory.get(user=user, is_read=True)
        NotificationFactory.get()

        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()['results']) == 2

    @pytest.mark.django_db
    def test_mark_notification(self, client):
        """
        User should mark notification as it has been read
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        n = NotificationFactory(user=user)
        assert n.is_read is False

        url = self.get_url('mark_notification', id=n.id)
        r = client.post(url, {}, content_type='application/json')
        assert r.status_code == 204

        n.refresh_from_db()
        assert n.is_read is True
