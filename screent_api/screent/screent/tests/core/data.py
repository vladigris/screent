

def get_computer_data():
    """
    Return data to create computer
    """
    return {
        'name': 'some name',
        'prog_name': 'someprogname'
    }


def get_set_serials_data(computer):
    """
    Returns data for testing set serials.
    """
    computer.serial_mb = ''
    computer.serial_hdd = ''
    computer.serial_proc = ''
    computer.save()

    return {
        'token': computer.token,
        'serial_mb': 'some mb',
        'serial_hdd': 'some hdd',
        'serial_proc': 'some proc',
    }
