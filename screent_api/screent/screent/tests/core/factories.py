import factory
from random import randint
from faker import Faker
from django.utils import timezone

from core.models import Computer
from core.models import Screen
from core.models import Keyboard
from core.models import Version
from core.models import User


class VersionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Version

    name = '1.0'
    is_active = True

    @staticmethod
    def get():
        version = Version.objects.filter(name='1.0').first()
        if version:
            return version
        return VersionFactory()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('email',)

    first_name = factory.lazy_attribute(lambda _: Faker().first_name())
    first_name = factory.lazy_attribute(lambda _: Faker().last_name())
    is_active = True
    joined_at = timezone.now()

    @staticmethod
    def get(email=None):
        if email is None:
            email = Faker().email()
        u = UserFactory(email=email)
        u.set_password('12345678')
        u.save()

        # First version should be available in all tests.
        VersionFactory.get()
        return u


class ComputerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Computer

    name = factory.lazy_attribute(
        lambda _: '{}{}'.format(Faker().word(), randint(10000, 99999)))
    prog_name = factory.lazy_attribute(lambda _: Faker().word())

    @staticmethod
    def get(user=None, version=None, **kwargs):
        if user is None:
            user = UserFactory.get()

        if version is None:
            version = VersionFactory.get()
        return ComputerFactory(user=user, version=version, **kwargs)


class ScreenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Screen

    program = factory.lazy_attribute(lambda _: Faker().word())
    title = factory.lazy_attribute(lambda _: Faker().word())

    @staticmethod
    def get(created_at, computer=None):
        if computer is None:
            computer = ComputerFactory.get()
        return ScreenFactory(created_at=created_at, computer=computer)


class KeyboardFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Keyboard

    computer = factory.SubFactory(ComputerFactory)
    text = factory.lazy_attribute(lambda _: Faker().word())

    @staticmethod
    def get(created_at, computer=None):
        if computer is None:
            computer = ComputerFactory.get()
        return KeyboardFactory(created_at=created_at, computer=computer)
