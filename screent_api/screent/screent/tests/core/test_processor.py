import datetime
import json
import pytest
import random
import uuid
from screent.tests.web import base
from django.conf import settings

from core.models import Keyboard
from core.models import Screen
from msg.models import Log

from core.services.processor import Processor

from .factories import ComputerFactory


class TestProcessor(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        super().setup()
        settings.REDIS_DB.flushdb()

    @pytest.mark.django_db
    def fill_screenshots(self, computer):
        """
        Creates 20 screenshots in Redis
        """
        dt = datetime.datetime.now()
        for _ in range(20):
            dt += datetime.timedelta(seconds=20)

            d = {
                'computerId': computer.id,
                'guid': uuid.uuid4().hex,
                'time': dt.strftime('%Y-%m-%d %H:%M:%S'),
                'title': 'some title',
                'program': 'some program',
                'fileSize': 123456,
                'width': 1920,
                'height': 1080
            }
            rs = settings.REDIS_DB
            key = settings.REDIS_KEY_SCREEN_DATA
            value = json.dumps(d)
            rs.rpush(key, value)

    def fill_keyboards(self, computer):
        """
        Creates 20 keyboards in Redis
        """
        dt = datetime.datetime.now()
        for _ in range(20):
            dt += datetime.timedelta(seconds=20)

            d = {
                'computerId': computer.id,
                'time': dt.strftime('%Y-%m-%d %H:%M:%S'),
                'text': 'some text',
            }
            rs = settings.REDIS_DB
            key = settings.REDIS_KEY_KEYBOARD_DATA
            value = json.dumps(d)
            rs.rpush(key, value)

    def fill_logs(self, computer):
        """
        Creates 20 logs in Redis
        """
        dt = datetime.datetime.now()
        levels = ['INFO', 'ERROR', 'DEBUG', 'WARNING']
        for _ in range(20):
            dt += datetime.timedelta(seconds=20)
            d = {
                'computerGuid': computer.guid,
                'level': random.choice(levels),
                'message': 'some message',
                'createdAt': dt.strftime('%Y-%m-%d %H:%M:%S'),
            }
            rs = settings.REDIS_DB
            key = settings.REDIS_KEY_LOG_DATA
            value = json.dumps(d)
            rs.rpush(key, value)

    @pytest.mark.django_db
    def test_process_screenshots(self, client):
        computer = ComputerFactory.get()
        self.fill_screenshots(computer)

        rs = settings.REDIS_DB
        key = settings.REDIS_KEY_SCREEN_DATA
        count = rs.llen(key)
        assert count > 0

        p = Processor()
        p.process_screenshots()
        assert Screen.objects.count() == count

    @pytest.mark.django_db
    def test_process_keyboards(self, client):
        computer = ComputerFactory.get()
        self.fill_keyboards(computer)

        rs = settings.REDIS_DB
        key = settings.REDIS_KEY_KEYBOARD_DATA
        count = rs.llen(key)
        assert count > 0

        p = Processor()
        p.process_keyboards()
        assert Keyboard.objects.count() == count

    @pytest.mark.django_db
    def test_process_logs(self, client):
        computer = ComputerFactory.get()
        self.fill_logs(computer)

        rs = settings.REDIS_DB
        key = settings.REDIS_KEY_LOG_DATA
        count = rs.llen(key)
        assert count > 0

        p = Processor()
        p.process_logs()
        assert Log.objects.count() == count
