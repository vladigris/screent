import json
import pytest
from django.conf import settings

from core.models import Computer

from screent.tests.web import base
from screent.tests.payment.factories import TariffFactory
from screent.tests.payment.factories import TariffUserFactory

from .factories import ComputerFactory
from .factories import UserFactory
from .data import get_computer_data


class TestComputer(base.Mixin):
    @pytest.mark.django_db
    def test_redis(self, client):
        """
        On every computer save, Redis should have some info
        """
        rs = settings.REDIS_DB
        c = ComputerFactory.get()

        key = Computer.objects.get_redis_key_config(c.token)
        assert rs.get(key) is not None

        key = Computer.objects.get_redis_key_config_snapshot(c.token)
        assert rs.get(key) is not None

    @pytest.mark.django_db
    def test_get_list01(self, client):
        """
        User should get computers
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        ComputerFactory.get(user=user)
        ComputerFactory.get(user=user)
        ComputerFactory.get(user=user, is_removed=True)
        ComputerFactory.get()

        url = self.get_url('computers')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()['results']) == 2

    @pytest.mark.django_db
    def test_get01(self, client):
        """
        User should get detail page
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get(user=user)

        url = self.get_url('computer', guid=c.guid)
        r = client.get(url)
        assert r.status_code == 200

    @pytest.mark.django_db
    def test_get02(self, client):
        """
        User should not get other's user computer
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get()

        url = self.get_url('computer', guid=c.guid)
        r = client.get(url)
        assert r.status_code == 404

    @pytest.mark.django_db
    def test_create01(self, client):
        """
        User should not create computer with empty name
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('computers')
        dic = get_computer_data()
        dic['name'] = ''

        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_create02(self, client):
        """
        User should not create computer with empty prog_name
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('computers')
        dic = get_computer_data()
        dic['prog_name'] = ''

        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_create03(self, client):
        """
        The computer name should be slug
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('computers')
        dic = get_computer_data()
        dic['name'] = 'some!!!'

        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_create04(self, client):
        """
        User should not be able to create second computer in trial mode
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        ComputerFactory.get(user=user)

        url = self.get_url('computers')
        dic = get_computer_data()

        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_create05(self, client):
        """
        User can not create more computers than available in tariff
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        t = TariffFactory.get()
        t.max_computers = 2
        t.save()

        TariffUserFactory(user=user, tariff=t)

        ComputerFactory.get(user=user)
        ComputerFactory.get(user=user)

        url = self.get_url('computers')
        dic = get_computer_data()
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_create06(self, client):
        """
        User should create computer
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('computers')
        dic = get_computer_data()
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

    @pytest.mark.django_db
    def test_update01(self, client):
        """
        The name of computer should not be empty
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get(user=user)

        url = self.get_url('computer', guid=c.guid)
        dic = {'name': ''}

        data = json.dumps(dic)
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_update02(self, client):
        """
        Should not set interval less than minimum interval by tariff
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        t = TariffFactory.get()
        t.max_computers = 4
        t.min_interval = 20
        t.save()

        TariffUserFactory(user=user, tariff=t)

        c = ComputerFactory.get(user=user)

        url = self.get_url('computer', guid=c.guid)
        dic = {'interval': 19}

        data = json.dumps(dic)
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_update03(self, client):
        """
        Should not set interval more than maximum interval
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get(user=user)

        url = self.get_url('computer', guid=c.guid)
        dic = {'interval': settings.MAX_INTERVAL + 1}

        data = json.dumps(dic)
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_update04(self, client):
        """
        User should not edit other's user's computer
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get()

        url = self.get_url('computer', guid=c.guid)
        dic = {'name': 'some'}

        data = json.dumps(dic)
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 404

    @pytest.mark.django_db
    def test_update05(self, client):
        """
        User should be able to set computer on pause
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get(user=user)
        c.is_pause = False
        c.save()

        url = self.get_url('computer', guid=c.guid)
        dic = {'is_pause': True}

        data = json.dumps(dic)
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 200
        c.refresh_from_db()
        assert c.is_pause is True

    @pytest.mark.django_db
    def test_update06(self, client):
        """
        User should edit computer
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get(user=user)

        url = self.get_url('computer', guid=c.guid)
        dic = {'name': 'other'}

        data = json.dumps(dic)
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 200
        c.refresh_from_db()
        assert c.name == dic['name']
