import pytest
from screent.tests.web import base

from .factories import UserFactory
from .factories import VersionFactory


class TestVersion(base.Mixin):
    @pytest.mark.django_db
    def test_get(self, client):
        """
        Get active version.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        VersionFactory.get()
        url = self.get_url('active_version')
        r = client.get(url)
        assert r.status_code == 200
        assert 'name' in r.json()
        assert 'installer_link' in r.json()
