import json
import pytest

from screent.tests.web import base
from .factories import ComputerFactory
from .factories import UserFactory
from .data import get_set_serials_data


class TestInstall(base.Mixin):
    @pytest.mark.django_db
    def test_step1_01(self, client):
        """
        Should not work with empty email
        """
        url = self.get_url('install_step1')
        dic = {
            'email': '',
            'password': '12345678'
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step1_02(self, client):
        """
        Should not work with empty password
        """
        user = UserFactory.get()

        url = self.get_url('install_step1')
        dic = {
            'email': user.email,
            'password': ''
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step1_03(self, client):
        """
        Should not work with incorrect email
        """
        url = self.get_url('install_step1')
        dic = {
            'email': 'incorrect@mail.com',
            'password': '12345678'
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step1_04(self, client):
        """
        Should not work with incorrect password
        """
        user = UserFactory.get()

        url = self.get_url('install_step1')
        dic = {
            'email': user.email,
            'password': 'incorrect'
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step1_05(self, client):
        """
        Should not work if user is not active
        """
        user = UserFactory.get()
        user.is_active = False
        user.save()

        url = self.get_url('install_step1')
        dic = {
            'email': user.email,
            'password': '12345678'
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step1_06(self, client):
        """
        Should not work if user not completed signup
        """
        user = UserFactory.get()
        user.joined_at = None
        user.save()

        url = self.get_url('install_step1')
        dic = {
            'email': user.email,
            'password': '12345678'
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step1_07(self, client):
        """
        Should not work if user doesn't have active computers
        """
        user = UserFactory.get()

        url = self.get_url('install_step1')
        dic = {
            'email': user.email,
            'password': '12345678'
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step1_08(self, client):
        """
        Should get success
        """
        user = UserFactory.get()

        ComputerFactory.get(user=user)

        url = self.get_url('install_step1')
        dic = {
            'email': user.email,
            'password': '12345678'
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200

    @pytest.mark.django_db
    def test_step2_01(self, client):
        """
        Should not work with empty email
        """
        user = UserFactory.get()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_step2')
        dic = {
            'email': '',
            'password': '12345678',
            'id': c.id
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step2_02(self, client):
        """
        Should not work with empty password
        """
        user = UserFactory.get()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_step2')
        dic = {
            'email': user.email,
            'password': '',
            'id': c.id
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step2_03(self, client):
        """
        Should not work with incorrect email
        """
        user = UserFactory.get()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_step2')
        dic = {
            'email': 'incorrect@gmail.com',
            'password': '12345678',
            'id': c.id
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step2_04(self, client):
        """
        Should not work with incorrect password
        """
        user = UserFactory.get()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_step2')
        dic = {
            'email': user.email,
            'password': '22222222',
            'id': c.id
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step2_05(self, client):
        """
        Should not work if user not active
        """
        user = UserFactory.get()
        user.is_active = False
        user.save()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_step2')
        dic = {
            'email': user.email,
            'password': '12345678',
            'id': c.id
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step2_06(self, client):
        """
        Should not work if user not completed signup
        """
        user = UserFactory.get()
        user.joined_at = None
        user.save()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_step2')
        dic = {
            'email': user.email,
            'password': '12345678',
            'id': c.id
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step2_07(self, client):
        """
        Should not work if user does not have active computers
        """
        user = UserFactory.get()

        url = self.get_url('install_step2')
        dic = {
            'email': user.email,
            'password': '12345678',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step2_08(self, client):
        """
        Should not work with incorrect computer id
        """
        user = UserFactory.get()
        ComputerFactory.get(user=user)

        url = self.get_url('install_step2')
        dic = {
            'email': user.email,
            'password': '12345678',
            'id': 1000000000
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step2_09(self, client):
        """
        Should not work if computer belongs to other user
        """
        user = UserFactory.get()
        ComputerFactory.get(user=user)

        c = ComputerFactory.get()

        url = self.get_url('install_step2')
        dic = {
            'email': user.email,
            'password': '12345678',
            'id': c.id
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<error>' in r.content.decode()

    @pytest.mark.django_db
    def test_step2_10(self, client):
        """
        Should get success
        """
        user = UserFactory.get()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_step2')
        dic = {
            'email': user.email,
            'password': '12345678',
            'id': c.id
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert '<root><config>' in r.content.decode()
        assert '</config></root>' in r.content.decode()

    @pytest.mark.django_db
    def test_set_serials_01(self, client):
        """
        Should not work with invalid token
        """
        user = UserFactory.get()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_set_serials')
        dic = get_set_serials_data(c)
        dic['token'] = 'incorrect'

        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert r.content.decode() == 'OK'
        c.refresh_from_db()

        assert c.serial_mb == ''
        assert c.serial_hdd == ''
        assert c.serial_proc == ''

    @pytest.mark.django_db
    def test_set_serials_02(self, client):
        """
        Should not work with empty token
        """
        user = UserFactory.get()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_set_serials')
        dic = get_set_serials_data(c)
        dic['token'] = ''

        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert r.content.decode() == 'OK'
        c.refresh_from_db()

        assert c.serial_mb == ''
        assert c.serial_hdd == ''
        assert c.serial_proc == ''

    @pytest.mark.django_db
    def test_set_serials_03(self, client):
        """
        Should get success
        """
        user = UserFactory.get()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_set_serials')
        dic = get_set_serials_data(c)

        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert r.content.decode() == 'OK'
        c.refresh_from_db()

        assert c.serial_mb == dic['serial_mb']
        assert c.serial_hdd == dic['serial_hdd']
        assert c.serial_proc == dic['serial_proc']

    @pytest.mark.django_db
    def test_mark_install(self, client):
        """
        Should get success
        """
        user = UserFactory.get()
        c = ComputerFactory.get(user=user)

        url = self.get_url('install_mark')
        dic = {'token': c.token}

        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        assert r.content.decode() == 'OK'
        c.refresh_from_db()
        assert c.is_installed is True
