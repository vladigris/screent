import pytest

from faker import Faker
from screent.tests.web import base
from .factories import ComputerFactory


class TestCrypt(base.Mixin):
    @pytest.mark.django_db
    def test_encrypt__decrypt_bytes(self, client):
        c = ComputerFactory.get()

        for _ in range(20):
            text = Faker().sentence()
            bytes = text.encode()

            res = c.encrypt(bytes)
            assert c.decrypt(res) == bytes

    @pytest.mark.django_db
    def test_encrypt__decrypt_string(self, client):
        c = ComputerFactory.get()

        for _ in range(20):
            text = Faker().sentence()
            res = c.encrypt_string(text)
            assert c.decrypt_string(res) == text
