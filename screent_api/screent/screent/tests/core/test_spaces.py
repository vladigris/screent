import pytest
import boto3


from django.conf import settings
from screent.tests.web import base


class TestSpaces(base.Mixin):
    @pytest.mark.django_db
    def test_01(self, client):
        session = boto3.session.Session()
        client = session.client(
            's3',
            region_name='nyc3',
            endpoint_url='https://nyc3.digitaloceanspaces.com',
            aws_access_key_id=settings.SPACES_KEY,
            aws_secret_access_key=settings.SPACES_SECRET)

        response = client.list_buckets()
        spaces = [space['Name'] for space in response['Buckets']]
        print("Spaces List: %s" % spaces)
