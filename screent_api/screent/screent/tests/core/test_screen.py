import datetime
import pytest
from django.utils import timezone

from screent.tests.web import base

from core.models import Screen

from .factories import ComputerFactory
from .factories import ScreenFactory
from .factories import KeyboardFactory
from .factories import UserFactory


class TestScreen(base.Mixin):
    @pytest.mark.django_db
    def test_get_list01(self, client):
        """
        Get list of screens.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get(user=user)

        num = 1
        for i in range(20):
            created_at = timezone.now() + datetime.timedelta(minutes=1)
            screen = ScreenFactory.get(created_at, computer=c)
            KeyboardFactory.get(created_at, computer=screen.computer)
            num += 1

        url = self.get_url('screens', guid=c.guid)
        url += '?page=1'
        r = client.get(url)
        assert r.status_code == 200
        assert Screen.objects.count() == 20

    @pytest.mark.django_db
    def test_get_list02(self, client):
        """
        Filter by title.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get(user=user)
        created_at = timezone.now() - datetime.timedelta(minutes=1)
        s = ScreenFactory.get(created_at, computer=c)
        s.title = 'Medicine'
        s.save()

        created_at = timezone.now() - datetime.timedelta(minutes=2)
        s2 = ScreenFactory.get(created_at, computer=c)
        s2.title = 'Some other'
        s2.save()

        url = self.get_url('screens', guid=c.guid)
        url += '?page=1'
        url += '&title=Med'

        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()['screens']) == 1

    @pytest.mark.django_db
    def test_get_list03(self, client):
        """
        Test pagination
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        c = ComputerFactory.get(user=user)
        created_at = timezone.now() - datetime.timedelta(minutes=1)
        ScreenFactory.get(created_at, computer=c)

        url = self.get_url('screens', guid=c.guid)
        url += '?page=1'
        r = client.get(url)
        assert r.status_code == 200

        url = self.get_url('screens', guid=c.guid)
        url += '?page=2'
        r = client.get(url)
        assert r.status_code == 404
