import json
import pytest

from core.models import User
from web.models import Emailer

from screent.tests.core.factories import UserFactory
from . import base


class TestAuth(base.Mixin):
    @pytest.mark.django_db
    def test_signup01(self, client):
        """
        Test with correct email.
        User should get email with signup link.
        """
        url = self.get_url('signup')
        dic = {
            'email': 'bob@gmail.com',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

        user = UserFactory(email='bob@gmail.com')
        assert user.email == 'bob@gmail.com'

        em = Emailer.objects.first()
        assert user.signup_token in em.txt
        assert user.signup_token in em.html

    @pytest.mark.django_db
    def test_signup02(self, client):
        """
        Test with incorrect email.
        """
        url = self.get_url('signup')
        dic = {
            'email': '!!!',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup03(self, client):
        """
        Test with empty email.
        """
        url = self.get_url('signup')
        dic = {
            'email': '',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup05(self, client):
        """
        Test with taken email.
        """
        user = UserFactory.get()

        url = self.get_url('signup')
        dic = {
            'email': user.email,
            'password': '12345678',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_step2_check_01(self, client):
        """
        Test with correct data.
        """
        url = self.get_url('signup_step2_check')
        user = UserFactory.get()
        user.joined_at = None
        user.save()
        data = {
            'guid': user.guid,
            'signup_token': user.signup_token
        }

        r = client.post(url, data)
        assert r.status_code == 204

    @pytest.mark.django_db
    def test_signup_step2_check_02(self, client):
        """
        Test with incorrect data.
        """
        url = self.get_url('signup_step2_check')
        data = {
            'guid': 'aaa',
            'signup_token': 'bbb'
        }
        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_step2_01(self, client):
        """
        Test with correct data.
        """
        url = self.get_url('signup_step2')
        user = UserFactory.get()
        user.joined_at = None
        user.save()
        data = {
            'guid': user.guid,
            'signup_token': user.signup_token,
            'password': '12345678',
        }

        r = client.post(url, data)
        assert r.status_code == 201

        d = r.json()
        assert user.get_token().key == d['token']

        user.refresh_from_db()
        assert user.joined_at is not None

    @pytest.mark.django_db
    def test_signup_step2_02(self, client):
        """
        Test with incorrect guid.
        """
        url = self.get_url('signup_step2')
        user = UserFactory.get()
        user.joined_at = None
        user.save()
        data = {
            'guid': 'wrong',
            'signup_token': user.signup_token,
            'password': '12345678',
        }

        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_step2_03(self, client):
        """
        Test with incorrect signup_token.
        """
        url = self.get_url('signup_step2')
        user = UserFactory.get()
        user.joined_at = None
        user.save()
        data = {
            'guid': user.guid,
            'signup_token': 'wrong',
            'password': '12345678',
        }

        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_step2_04(self, client):
        """
        Test with empty password.
        """
        url = self.get_url('signup_step2')
        user = UserFactory.get()
        user.joined_at = None
        user.save()
        data = {
            'guid': user.guid,
            'signup_token': 'wrong',
            'password': '',
        }

        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login01(self, client):
        """
        Test with correct data.
        """
        user = UserFactory.get()
        user.set_password('111')
        user.save()

        url = self.get_url('login')
        data = {
            'email': user.email,
            'password': '111',
        }

        r = client.post(url, data)
        assert r.status_code == 200

        d = r.json()
        user = User.objects.get(id=user.id)
        assert user.get_token().key == d['token']

    @pytest.mark.django_db
    def test_login02(self, client):
        """
        Test with empty email.
        """
        user = UserFactory.get()
        user.set_password('111')
        user.save()

        url = self.get_url('login')
        data = {
            'email': '',
            'password': '111',
        }

        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login03(self, client):
        """
        Test with empty password.
        """
        user = UserFactory.get()
        user.set_password('111')
        user.save()

        url = self.get_url('login')
        data = {
            'email': user.email,
            'password': '',
        }

        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login04(self, client):
        """
        Test with not existing email.
        """
        user = UserFactory.get()
        user.set_password('111')
        user.save()

        url = self.get_url('login')
        data = {
            'email': 'az@az.com',
            'password': '111',
        }

        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login05(self, client):
        """
        Test with incorrect password.
        """
        user = UserFactory.get()
        user.set_password('111')
        user.save()

        url = self.get_url('login')
        data = {
            'email': user.email,
            'password': '555',
        }

        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login06(self, client):
        """
        User can't login if is_active=False
        """
        user = UserFactory.get()
        user.set_password('111')
        user.is_active = False
        user.save()

        url = self.get_url('login')
        data = {
            'email': user.email,
            'password': '111',
        }

        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login07(self, client):
        """
        User can not login if joined_at is None
        """
        user = UserFactory.get()
        user.set_password('111')
        user.joined_at = None
        user.save()

        url = self.get_url('login')
        data = {
            'email': user.email,
            'password': '111',
        }

        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_reset_password01(self, client):
        """
        User should get email with reset password link.
        """
        user = UserFactory.get()

        url = self.get_url('reset_password')
        dic = {
            'email': user.email,
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

        em = Emailer.objects.first()
        assert user.reset_token in em.txt
        assert user.reset_token in em.html

    @pytest.mark.django_db
    def test_reset_password02(self, client):
        """
        User should get error if email not exists
        """
        url = self.get_url('reset_password')
        dic = {
            'email': 'bob@gmail.com',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_reset_password_check01(self, client):
        """
        Test reset password check for params from reset password link.
        """
        user = UserFactory.get()
        user.reset_token = 'some'
        user.save()

        url = self.get_url('reset_password_check')
        dic = {
            'user_guid': user.guid,
            'reset_token': user.reset_token
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

    @pytest.mark.django_db
    def test_reset_password_check02(self, client):
        """
        Test reset password check for params from reset password link.
        Should get error with incorrect email
        """
        user = UserFactory.get()
        user.reset_token = 'some'
        user.save()

        url = self.get_url('reset_password_check')
        dic = {
            'user_guid': 'incorrect',
            'reset_token': user.reset_token
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_reset_password_check03(self, client):
        """
        Test reset password check for params from reset password link.
        Should get error with incorrect token
        """
        user = UserFactory.get()
        user.reset_token = 'some'
        user.save()

        url = self.get_url('reset_password_check')
        dic = {
            'user_guid': user.guid,
            'reset_token': 'incorrect'
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_reset_password_complete01(self, client):
        """
        Test reset password complete.
        Test only success case, because not success cases
        tested by reset_password_check and they use the same validation
        mixin.
        """
        user = UserFactory.get()
        user.reset_token = 'some'
        user.save()

        url = self.get_url('reset_password_complete')
        dic = {
            'user_guid': user.guid,
            'reset_token': user.reset_token,
            'password': '12345678'
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        assert r.json()['token'] == user.get_token().key
        user.refresh_from_db()
        assert user.reset_token == ''
        assert user.reset_at is None
