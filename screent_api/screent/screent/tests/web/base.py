import pytest
from django.core.urlresolvers import reverse
from django.apps import apps
from django.conf import settings
from rest_framework.test import APIClient
from web.management.commands.createemails import Command


class Mixin:
    @pytest.mark.django_db
    def setup(self):
        self.init_emailer()

    def get_auth_client(self, user):
        """
        Returns auth api client.
        """
        User = apps.get_model('core', 'User')

        client = APIClient()
        user = User.objects.get(id=user.id)
        token = user.get_token()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        return client

    def get_url(self, name, version='v1', **kwargs):
        """
        Wrapper to reverse url without kwargs.
        """
        d = {'version': version}
        d.update(kwargs)
        return reverse(name, kwargs=d)

    def init_emailer(self):
        cmd = Command()
        cmd.create_reset_password()
        cmd.create_signup()

    def print_emailer(self):
        Emailer = apps.get_model('web', 'Emailer')

        for em in Emailer.objects.all():
            print('-------')
            print('email_to: {}'.format(em.email_to))
            print('email_from: {}'.format(em.email_from))
            print('subject: {}'.format(em.subject))
            print('txt: {}'.format(em.txt))
