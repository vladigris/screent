import json
import pytest

from . import base
from screent.tests.core.factories import UserFactory


class TestProfile(base.Mixin):
    @pytest.mark.django_db
    def test_get_profile(self, client):
        """
        User should get own profile
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)
        url = self.get_url('user_profile')
        r = client.get(url)
        assert r.status_code == 200
        assert r.json()['id'] == user.id

    @pytest.mark.django_db
    def test_update_profile(self, client):
        """
        User should update profile
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('user_profile')
        dic = {
            'first_name': 'other name',
        }
        data = json.dumps(dic)
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 200

        user.refresh_from_db()
        assert user.first_name == dic['first_name']

    @pytest.mark.django_db
    def test_change_password01(self, client):
        """
        User should change password.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.save()

        client = self.get_auth_client(user)
        url = self.get_url('user_profile_change_password')
        dic = {
            'old_password': '11111111',
            'new_password': '22222222',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

    @pytest.mark.django_db
    def test_change_password02(self, client):
        """
        User should not change password.
        Min lenght should be no less than 8 symbols.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.save()

        client = self.get_auth_client(user)
        url = self.get_url('user_profile_change_password')
        dic = {
            'old_password': '11111111',
            'new_password': '222',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_change_password03(self, client):
        """
        User should not change password.
        Current password is not correct.
        """
        user = UserFactory.get()

        client = self.get_auth_client(user)
        url = self.get_url('user_profile_change_password')
        dic = {
            'old_password': 'incorrect',
            'new_password': '22222222',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_update_last_access(self, client):
        """
        User's last_access_at should be updated
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('update_last_access')
        r = client.post(url, {}, content_type='application/json')
        assert r.status_code == 204
