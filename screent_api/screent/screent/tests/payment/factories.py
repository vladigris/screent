import factory
from random import randint
from faker import Faker

from payment.models import Tariff
from payment.models import TariffUser

from screent.tests.core.factories import UserFactory


class TariffFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Tariff

    name = factory.lazy_attribute(
        lambda _: '{}{}'.format(Faker().word(), randint(10000, 99999)))
    price = 10
    day_limit = 100
    storage = 100

    @staticmethod
    def get():
        return TariffFactory()


class TariffUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TariffUser

    is_active = True

    @staticmethod
    def get(user=None, tariff=None):
        if user is None:
            user = UserFactory.get()

        if tariff is None:
            tariff = TariffFactory.get()

        return TariffUserFactory.get(user=user, tariff=tariff)
