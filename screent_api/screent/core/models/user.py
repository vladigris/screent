import datetime
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils import timezone
from django.apps import apps
from django.conf import settings
from web.services.helpers import get_guid
from django.db.models import CharField
from django.db.models import EmailField
from django.db.models import BooleanField
from django.db.models import DateTimeField
from django.db.models import ForeignKey


class UserManager(BaseUserManager):
    def create_user(self, email, password):
        if not email:
            raise ValueError('Please input email.')
        user = self.model(email=email)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password=password)
        user.is_admin = True
        user.joined_at = timezone.now()
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    guid = CharField(max_length=32, default=get_guid)
    lang = ForeignKey('web.Lang', null=True, blank=True, default=None)
    email = EmailField(
        max_length=255, null=True, unique=True, default=None)
    first_name = CharField(max_length=50, blank=True, default='')
    last_name = CharField(max_length=50, blank=True, default='')
    signup_token = CharField(max_length=255, blank=True)
    joined_at = DateTimeField(blank=True, default=None, null=True)
    reset_token = CharField(max_length=255)
    reset_at = DateTimeField(blank=True, default=None, null=True)
    last_access_at = DateTimeField(auto_now_add=True)
    new_email = EmailField(
        max_length=255, null=True, default=None)
    new_email_token = CharField(
        max_length=255, null=True, default=None, blank=True)
    new_email_expire_at = DateTimeField(
        null=True, default=None, blank=True)
    trial_ended_at = DateTimeField(null=True, default=None, blank=True)
    is_active = BooleanField(default=True, verbose_name='active')
    is_admin = BooleanField(default=False, verbose_name='admin')
    is_superuser = BooleanField(default=False, verbose_name='superuser')

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        app_label = 'core'
        verbose_name_plural = 'Users'
        ordering = ['email']
        db_table = 'users'

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.first_name + ' ' + self.last_name

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def is_signup_completed(self):
        if self.joined_at is not None:
            return True
        return False

    @property
    def computers_qs(self):
        return self.computers.filter(is_removed=False)

    @property
    def tariff(self):
        TariffUser = apps.get_model('payment', 'TariffUser')
        tu = TariffUser.objects.filter(user=self, is_active=True).first()
        if not tu:
            return None
        return tu.tariff

    @property
    def num_computers(self):
        return self.computers_qs.count()

    @property
    def num_installed_computers(self):
        return self.computers_qs.filter(is_installed=True).count()

    @property
    def num_screenshots(self):
        Screen = apps.get_model('core', 'Screen')
        return Screen.objects.filter(
            computer__user_id=self.id, computer__is_removed=False).count()

    def get_token(self):
        Token = apps.get_model('core', 'Token')
        token = Token.objects.filter(
            user=self, expire_at__gt=timezone.now()).first()
        if token:
            return token
        return Token.objects.create(self)

    def update_token(self):
        now = timezone.now()
        mins = settings.TOKEN_EXPIRE_MINUTES
        t = self.get_token()
        t.expire_at = now + datetime.timedelta(minutes=mins)
        t.save()
        return t

    def get_signup_link(self):
        dic = dict(
            url=settings.FRONTEND_URL,
            user_guid=self.guid,
            signup_token=self.signup_token
        )
        link = '{url}/signup/{user_guid}/{signup_token}'
        return link.format(**dic)

    def get_reset_password_link(self):
        dic = dict(
            url=settings.FRONTEND_URL,
            user_guid=self.guid,
            reset_token=self.reset_token
        )
        link = '{url}/reset_password/{user_guid}/{reset_token}'
        return link.format(**dic)

    def signup(self):
        Emailer = apps.get_model('web', 'Emailer')
        Email = apps.get_model('web', 'Email')
        Emailer.objects.process_email(Email.SIGNUP, user=self)

    def reset_password(self):
        Emailer = apps.get_model('web', 'Emailer')
        Email = apps.get_model('web', 'Email')
        self.reset_token = get_guid()
        self.reset_at = timezone.now()
        self.save()
        Emailer.objects.process_email(Email.RESET_PASSWORD, user=self)

    def reset_password_complete(self, new_password):
        self.set_password(new_password)
        self.reset_token = ''
        self.reset_at = None
        self.save()

    def change_email_request(self, new_email):
        Emailer = apps.get_model('web', 'Emailer')
        Email = apps.get_model('web', 'Email')

        self.new_email = new_email
        self.new_email_token = get_guid()
        self.new_email_expire_at =\
            timezone.now() + datetime.timedelta(hours=1)
        self.save()
        Emailer.objects.process_email(Email.CHANGE_EMAIL, user=self)

    def change_email_confirm(self, token):
        """
        Returns bool.
        """
        if self.new_email_token != token:
            return False

        if timezone.now() > self.new_email_expire_at:
            return False

        # TODO: create notification
        d = {
            'user': self,
            'email_old': self.email,
            'email_new': self.new_email,
        }
        return True

    @property
    def new_email_expired(self):
        if not self.new_email_expire_at:
            return True
        return timezone.now() > self.new_email_expire_at

    def set_trial_ended(self):
        """
        Initially trial period is None.
        As soon as we get first screenshot from user,
        set trial_ended_at
        """
        self.trial_ended_at = timezone.now()\
            + datetime.timedelta(days=settings.TRIAL_PERIOD)

    def save(self, *args, **kwargs):
        Token = apps.get_model('core', 'Token')
        created = True if not self.pk else False
        super().save(*args, **kwargs)

        if created:
            self.signup_token = get_guid()
            Token.objects.create(user=self)
            self.save()
