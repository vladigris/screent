import datetime
import os
from django.contrib import admin
from django.apps import apps
from django.utils import timezone
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import IntegerField
from django.db.models import DateTimeField

from web.services.helpers import get_guid


class StatManager(Manager):
    TERMS = ['30d', '7d', '3d', '1d', '1h']

    def validate(self, name, term):
        if name not in ['screens', 'keyboards']:
            raise ValueError('Incorrect name in StatManager get_stat')

        if term not in self.TERMS:
            raise ValueError('Incorrect term in StatManager get_stat')

    def get_stat(self, computer_id, name, term):
        """
        name: screens / keyboards
        term: 30d / 7d / 3d / 1d / 1h
        """
        Screen = apps.get_model('computers', 'Screen')
        Keyboard = apps.get_model('computers', 'Keyboard')

        self.validate(name, term)

        if name == 'screens':
            model = Screen
        elif name == 'keyboards':
            model = Keyboard

        if term.endswith('d'):
            days = int(term.rstrip('d'))
            delta = datetime.timedelta(days=days)
        elif term.endswith('h'):
            hours = int(term.rstrip('h'))
            delta = datetime.timedelta(hours=hours)

        created_at = timezone.now() - delta
        return model.objects.filter(
            computer_id=computer_id, created_at__gte=created_at).count()

    def get_screens_errors_count(self, computer_id):
        Computer = apps.get_model('computers', 'Computer')
        c = Computer.objects.get(id=computer_id)
        if not os.path.exists(c.error_screen_dir):
            return 0
        return len(os.listdir(c.error_screen_dir))

    def get_keyboards_errors_count(self, computer_id):
        Computer = apps.get_model('computers', 'Computer')
        c = Computer.objects.get(id=computer_id)
        if not os.path.exists(c.error_log_dir):
            return 0
        return len(os.listdir(c.error_log_dir))

    def update_stat(self):
        self.model.objects.all().delete()
        Computer = apps.get_model('computers', 'Computer')
        for c in Computer.objects.computers():
            stat = self.model(computer_id=c.id)
            for term in self.TERMS:
                attr = 'screens_{}'.format(term)
                setattr(stat, attr, self.get_stat(c.id, 'screens', term))

                attr = 'keyboards_{}'.format(term)
                setattr(
                    stat, attr, self.get_stat(c.id, 'keyboards', term))

            stat.screens_error_count = self.get_screens_errors_count(c.id)
            stat.keyboards_error_count = self.get_keyboards_errors_count(c.id)

            stat.save()


class Stat(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    computer = ForeignKey('Computer')
    screens_30d = IntegerField(default=0)
    screens_7d = IntegerField(default=0)
    screens_3d = IntegerField(default=0)
    screens_1d = IntegerField(default=0)
    screens_1h = IntegerField(default=0)
    keyboards_30d = IntegerField(default=0)
    keyboards_7d = IntegerField(default=0)
    keyboards_3d = IntegerField(default=0)
    keyboards_1d = IntegerField(default=0)
    keyboards_1h = IntegerField(default=0)
    screens_error_count = IntegerField(default=0)
    keyboards_error_count = IntegerField(default=0)
    created_at = DateTimeField(default=timezone.now)

    objects = StatManager()

    class Meta:
        app_label = 'core'
        verbose_name = 'stat'
        verbose_name_plural = 'stat'
        ordering = ['computer']
        db_table = 'stats'

    def __str__(self):
        return str(self.id)


@admin.register(Stat)
class StatAdmin(admin.ModelAdmin):
    list_display = (
        'computer',
        'created_at',
        'screens_30d',
        'screens_7d',
        'screens_3d',
        'screens_1d',
        'screens_1h',
        'keyboards_30d',
        'keyboards_7d',
        'keyboards_3d',
        'keyboards_1d',
        'keyboards_1h',
        'screens_error_count',
        'keyboards_error_count'
    )
    list_filter = ('computer',)
