import os
from django.conf import settings
from django.contrib import admin
from django.utils import timezone
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import IntegerField
from django.db.models import DateTimeField

from web.services.helpers import get_guid


class ScreenManager(Manager):
    def create_screen(self, computer, title, program, dt):
        """
        Used in generator and do not used in processing.
        """
        try:
            s = Screen.objects.get(
                computer=computer, created_at=dt)
            return s
        except Screen.DoesNotExist:
            pass

        s = Screen(
            program=program,
            computer=computer,
            created_at=dt,
            title=title)
        s.save()
        return s


class Screen(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    computer = ForeignKey('Computer', related_name='screens')
    program = CharField(
        max_length=255, blank=True, null=True, default=None)
    file_key = CharField(
        max_length=100, null=True, default=None, blank=True)
    title = CharField(
        max_length=500, null=True, default=None, blank=True)
    width = IntegerField(default=0)
    height = IntegerField(default=0)
    created_at = DateTimeField()
    file_size = IntegerField(
        null=True, default=None, blank=True)
    minute_index = CharField(
        null=True,
        default=None,
        db_index=True,
        max_length=50,
        help_text='created_at index rounded to minutes: yyyymmddHHMM')

    objects = ScreenManager()

    class Meta:
        app_label = 'core'
        verbose_name = 'screen'
        verbose_name_plural = 'screens'
        ordering = ['-created_at']
        unique_together = ['computer', 'created_at']
        db_table = 'screens'

    def __str__(self):
        if self.file_key is not None:
            return self.file_key
        return self.guid

    def save(self, *args, **kwargs):
        if not self.pk:
            self.file_key = '{}/{}.jpg'.format(self.computer.guid,
                                               self.guid)
        self.minute_index = self.created_at.strftime('%Y%m%d%H%M')
        super().save(*args, **kwargs)

    @property
    def src(self):
        return 'https://{}.nyc3.digitaloceanspaces.com/screens/{}'.format(
            settings.BUCKET_NAME, self.file_key)

    def generate_path(self):
        """
        Used in generator and don't used in processing
        """
        folder = '/home/vlad/files/screenshots/{}/{}/'.format(
            self.computer.guid,
            timezone.now().strftime('%Y-%m-%d'))
        os.makedirs(folder, mode=0o777, exist_ok=True)
        return folder + self.guid + '.jpg'


@admin.register(Screen)
class ScreenAdmin(admin.ModelAdmin):
    list_display = (
        'computer',
        'user',
        'program',
        'thumb',
        'title',
        'width',
        'height',
        'created_at',
        'minute_index',
    )

    list_filter = ('computer',)

    if not settings.PRODUCTION:
        list_display += ('file_key',)

    list_per_page = 100
    search_fields = ('computer__user__email',)

    def thumb(self, obj):
        return '<img src="{}" style="width:100px;height70px;" />'.format(
            obj.src)
    thumb.short_description = 'thumb'
    thumb.allow_tags = True

    def user(self, obj):
        return obj.computer.user.email
    user.short_description = 'user'
