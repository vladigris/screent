import datetime
from django.contrib import admin
from django.db import IntegrityError
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import TextField
from django.db.models import DateTimeField

from web.services.helpers import get_guid


class KeyboadManager(Manager):
    """
    Used in generator and do not used in processing
    """
    def create_keyboard(self, computer, text, dt):
        try:
            k = Keyboard(computer=computer, text=text, created_at=dt)
            k.save()
        except IntegrityError:
            k = Keyboard(computer=computer, created_at=dt)
        return k

    def process_index(self, index):
        l = []
        dt = datetime.datetime.strptime(index, '%Y%m%d%H%M')
        for i in range(0, 5):
            dt_new = dt - datetime.timedelta(minutes=i)
            l.append(dt_new.strftime('%Y%m%d%H%M'))

        l.append(dt.strftime('%Y%m%d%H%M'))

        for i in range(0, 5):
            dt_new = dt + datetime.timedelta(minutes=i)
            l.append(dt_new.strftime('%Y%m%d%H%M'))
        return l

    def get_indexes(self, indexes):
        """
        Input: list of string indexes: yyyymmddhhmm
        Returns: list of more indexes. For each index add
        -5 minutes, -4 minutes, -3 minutes, -2 minutes, -1 minute
        +1 minute, +2 minute, +3 minute, +4 minute, +5 minute
        """
        indexes = list(set(indexes))
        l = []
        for index in indexes:
            l += self.process_index(index)
        return list(set(l))


class Keyboard(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    computer = ForeignKey('Computer', related_name='keyboards')
    text = TextField()
    created_at = DateTimeField()
    minute_index = CharField(
        null=True,
        default=None,
        db_index=True,
        max_length=50,
        help_text='created_at index rounded to minutes: yyyymmddHHMM')

    objects = KeyboadManager()

    class Meta:
        app_label = 'core'
        ordering = ['-created_at']
        unique_together = ['computer', 'created_at']
        db_table = 'keyboards'

    def __str__(self):
        return '{}...'.format(self.text[:50])

    def save(self, *args, **kwargs):
        self.minute_index = self.created_at.strftime('%Y%m%d%H%M')
        super().save(*args, **kwargs)


@admin.register(Keyboard)
class KeyboardAdmin(admin.ModelAdmin):
    list_display = (
        'computer',
        'created_at',
        'minute_index',
        'text'
    )
    list_filter = ('computer',)
    search_fields = ('computer__user__email',)
