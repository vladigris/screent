import base64
import logging
import uuid
from Crypto.Cipher import AES
from collections import OrderedDict
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from xml.etree.ElementTree import tostring

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib import admin
from django.core.validators import RegexValidator
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.apps import apps
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import SlugField
from django.db.models import SmallIntegerField
from django.db.models import BooleanField
from django.db.models import DateTimeField


from core.services.pkcs7 import PKCS7Encoder
from web.services.helpers import randstring
from web.services.helpers import get_guid

logger = logging.getLogger('app')


class ComputerManager(Manager):
    def create_computer(self, user, name, prog_name):
        Version = apps.get_model('core', 'Version')

        c = Computer(
            user=user, name=name, prog_name=prog_name)

        c.version = Version.objects.get(is_active=True)
        c.interval = c.min_interval
        c.day_limit = c.max_day_limit
        c.save()
        return c

    def computers(self):
        return Computer.objects.filter(is_removed=False)

    def xml_error(self, message):
        """
        Returns <root><error>Error here</error></root>
        """
        s = '<root><error>{}</error></root>'
        return s.format(message)

    def install_step1_xml(self, user):
        """
        Returns XML for install_step1
        <root>
           <computers>
               <computer id=... name=... />
               <computer id=... name=... />
           </computers>
           <license>...</license>
       </root>
        """
        root = Element('root')
        child = SubElement(root, 'computers')
        for c in user.computers_qs:
            SubElement(child, 'computer', id=str(c.id), name=c.name)
        lic = SubElement(root, 'license')
        lic.text = 'TODO: License here'
        return tostring(root).decode()

    def install_step2_xml(self, computer):
        """
        Returns XML for install_step1
        <root><config>hash</config></root>
        """
        root = Element('root')
        child = SubElement(root, 'config')
        child.text = computer.enc_config
        return tostring(root).decode()

    def set_to_redis(self, key, value):
        rs = settings.REDIS_DB
        rs.set(key, value)
        rs.expire(key, 3600)

    def get_redis_key_config(self, token):
        return settings.REDIS_KEY_COMPUTER_CONFIG + token

    def get_redis_key_config_snapshot(self, token):
        return settings.REDIS_KEY_COMPUTER_CONFIG_SNAPSHOT + token

    def get_redis_key_computer(self, token):
        return settings.REDIS_KEY_COMPUTER + token

    def get_config_by_token(self, token):
        """
        First look in Redis.
        Then look in Postgre, set to Redis and return.
        """
        key = self.get_redis_key_config(token)
        value = settings.REDIS_DB.get(key)
        if value is not None:
            return value

        c = self.model.objects.filter(token=token).first()
        if c is None:
            return None

        value = c.enc_config
        self.set_to_redis(key, value)
        return value

    def get_config_snapshot_by_token(self, token):
        """
        First look in Redis.
        Then look in Postgre, set to Redis and return.
        """
        key = self.get_redis_key_config_snapshot(token)
        value = settings.REDIS_DB.get(key)
        if value is not None:
            return value

        c = self.model.objects.filter(token=token).first()
        if c is None:
            return None

        self.set_to_redis(key, c.snapshot)
        return c.snapshot


class Computer(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    user = ForeignKey('User', related_name='computers')
    version = ForeignKey('Version')
    name = CharField(
        max_length=50,
        validators=[
            RegexValidator(
                regex='^[A-Za-z0-9_\s-]+$',
                message=_('Allowed chars: letters, numbers, dash, \
                underscore and space.')
            ),
        ]
    )
    token = CharField(max_length=50, unique=True, default=get_guid)
    prog_name = SlugField(max_length=100)
    conf_name = CharField(max_length=50, default=get_guid)
    data_name = CharField(
        max_length=50, default=get_guid,
        help_text='name of file where computer saves some data')
    screen_dir = CharField(max_length=50, default=get_guid)
    log_dir = CharField(max_length=50, default=get_guid)
    interval = SmallIntegerField(default=0)
    day_limit = SmallIntegerField(default=0)
    day_limit_filename = CharField(max_length=50, default=get_guid)
    serial_mb = CharField(
        max_length=255, null=True, default=None, blank=True)
    serial_proc = CharField(
        max_length=255, null=True, default=None, blank=True)
    serial_hdd = CharField(
        max_length=255, null=True, default=None, blank=True)
    created_at = DateTimeField(auto_now_add=True)
    snapshot = CharField(
        default=get_guid,
        max_length=50,
        help_text='updates with unique guid after each save')
    is_pause = BooleanField(default=False)
    is_active = BooleanField(
        default=True,
        help_text='Internal. Desktop program should know is it active.')
    is_installed = BooleanField(default=False)
    is_removed = BooleanField(default=False)
    is_removed_on_desktop = BooleanField(default=False)

    objects = ComputerManager()

    class Meta:
        app_label = 'core'
        verbose_name = 'Computer'
        verbose_name_plural = 'Computers'
        ordering = ['user', 'name']
        unique_together = ['user', 'name']
        db_table = 'computers'

    def __str__(self):
        return self.name

    @property
    def config(self):
        """
        If implementation is changed, change also
        appropriate method in GoLang.
        """
        dic = OrderedDict()
        dic['Lang'] = 'en'
        dic['Version'] = self.version.name
        dic['WorkerChecksum'] = self.version.checksum
        dic['WorkerDownloadLink'] = self.version.worker_link
        dic['Ip'] = settings.SERVER_APP_IP
        dic['Ips'] = settings.SERVER_APP_IPS
        dic['Token'] = self.token
        dic['WorkerName'] = self.prog_name
        dic['WorkerDir'] = self.prog_dir
        dic['WorkerConfName'] = self.conf_name
        dic['ScreenDir'] = self.screen_dir
        dic['LogDir'] = self.log_dir
        dic['Interval'] = self.interval
        dic['DayLimit'] = self.day_limit
        dic['DayLimitFileName'] = self.day_limit_filename
        dic['Snapshot'] = self.snapshot
        dic['IsActive'] = self.is_active
        dic['IsPause'] = self.is_pause
        dic['IsRemoved'] = self.is_removed
        return dic

    @property
    def enc_config(self):
        """
        <root>
            <config>
                <name>value</name>
                <Ips><Ip></Ip></Ips>
                ...
        Returns crypted xml
        """
        dic = self.config

        root = Element('root')
        config = SubElement(root, 'config')
        for k, v in dic.items():
            el = SubElement(config, k)
            if k in ['Ips']:
                if v is not None:
                    for item in v:
                        se = SubElement(el, k.rstrip('s'))
                        se.text = item
            else:
                el.text = str(v)

        s = tostring(root).decode()
        return self.encrypt_string(s)

    @property
    def prog_dir(self):
        return self.prog_name

    def encrypt(self, bytes):
        """
        Returns IV + cipher_bytes

        Encrypts bytes, then adds IV (random 16 bytes) before
        encrypted bytes
        """
        encoder = PKCS7Encoder()
        pad_bytes = encoder.encode(bytes)

        iv = randstring(16).encode()
        aes = AES.new(
            self.version.crypt_key, AES.MODE_CBC, iv)

        cipher = aes.encrypt(pad_bytes)
        return b''.join([iv, cipher])

    def decrypt(self, bytes):
        """
        Take iv as first 16 bytes.
        Returns decrypted bytes.
        """
        iv = bytes[:16]
        enc_bytes = bytes[16:]

        aes = AES.new(
            self.version.crypt_key, AES.MODE_CBC, iv)
        dec_bytes = aes.decrypt(enc_bytes)

        encoder = PKCS7Encoder()
        return encoder.decode(dec_bytes)

    def encrypt_string(self, text):
        """
        Returns string as base64.
        1) Convert string to bytes and encrypts bytes
        2) Returns base64 string
        """
        bytes = text.encode()
        enc_bytes = self.encrypt(bytes)
        return base64.b64encode(enc_bytes).decode()

    def decrypt_string(self, text):
        """
        Returns decrypted string

        1) Convert text from base64 to bytes
        2) Decrypt bytes
        """
        bytes = text.encode()
        enc_bytes = base64.b64decode(bytes)
        dec_bytes = self.decrypt(enc_bytes)
        return dec_bytes.decode()

    @property
    def total_screens(self):
        return self.screens.count()

    @property
    def min_date(self):
        """
        The date of first available screen.
        """
        Screen = apps.get_model('core', 'Screen')
        try:
            s = Screen.objects.filter(computer=self).earliest('dt')
        except Screen.DoesNotExist:
            return None
        return s.dt

    @property
    def max_date(self):
        """
        The date of last available screen
        """
        Screen = apps.get_model('core', 'Screen')
        try:
            s = Screen.objects.filter(computer=self).latest('dt')
        except Screen.DoesNotExist:
            return None
        return s.dt

    @property
    def min_interval(self):
        """
        Returns minimum interval accordingly to tariff.
        """
        Tariff = apps.get_model('payment', 'Tariff')
        t = self.user.tariff
        if t is None:
            return Tariff.DEFAULT_INTERVAL
        return t.min_interval

    @property
    def max_day_limit(self):
        """
        Returns day limit accordingly to tariff.
        """
        Tariff = apps.get_model('payment', 'Tariff')
        t = self.user.tariff
        if t is None:
            return Tariff.DEFAULT_DAY_LIMIT
        return t.day_limit

    def save(self, *args, **kwargs):
        self.snapshot = uuid.uuid4().hex
        super().save(*args, **kwargs)


@receiver(post_save, sender=Computer)
def update_remove(sender, instance, created, **kwargs):
    """
    Computer's name should be unique
    """
    if instance.is_removed is True and instance.is_removed_on_desktop is False:
        new_name = instance.name + ' (removed) ' + str(instance.id)
        Computer.objects.filter(id=instance.id)\
            .update(name=new_name, is_removed_on_desktop=True)


@receiver(post_save, sender=Computer)
def update_redis(sender, instance, created, **kwargs):
    """
    Update Redis on each save
    """
    key = Computer.objects.get_redis_key_config(instance.token)
    Computer.objects.set_to_redis(key, instance.enc_config)

    key = Computer.objects.get_redis_key_config_snapshot(instance.token)
    Computer.objects.set_to_redis(key, instance.snapshot)


@admin.register(Computer)
class ComputerAdmin(admin.ModelAdmin):
    list_filter = (
        'is_active',
        'is_pause'
    )
    list_display = (
        'name',
        'user',
        'tariff',
        'version',
        'prog_name',
        'interval',
        'is_active',
        'is_pause'
    )
    readonly_fields = (
        'token',
        'prog_name',
        'conf_name',
        'screen_dir',
        'log_dir',
        'serial_mb',
        'serial_proc',
        'serial_hdd',
        'created_at'
    )

    def add_view(self, request, extra_context=None):
        self.fields = (
            'user',
            'version',
            'name',
            'prog_name',
        )
        self.readonly_fields = ()
        return super().add_view(request, extra_context=extra_context)

    def tariff(self, obj):
        return obj.user.tariff
