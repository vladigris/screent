from django.conf import settings
from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import DateTimeField

from web.fields import UniqueBooleanField
from web.services.helpers import get_guid


class VersionManager(Manager):
    def get_active_version(self):
        return self.model.objects.filter(is_active=True).first()


class Version(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    name = CharField(max_length=10, unique=True)
    is_active = UniqueBooleanField(default=False)
    created_at = DateTimeField(auto_now_add=True)

    objects = VersionManager()

    class Meta:
        app_label = 'core'
        verbose_name = 'Version'
        verbose_name_plural = 'Versions'
        ordering = ['name']
        db_table = 'versions'

    def __str__(self):
        return self.name

    @property
    def crypt_key(self):
        return settings.VERSIONS[self.name]['CryptKey']

    @property
    def checksum(self):
        return settings.VERSIONS[self.name]['Checksum']

    @property
    def folder(self):
        return settings.VERSIONS[self.name]['Folder']

    @property
    def worker_filename(self):
        return settings.VERSIONS[self.name]['WorkerFilename']

    @property
    def installer_filename(self):
        return 'installer.exe'

    @property
    def installer_key(self):
        return 'programs/{}/{}/{}'.format(self.name, self.folder,
                                          self.installer_filename)

    @property
    def worker_key(self):
        return 'programs/{}/{}/{}'.format(self.name, self.folder,
                                          self.worker_filename)

    @property
    def installer_link(self):
        return 'https://{}.nyc3.digitaloceanspaces.com/{}'\
            .format(settings.BUCKET_NAME, self.installer_key)

    @property
    def worker_link(self):
        return 'https://{}.nyc3.digitaloceanspaces.com/{}'\
            .format(settings.BUCKET_NAME, self.worker_key)


@admin.register(Version)
class VersionAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'folder',
        'worker',
        'installer',
        'is_active'
    )

    def folder(self, obj):
        return obj.folder

    def worker(self, obj):
        return obj.worker_filename

    def installer(self, obj):
        return obj.installer_filename
