from django.conf.urls import url
from .import views as v


urlpatterns = [
    url(
        r'^(?P<version>(v1))/install/step1$',
        v.InstallStep1.as_view(),
        name='install_step1'),

    url(
        r'^(?P<version>(v1))/install/step2$',
        v.InstallStep2.as_view(),
        name='install_step2'),

    url(
        r'^(?P<version>(v1))/install/set_serials$',
        v.InstallSetSerials.as_view(),
        name='install_set_serials'),

    url(
        r'^(?P<version>(v1))/install/mark$',
        v.InstallMark.as_view(),
        name='install_mark'),

    url(
        r'^(?P<version>(v1))/core/get_config/(?P<token>\w+)$',
        v.GetConfig.as_view(),
        name='get_config'),

    url(
        r'^(?P<version>(v1))/core/computers$',
        v.Computers.as_view(),
        name='computers'),

    url(
        r'^(?P<version>(v1))/core/computers/(?P<guid>\w+)$',
        v.ComputerDetail.as_view(),
        name='computer'),

    url(
        r'^(?P<version>(v1))/core/computers/(?P<guid>\w+)/screens$',
        v.Screens.as_view(),
        name='screens'),

    url(
        r'^(?P<version>(v1))/core/active_version$',
        v.ActiveVersion.as_view(),
        name='active_version'),
]
