from django.utils.translation import ugettext_lazy as _
from core.models import Computer
from core.models import User


class InstallMixin:
    def check_install_step1(self, data):
        """
        Returns xml error string or None
        """
        if data['email'] == '' or data['password'] == '':
            return Computer.objects.xml_error(_('Incorrect email or password'))

        user = User.objects.filter(email=data['email']).first()
        if user is None:
            return Computer.objects.xml_error(_('Incorrect email or password'))

        if not user.check_password(data['password']):
            return Computer.objects.xml_error(_('Incorrect email or password'))

        if not user.is_active:
            return Computer.objects.xml_error(_('User account is not enabled'))

        if not user.is_signup_completed:
            return Computer.objects.xml_error(_('The signup is not completed'))

        if user.computers_qs.count() == 0:
            return Computer.objects.xml_error(
                _('You do not have computers'))

        return None

    def check_install_step2(self, data):
        """
        Returns xml error string or None
        """
        computer = Computer.objects.filter(id=data['id']).first()
        if computer is None:
            return Computer.objects.xml_error(_('Incorrect computer'))

        user = User.objects.get(email=data['email'])
        if user.id != computer.user.id:
            return Computer.objects.xml_error(_('Incorrect computer'))

        return None
