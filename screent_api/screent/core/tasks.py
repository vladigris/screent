from celery.decorators import periodic_task
from celery.task.schedules import crontab
from core.services.processor import Processor


@periodic_task(run_every=crontab(minute='*'))
def run_processor():
    p = Processor()
    p.process_screenshots()
    p.process_keyboards()
    p.process_logs()
