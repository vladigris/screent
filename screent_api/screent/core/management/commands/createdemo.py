import datetime
import json
import random
from os.path import join

from django.apps import apps
from django.utils import timezone
from django.db import transaction
from django.conf import settings
from django.core.management.base import BaseCommand

from core.models import Computer
from core.models import User
from web.models import Lang


class Command(BaseCommand):
    """
    Creates demo user, demo computer, demo screenshots,
    demo keyboards.
    """
    def init(self):
        self.cur_dir = join(settings.BASE_DIR,
                            'screent/core/management/commands/')
        self.screens_data_path = join(self.cur_dir, 'data/screens.json')

        sentences_data_path = join(self.cur_dir, 'data/sentences.json')
        f = open(sentences_data_path)
        data = f.read()
        f.close()
        self.sentences = json.loads(data)
        self.sentences_iterator = iter(self.sentences)

        self.width = 1366
        self.height = 768

        # Set later after creation
        self.computer = None
        self.screens = []

    def spaces_path(self, d):
        return 'screens/{}/{}.jpg'.format(d['computer_guid'],
                                          d['screen_guid'])

    def get_start_hour(self):
        """
        Returns random hour from 10 to 17.
        """
        return random.randint(10, 17)

    def get_start_minute(self):
        """
        Returns random minute from 1 to 10.
        """
        return random.randint(1, 10)

    def get_start_second(self):
        """
        Returns random second from 3 to 7.
        """
        return random.randint(3, 7)

    def get_end_hour(self, start_hour):
        return start_hour + random.randint(1, 2)

    def get_text(self):
        try:
            return next(self.sentences_iterator)
        except StopIteration:
            self.sentences_iterator = iter(self.sentences)
        return next(self.sentences_iterator)

    def create_user(self):
        User.objects.filter(email=settings.DEMO_USER_EMAIL).delete()

        user = User.objects.create_user(
            settings.DEMO_USER_EMAIL, settings.DEMO_USER_PASSWORD)
        user.lang = Lang.objects.get(code='en')
        user.first_name = 'Demo'
        user.last_name = 'Demo'
        user.joined_at = timezone.now()
        user.save()

    def create_computer(self):
        user = User.objects.get(email=settings.DEMO_USER_EMAIL)
        c = Computer.objects.create_computer(user, 'DemoPC', 'testprog')
        c.is_installed = True
        c.guid = settings.DEMO_COMPUTER_GUID
        c.save()

        c.token = 'anythingnotused'
        c.interval = 20
        c.day_limit = 100
        c.save()

        self.computer = c

    def get_screens_iterator(self):
        """
        Returns iterator for screens from json file.
        """
        l = []
        with open(self.screens_data_path) as f:
            data = f.read()
            l = json.loads(data)
        return iter(l)

    def create_screens(self):
        """
        Creates screens for number of screens in Spaces.
        """
        Screen = apps.get_model('core', 'Screen')

        it = self.get_screens_iterator()
        date = datetime.datetime.today()

        while True:
            start_hour = self.get_start_hour()
            end_hour = self.get_end_hour(start_hour)
            s = '{}-{}-{} {}:{}:{}'.format(
                date.year,
                date.month,
                date.day,
                start_hour,
                self.get_start_minute(),
                self.get_start_second()
            )
            dt = datetime.datetime.strptime(s, '%Y-%m-%d %H:%M:%S')
            # create screen every 30 seconds until end_hour
            while True:
                try:
                    dic = next(it)
                except StopIteration:
                    Screen.objects.bulk_create(self.screens)
                    return

                dt = dt + datetime.timedelta(seconds=30)
                if dt.hour > end_hour:
                    break
                self.create_screen(dic, dt)

            date = date - datetime.timedelta(days=1)

    def create_screen(self, d, dt):
        Screen = apps.get_model('core', 'Screen')
        created_at = timezone.make_aware(dt)

        file_key = '{}/{}.jpg'.format(self.computer.guid,
                                      d['screen_guid'])

        screen = Screen(
            guid=d['screen_guid'],
            computer=self.computer,
            title=self.get_text(),
            program='Chrome',
            width=self.width,
            height=self.height,
            created_at=created_at,
            minute_index=created_at.strftime('%Y%m%d%H%M'),
            file_key=file_key)
        self.screens.append(screen)

    def create_keyboards(self):
        """
        Creates keyboards for the same time period as screens.
        """
        computer = self.computer

        Keyboard = apps.get_model('core', 'Keyboard')
        Screen = apps.get_model('core', 'Screen')
        min_dt = Screen.objects.filter(
            computer=computer).earliest('created_at').created_at
        max_dt = Screen.objects.filter(
            computer=computer).latest('created_at').created_at
        cur_dt = min_dt.replace(second=0, microsecond=0)

        keyboards = []
        while cur_dt < max_dt:
            cur_dt = cur_dt + datetime.timedelta(minutes=1)
            text = self.get_text()
            k = Keyboard(computer=computer, text=text,
                         created_at=cur_dt,
                         minute_index=cur_dt.strftime('%Y%m%d%H%M'))
            keyboards.append(k)
        Keyboard.objects.bulk_create(keyboards)

    @transaction.atomic
    def run(self):
        self.create_user()
        self.create_computer()
        self.create_screens()
        self.create_keyboards()

    def handle(self, *args, **options):
        self.init()
        self.run()
