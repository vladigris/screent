import json
import boto3
from os.path import basename
from os.path import join
from os.path import splitext

from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """
    Upload demo images from /home/vlad/screenshots/processed/
    to DigitalOcean Spaces.

    Previously screenshots should be prepared by
    python manage.py createdemoimages
    """
    def init(self):
        self.cur_dir = join(settings.BASE_DIR,
                            'screent/core/management/commands/')
        self.json_path = join(self.cur_dir, 'data/screens.json')
        self.images_path = '/home/vlad/screenshots/processed/'
        self.existing_screens = []

    def get_client(self):
        """
        Returns Spaces client for upload.
        """
        session = boto3.session.Session()
        client = session.client(
            's3',
            region_name='nyc3',
            endpoint_url='https://nyc3.digitaloceanspaces.com',
            aws_access_key_id=settings.SPACES_KEY,
            aws_secret_access_key=settings.SPACES_SECRET)
        return client

    def get_existing_screens(self):
        """
        Returns list of names of screnns that already were uploaded to bucket.
        """
        client = self.get_client()
        r = client.list_objects(
            Bucket='screent',
            Marker='screens/{}'.format(settings.DEMO_COMPUTER_GUID),
            MaxKeys=10000,
        )
        names = []
        l = [d['Key'] for d in r['Contents']]
        for path in l:
            if not path.endswith('.jpg'):
                continue
            name, _ = splitext(basename(path))
            names.append(name)
        return names

    def upload_file(self, d):
        if d['screen_guid'] in self.existing_screens:
            print('exists')
            return

        client = self.get_client()
        spaces_path = 'screens/{}/{}.jpg'.format(d['computer_guid'],
                                                 d['screen_guid'])

        local_path = join(self.images_path, d['screen_guid'] + '.jpg')
        client.upload_file(
            local_path, 'screent', spaces_path,
            ExtraArgs={'ContentType': 'image/jpeg', 'ACL': 'public-read'})

    def run(self):
        self.existing_screens = self.get_existing_screens()

        count = 0
        with open(self.json_path) as f:
            data = f.read()
            l = json.loads(data)
            for d in l:
                count += 1
                print(count)
                self.upload_file(d)

    def handle(self, *args, **options):
        self.init()
        self.run()
