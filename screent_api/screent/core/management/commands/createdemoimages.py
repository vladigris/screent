import json
import os
import shutil
import uuid
import tempfile
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from os.path import join

from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """
    /home/vlad/screenshots/blured contains sample blured screenshots

    This command set on screen text "Screent Demo", saves them to
    /home/vlad/screenshots/processed/
    and saves result to json file.
    """
    def init(self, num=5775):
        self.cur_dir = join(settings.BASE_DIR,
                            'screent/core/management/commands/')
        self.font_path = join(self.cur_dir, 'resources/arial.ttf')
        self.json_path = join(self.cur_dir, 'data/screens.json')
        self.source_path = '/home/vlad/screenshots/blured/'
        self.dest_path = '/home/vlad/screenshots/processed/'
        self.number_of_files = num
        os.makedirs(self.dest_path, mode=0o777, exist_ok=True)
        self.result = []

    def get_files(self):
        """
        Fills list until number_of_files
        """
        l = []

        while True:
            for file in os.listdir(self.source_path):
                path = join(self.source_path, file)
                l.append(path)

                if len(l) >= self.number_of_files:
                    return l

    def process_image_file(self, file):
        """
        Set text on blured image, upload image to spaces and adds
        result to self.result
        """
        im = Image.open(file)
        draw = ImageDraw.Draw(im)

        text = 'Screent Demo'

        font = ImageFont.truetype(self.font_path, 70)
        w, h = font.getsize(text)

        draw.text(
            ((im.width - w) / 2, (im.height - h) / 2),
            text,
            font=font,
            fill='#444')

        del draw

        f = tempfile.NamedTemporaryFile()
        im.save(f.name, 'JPEG')

        d = dict(
            computer_guid=settings.DEMO_COMPUTER_GUID,
            screen_guid=uuid.uuid4().hex)

        image_name = '{}.jpg'.format(d['screen_guid'])
        path = join(self.dest_path, image_name)
        shutil.copy(f.name, path)
        self.result.append(d)
        f.close()

    def run(self):
        """
        Create demo files and save them to dest_path on local drive.
        Total result saves to result json file.
        """
        files = self.get_files()
        count = 0
        for file in files:
            count += 1
            self.process_image_file(file)
            print(count)

        f = open(self.json_path, 'w')
        f.write(json.dumps(self.result))
        f.close()

    def handle(self, *args, **options):
        self.init()
        self.run()
