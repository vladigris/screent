from django_filters import FilterSet
from django_filters import DateTimeFilter
from django_filters import CharFilter

from core.models import Screen


class ScreenFilter(FilterSet):
    start_at = DateTimeFilter(name='created_at', lookup_expr='gte')
    end_at = DateTimeFilter(name='created_at', lookup_expr='lte')
    program = CharFilter(lookup_expr='icontains')
    title = CharFilter(lookup_expr='icontains')

    class Meta:
        model = Screen
        fields = (
            'start_at',
            'end_at',
            'program',
            'title'
        )
