
class PKCS7Encoder:
    def decode(self, bytestring, k=16):
        val = bytestring[-1]
        if val > k:
            raise ValueError('Input is not padded or padding is corrupt')
        l = len(bytestring) - val
        return bytestring[:l]

    def encode(self, bytestring, k=16):
        l = len(bytestring)
        val = k - (l % k)
        return bytestring + bytearray([val] * val)
