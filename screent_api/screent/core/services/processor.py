import datetime
import json
from django.utils import timezone
from django.apps import apps
from django.conf import settings


class Processor:
    """
    1) TCP server sends screenshot to Spaces and saves info to Redis.
    2) TCP server saves keyboard info and logs to Redis

    Processor fetch this data and saves to PostgreSQL
    """
    def __init__(self):
        self.rs = settings.REDIS_DB

    def process_screenshots(self):
        Computer = apps.get_model('core', 'Computer')
        Screen = apps.get_model('core', 'Screen')
        key = settings.REDIS_KEY_SCREEN_DATA

        while True:
            value = self.rs.lpop(key)
            if value is None:
                return

            try:
                d = json.loads(value.decode())
            except:
                continue

            computer = Computer.objects.filter(id=d['computerId']).first()
            if computer is None:
                continue

            dt = datetime.datetime.strptime(d['time'], '%Y-%m-%d %H:%M:%S')

            Screen.objects.create(
                computer_id=computer.id,
                guid=d['guid'],
                program=d['program'],
                title=d['title'],
                file_size=d['fileSize'],
                width=d['width'],
                height=d['height'],
                created_at=timezone.make_aware(dt))

    def process_keyboards(self):
        Computer = apps.get_model('core', 'Computer')
        Keyboard = apps.get_model('core', 'Keyboard')
        key = settings.REDIS_KEY_KEYBOARD_DATA

        while True:
            value = self.rs.lpop(key)
            if value is None:
                return

            try:
                d = json.loads(value.decode())
            except:
                continue

            computer = Computer.objects.filter(id=d['computerId']).first()
            if computer is None:
                continue

            dt = datetime.datetime.strptime(d['time'], '%Y-%m-%d %H:%M:%S')

            Keyboard.objects.create(
                computer_id=computer.id,
                text=d['text'],
                created_at=timezone.make_aware(dt))

    def process_logs(self):
        Computer = apps.get_model('core', 'Computer')
        Log = apps.get_model('msg', 'Log')
        key = settings.REDIS_KEY_LOG_DATA

        while True:
            value = self.rs.lpop(key)
            if value is None:
                return

            try:
                d = json.loads(value.decode())
            except:
                continue

            computer = Computer.objects.filter(guid=d['computerGuid']).first()
            if computer is None:
                continue

            dt = datetime.datetime.strptime(d['createdAt'],
                                            '%Y-%m-%d %H:%M:%S')

            Log.objects.create(
                computer_id=computer.id,
                level=d['level'].lower(),
                text=d['message'],
                service=Log.TCP,
                env=Log.PROD,
                created_at=timezone.make_aware(dt))
