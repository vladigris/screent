from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.pagination import PageNumberPagination

from core.serializers import InstallStep1Serializer
from core.serializers import InstallStep2Serializer
from core.serializers import InstallSetSerialsSerializer
from core.serializers import InstallMarkSerializer

from core.serializers import ComputerSerializer
from core.serializers import CreateComputerSerializer
from core.serializers import UpdateComputerSerializer
from core.serializers import KeyboardSerializer
from core.serializers import ScreenSerializer
from core.serializers import VersionSerializer

from core.permissions import ComputerPermissions

from core.filters import ScreenFilter

from core.models import Computer
from core.models import Screen
from core.models import Keyboard
from core.models import Version

from web.views import AuthMixin


class InstallStep1(APIView):
    """
    post:
        <h1>Install step 1</h1>
        <p>Returns XML whether error or not.</p>
        <p>Response code always 200</p>
    """
    def post(self, request, **kwargs):
        s = InstallStep1Serializer(data=request.data)
        s.is_valid()
        return HttpResponse(s.process())


class InstallStep2(APIView):
    """
    post:
        <h1>Install step 2</h1>
        <p>Returns XML whether error or not.</p>
        <p>Response code always 200</p>
    """
    def post(self, request, **kwargs):
        s = InstallStep2Serializer(data=request.data)
        s.is_valid()
        return HttpResponse(s.process())


class InstallSetSerials(APIView):
    """
    post:
        <h1>Install set serials</h1>
        <p>Response code always 200 and response text always OK</p>
    """
    def post(self, request, **kwargs):
        s = InstallSetSerialsSerializer(data=request.data)
        s.is_valid()
        return HttpResponse(s.process())


class InstallMark(APIView):
    """
    post:
        <h1>Mark computer as installed</h1>
    """
    def post(self, request, **kwargs):
        s = InstallMarkSerializer(data=request.data)
        s.is_valid()
        return HttpResponse(s.process())


class GetConfig(APIView):
    """
    get:
        <h1>Get encrypted config by token</h1>
        <p>Used for debugging.</p>
        <p>Response code 200</p>
    """
    def get(self, request, token, **kwargs):
        c = get_object_or_404(Computer, token=token)
        return HttpResponse(c.enc_config)


class ActiveVersion(AuthMixin, APIView):
    """
    get:
        <h1>Get active version</h1>
        <p>Response code 200</p>
    """
    def get(self, request, **kwargs):
        version = Version.objects.get_active_version()
        return Response(VersionSerializer(version).data)


class Computers(AuthMixin, ListCreateAPIView):
    """
    get:
        <h1>Get list of computers</h1>
        <p>Response code: 200</p>
    post:
        <h1>Create computer</h1>
        <p>Response code: 201</p>
    """
    def get_serializer_class(self):
        if self.request.method == 'POST':
            return CreateComputerSerializer
        return ComputerSerializer

    def get_queryset(self):
        user = self.request.user
        return user.computers_qs.all()

    def get_serializer_context(self):
        return {'user': self.request.user}


class ComputerDetail(AuthMixin, RetrieveUpdateDestroyAPIView):
    lookup_field = 'guid'

    def __init__(self):
        super().__init__()
        self.permission_classes += (ComputerPermissions,)

    def get_serializer_class(self):
        if self.request.method in ['PUT', 'PATCH']:
            return UpdateComputerSerializer
        return ComputerSerializer

    def get_queryset(self):
        return self.request.user.computers_qs

    def get_serializer_context(self):
        return {'user': self.request.user,
                'guid': self.kwargs.get('guid')}

    def perform_destroy(self, instance):
        instance.is_active = True
        instance.is_removed = True
        instance.save()


class Screens(AuthMixin, APIView):
    """
    get:
        <h1>User can get and filter screenshots</h1>
        <b>Query input params:</b><br />
        start_at (YYYY-MM-DD HH:MM:SS)<br />
        end_at (YYYY-MM-DD HH:MM:SS)<br />
        program<br />
        title<br />
        <p>Response object fields: count, screens, keyboards</p>
        <p>Response code: 200</p>
    """
    def get(self, request, guid, **kwargs):
        qs = Screen.objects.filter(computer__user=self.request.user,
                                   computer__guid=guid)

        f = ScreenFilter(request.query_params, queryset=qs)
        qs = f.qs

        paginator = PageNumberPagination()
        page = paginator.paginate_queryset(qs, request)

        s = ScreenSerializer(page, many=True)

        indexes = [x.minute_index for x in page]
        indexes = Keyboard.objects.get_indexes(indexes)
        kqs = Keyboard.objects.filter(
            computer__guid=guid, minute_index__in=indexes)

        d = {
            'count': qs.count(),
            'screens': s.data,
            'keyboards': KeyboardSerializer(kqs, many=True).data
        }
        return Response(d, status=200)
