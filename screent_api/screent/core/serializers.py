from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.shortcuts import get_object_or_404
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import Serializer
from rest_framework.serializers import CharField
from rest_framework.serializers import IntegerField
from rest_framework.serializers import ModelField
from django.core.exceptions import ValidationError

from core.models import User
from core.models import Computer
from core.models import Keyboard
from core.models import Screen
from core.models import Version

from core.mixins import InstallMixin


class InstallStep1Serializer(InstallMixin, Serializer):
    email = CharField(allow_null=True)
    password = CharField(allow_null=True)

    def process(self):
        d = self.data
        error = self.check_install_step1(d)
        if error:
            return error

        user = User.objects.get(email=d['email'])
        return Computer.objects.install_step1_xml(user)


class InstallStep2Serializer(InstallMixin, Serializer):
    email = CharField(allow_null=True)
    password = CharField(allow_null=True)
    id = IntegerField(allow_null=True)

    def process(self):
        """
        Check the same as in InstallStep1
        Plus additional.
        """
        d = self.data
        error = self.check_install_step1(d)
        if error:
            return error

        error = self.check_install_step2(d)
        if error:
            return error

        computer = Computer.objects.get(id=d['id'])
        return Computer.objects.install_step2_xml(computer)


class InstallSetSerialsSerializer(Serializer):
    token = CharField(allow_null=True)
    serial_mb = CharField(allow_null=True)
    serial_proc = CharField(allow_null=True)
    serial_hdd = CharField(allow_null=True)

    def process(self):
        """
        Returns OK in any case.
        """
        d = self.data

        if d['token'] == '':
            return 'OK'

        computer = Computer.objects.filter(token=d['token']).first()
        if computer is None:
            return 'OK'

        computer.serial_mb = d['serial_mb']
        computer.serial_hdd = d['serial_hdd']
        computer.serial_proc = d['serial_proc']
        computer.save()
        return 'OK'


class InstallMarkSerializer(Serializer):
    token = CharField(allow_null=True)

    def validate(self, data):
        token = data.get('token', None)
        if token is None:
            raise ValidationError('No token')

        data['computer'] = get_object_or_404(Computer, token=token)
        return data

    def process(self):
        d = self.validated_data

        c = d['computer']
        c.is_installed = True
        c.save()
        return 'OK'


class ComputerSerializer(ModelSerializer):
    version_id = ModelField(model_field=Computer._meta.get_field('version'))

    class Meta:
        model = Computer
        fields = (
            'id',
            'guid',
            'version_id',
            'name',
            'prog_name',
            'interval',
            'day_limit',
            'created_at',
            'is_pause',
            'is_active',
            'is_installed',
            'is_removed',
            'prog_dir'
        )


class CreateComputerSerializer(ModelSerializer):
    class Meta:
        model = Computer
        fields = (
            'name',
            'prog_name',
        )

    def to_representation(self, instance):
        s = ComputerSerializer(instance)
        return s.data

    def validate(self, data):
        """
        User can not create more than 1 computer if:
        1) User is not paid
        2) User's tariffs doesn't allow more than 1 computer
        """
        user = self.context.get('user')
        num = user.computers_qs.count()

        if num == 0:
            return data

        tariff = user.tariff
        dt = user.trial_ended_at
        if tariff is None and dt and dt > timezone.now():
            raise ValidationError(_('To add computer create subscription'))

        if tariff is None:
            raise ValidationError(_('You can not add more than 1 computer'))

        if tariff.max_computers <= num:
            message = _(
                'Maximum number of computers on your tariff is {}'.format(
                    tariff.max_computers))
            raise ValidationError(message)
        return data

    def create(self, validated_data):
        d = validated_data
        user = self.context.get('user')
        return Computer.objects.create_computer(
            user, d['name'], d['prog_name'])


class UpdateComputerSerializer(ModelSerializer):
    class Meta:
        model = Computer
        fields = (
            'name',
            'interval',
            'is_pause',
        )

    def to_representation(self, instance):
        s = ComputerSerializer(instance)
        return s.data

    def validate_interval(self, value):
        """
        Validate minimum interval.
        Validate maximum interval.
        """
        computer = get_object_or_404(Computer,
                                     guid=self.context.get('guid'))

        min_interval = computer.min_interval
        if value < min_interval:
            raise ValidationError(
                _('The minimum interval is {}'.format(min_interval)))

        max_interval = settings.MAX_INTERVAL
        if value > max_interval:
            raise ValidationError(
                _('The maximum interval is {}'.format(max_interval)))
        return value


class ScreenSerializer(ModelSerializer):
    computer_id = ModelField(model_field=Screen._meta.get_field('computer'))

    class Meta:
        model = Screen
        fields = (
            'id',
            'guid',
            'computer_id',
            'program',
            'src',
            'title',
            'width',
            'height',
            'created_at',
            'file_size',
            'minute_index',
        )


class KeyboardSerializer(ModelSerializer):
    computer_id = ModelField(model_field=Screen._meta.get_field('computer'))

    class Meta:
        model = Keyboard
        fields = (
            'id',
            'guid',
            'text',
            'created_at',
            'minute_index',
            'computer_id'
        )


class UserProfileSerializer(ModelSerializer):
    lang_id = ModelField(model_field=User._meta.get_field('lang'))

    class Meta:
        model = User
        fields = (
            'id',
            'guid',
            'lang_id',
            'email',
            'first_name',
            'last_name',
            'joined_at',
            'is_signup_completed',
            'num_computers',
            'num_installed_computers',
            'num_screenshots',
        )


class UpdateUserProfileSerializer(UserProfileSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
        )

    def to_representation(self, instance):
        s = UserProfileSerializer(instance)
        return s.data


class UserProfileChangePasswordSerializer(Serializer):
    old_password = CharField()
    new_password = CharField(min_length=8, max_length=30)

    def validate_old_password(self, value):
        user = self.context.get('user')
        if not user.check_password(value):
            raise ValidationError(
                _('Current password is not correct.'))
        return value

    def save(self):
        d = self.validated_data
        u = self.context.get('user')
        u.set_password(d['new_password'])
        u.save()


class VersionSerializer(ModelSerializer):
    class Meta:
        model = Version
        fields = (
            'name',
            'installer_link'
        )
