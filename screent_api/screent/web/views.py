import logging
from django.utils import timezone
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from core.serializers import UserProfileSerializer
from core.serializers import UpdateUserProfileSerializer
from core.serializers import UserProfileChangePasswordSerializer

from web.serializers import LoginSerializer
from web.serializers import ResetPasswordSerializer
from web.serializers import ResetPasswordCheckSerializer
from web.serializers import ResetPasswordCompleteSerializer

from web.serializers import SignupSerializer
from web.serializers import SignupStep2Serializer
from web.serializers import SignupStep2CheckSerializer

from core.models import User
from web.tasks import react_pull

logger = logging.getLogger('app')


@csrf_exempt
def bitbucket_webhook(request):
    react_pull.delay()
    return HttpResponse('OK')


def error404(request):
    return render(request, 'web/html/404.html', status=404)


def error500(request):
    return render(request, 'web/html/500.html', status=500)


def r400(error_message):
    return Response({
        'non_field_errors': [error_message]}, status=400)


class AuthMixin:
    permission_classes = (IsAuthenticated,)


def home(request):
    return HttpResponse('API')


class Signup(APIView):
    """
    post:
        <h1>Signup endpoint for user</h1>
        <p>After signup user receives email with verification link</p>
        <b>Input params:</b> email<br />
        <b>Response code:</b> 204
    """
    def post(self, request, **kwargs):
        s = SignupSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        s.save()
        return Response(status=204)


class SignupStep2Check(APIView):
    """
    post:
        <h1>Signup Step2 Check endpoint for user.</h1>
        <p>After user has received email with verification link,
        it visits frontend's page. Frontend checks user's guid
        and signup token (in link params) for params are correct.
        Verification url on frontend for implementation:
        example.com/signup/{user_guid}/{signup_token}
        If everything is OK, server responds with 204 code.
        </p>
        <b>Input params:</b> guid, signup_token<br />
        <b>Response code:</b> 204
    """
    def post(self, request, **kwargs):
        s = SignupStep2CheckSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        return Response(status=204)


class SignupStep2(APIView):
    """
    post:
        <h1>Signup Step2 endpoint for user.</h1>
        <p>After frontend verified user's guid and signup_token, frontend
        collect's password from user. Then send all params
        to this endpoint. If everything is good, user get status
        is_signup_completed and server responds with Auth Token.
        </p>
        <b>Input params:</b> guid, signup_token, password<br />
        <b>Response fields:</b> token<br />
        <b>Response code:</b> 201
    """
    def post(self, request, **kwargs):
        s = SignupStep2Serializer(data=request.data)
        s.is_valid(raise_exception=True)
        data = s.save()
        return Response(data, status=201)


class Login(APIView):
    """
    post:
        <h1>Login endpoint</h1>
        <p>Server responds with Auth Token</p>
        <b>Input params:</b> email, password<br />
        <b>Response fields:</b> token<br />
        <b>Response code:</b> 200
    """
    def post(self, request, **kwargs):
        s = LoginSerializer(data=request.data)
        s.is_valid(raise_exception=True)

        d = s.data
        user = User.objects.get(email=d['email'])
        data = {'token': user.get_token().key}

        user.last_access_at = timezone.now()
        user.save()
        return Response(data, status=200)


class ResetPassword(APIView):
    """
    post:
        <h1>Reset password endpoint</h1>
        <p>Admin or app user can reset own password</p>
        <p>User inputs email and if it is correct,
        server send email to user with reset password link</p>
        <b>Input params:</b> email, subdomain<br />
        <b>Response code:</b> 204
    """
    def post(self, request, **kwargs):
        s = ResetPasswordSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        s.save()
        return Response(status=204)


class ResetPasswordCheck(APIView):
    """
    post:
        <h1>Reset password check endpoint</h1>
        <p>After user initiated password reset, user get
        reset password link to email</p>
        <p>The link to implementation on frontend:</p>
        <p>sub.example.com/reset_password/{user_guid}/{reset_token}</p>
        <p>Frontend checks that user's guid and reset token correct</p>
        <p>If everything is correct, server responds with 204 code</p>
        <b>Input params:</b> user_guid, reset_token<br />
        <b>Response code:</b> 204
    """
    def post(self, request, **kwargs):
        s = ResetPasswordCheckSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        return Response(status=204)


class ResetPasswordComplete(APIView):
    """
    post:
        <h1>Reset password complete endpoint</h1>
        <p>After frontend cheked user's guid and reset token
        it collects from user new password and send all info to server</p>
        <p>If everything is good, server responds with Auth token</p>
        <b>Input params:</b> user_guid, reset_token, password<br />
        <b>Response fields:</b> token<br />
        <b>Response code:</b> 201
    """
    def post(self, request, **kwargs):
        s = ResetPasswordCompleteSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        data = s.save()
        return Response(data, status=201)


class UserProfile(AuthMixin, APIView):
    """
    get:
        <h1>User can get own profile</h1>
        <b>Response:</b> User object<br />
        <b>Response code:</b> 200<br />
    patch:
        <h1>User can update own profile</h1>
        <b>Input params:</b><br />
        first_name<br />
        last_name<br />
        <b>Response:</b> User object<br />
        <b>Response code:</b> 200<br />
    """
    def get(self, request, **kwargs):
        s = UserProfileSerializer(request.user)
        return Response(s.data)

    def patch(self, request, **kwargs):
        s = UpdateUserProfileSerializer(request.user,
                                        data=request.data, partial=True)
        s.is_valid(raise_exception=True)
        s.save()
        return Response(s.data)


class UserProfileChangePassword(AuthMixin, APIView):
    """
    post:
        <h1>User can change own password</h1>
        <b>Input params:</b> old_password, new_password<br />
        <b>Response code:</b> 204<br />
    """
    def post(self, request, **kwargs):
        s = UserProfileChangePasswordSerializer(data=request.data,
                                                context={'user': request.user})
        s.is_valid(raise_exception=True)
        s.save()
        return Response(status=204)


class UpdateLastAccess(AuthMixin, APIView):
    """
    post:
        <h1>Endpoint to update user's last_access_at</h1>
        <p>Frontend call this endpoint once per "X" minutes</p>
        <b>Input params: </b> No<br />
        <b>Response code:</b> 204<br />
    """
    def post(self, request, **kwargs):
        u = request.user
        u.last_access_at = timezone.now()
        u.save()
        return Response(status=204)
