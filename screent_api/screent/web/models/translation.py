from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import SlugField
from django.db.models import TextField
from django.db.models import DateTimeField

from web.services.helpers import get_guid


class TranslationManager(Manager):
    pass


class Translation(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    code = SlugField(
        max_length=4, unique=True)
    name = CharField(
        max_length=255, blank=True, default='')
    value_en = TextField(
        verbose_name='value en', blank=True, default='')
    created_at = DateTimeField(auto_now_add=True)

    objects = TranslationManager()

    class Meta:
        app_label = 'web'
        ordering = ['code']
        db_table = 'translations'

    def __str__(self):
        return self.name


@admin.register(Translation)
class TranslationAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'value_en')
    search_fields = ('name', 'code', 'value_en')
    list_display_links = ('code',)
    list_editable = ('value_en', 'name')
