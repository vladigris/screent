from django.contrib import admin
from django.db.models import Model
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import IntegerField
from django.db.models import DateTimeField

from web.services.helpers import get_guid


class Faq(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    question = CharField(max_length=255)
    answer = TextField(blank=True, default='')
    sort = IntegerField(default=0)
    created_at = DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'web'
        verbose_name = 'faq'
        verbose_name_plural = 'faq'
        ordering = ['sort']
        db_table = 'faqs'

    def __str__(self):
        return self.question


@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    list_display = ('question', 'answer', 'sort')
    list_editable = ('sort',)
