from django.contrib import admin
from django.db.models import Model
from django.db.models import CharField
from django.db.models import DateTimeField

from web.services.helpers import get_guid


class Setting(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    code = CharField(max_length=50)
    value = CharField(max_length=255, default='', blank=True)
    info = CharField(max_length=255, default='', blank=True)
    created_at = DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'web'
        db_table = 'settings'

    def __str__(self):
        return self.code


@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = ('code', 'value', 'info')
    list_editable = ('value',)
    list_display_links = None
    actions = None
