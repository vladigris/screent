from django.db.models import Manager
from django.db.models import Model
from django.db.models import QuerySet
from django.db.models import BooleanField


class ActiveQuerySet(QuerySet):
    def delete(self):
        self.update(is_active=False)


class ActiveManager(Manager):
    def active(self):
        return self.model.objects.filter(is_active=True)

    def get_queryset(self):
        return ActiveQuerySet(self.model, using=self._db)


class ActiveModel(Model):
    """
    is_active state instead of deleting.
    """
    is_active = BooleanField(default=True, editable=False)

    class Meta:
        abstract = True

    def delete(self):
        self.is_active = False
        self.save()

    objects = ActiveManager()
