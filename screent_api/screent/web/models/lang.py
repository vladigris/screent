from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class LangManager(Manager):
    pass


class Lang(Model):
    code = CharField(max_length=10)
    name = CharField(max_length=30)
    sort = IntegerField(default=0)

    objects = LangManager()

    class Meta:
        app_label = 'web'
        db_table = 'langs'
        verbose_name = 'lang'
        verbose_name_plural = 'langs'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(Lang)
class LangAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')
