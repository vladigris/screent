from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import IntegerField
from django.db.models import DateTimeField
from django.db.models import ForeignKey

from web.services.helpers import get_guid


class FeaturesManager(Manager):
    pass


class Features(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    lang = ForeignKey('Lang', null=True, default=None, blank=True)
    title = CharField(max_length=255, default='', blank=True)
    text = TextField(default='', blank=True)
    icon = CharField(max_length=50, default='', blank=True)
    sort = IntegerField(default=0)
    created_at = DateTimeField(auto_now_add=True)

    objects = FeaturesManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'features'
        verbose_name_plural = 'features'
        ordering = ['sort']
        db_table = 'features'

    def __str__(self):
        return self.icon


@admin.register(Features)
class FeaturesAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'icon', 'lang', 'text', 'sort')
    list_filter = ('lang',)
    list_editable = ('sort', 'title', 'icon', 'text')
