import os
from django.contrib import admin
from django.conf import settings
from django.template.loader import get_template
from django.db.models import Model
from django.db.models import CharField
from django.db.models import TextField


class Email(Model):
    RESET_PASSWORD = 'reset_password'
    SIGNUP = 'signup'

    CODE_CHOICES = (
        (RESET_PASSWORD, 'reset password'),
        (SIGNUP, 'signup')
    )

    code = CharField(max_length=50, choices=CODE_CHOICES)
    name = CharField(max_length=255)
    subject = CharField(max_length=255)
    txt = TextField(default='', blank=True)
    html = TextField(default='', blank=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'web'
        verbose_name = 'email'
        verbose_name_plural = 'emails'
        ordering = ['code']
        db_table = 'emails'

    def save(self, *args, **kwargs):
        if not self.pk:
            if not self.subject:
                self.subject = self.name
        super().save(*args, **kwargs)

    @property
    def folder(self):
        return os.path.join(
            settings.BASE_DIR, 'screent/screent/templates/web/emails/')

    def get_txt_template(self):
        """
        Returns Django Template instance.
        The name of file should be equal to code.
        """
        file_path = '{}{}.txt'.format(self.folder, self.code)
        return get_template(file_path)

    def get_html_template(self):
        """
        Returns Django Template instance.
        The name of file should be equal to code.
        """
        file_path = '{}{}.html'.format(self.folder, self.code)
        return get_template(file_path)


@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    list_display = (
        'code',
        'name',
        'subject',
        'txt',
        'html'
    )
    list_editable = (
        'subject',
    )
