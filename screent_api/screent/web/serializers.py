from django.contrib import auth
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import Serializer
from rest_framework.serializers import ValidationError
from rest_framework.serializers import CharField

from core.models import User

from web.mixins import GuidMixin
from web.mixins import SignupMixin
from web.mixins import ResetPasswordMixin

from web.services.helpers import get_guid


class LoginSerializer(ModelSerializer):
    email = CharField()
    password = CharField()

    class Meta:
        model = User
        fields = (
            'email',
            'password',
        )

    def validate(self, data):
        user = auth.authenticate(email=data['email'],
                                 password=data['password'])
        if user is None:
            raise ValidationError(_('Email or password is not correct.'))

        if user.joined_at is None:
            raise ValidationError(_('Registration is not completed.'))

        return data


class SignupSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = (
            'email',
        )

    def validate_email(self, value):
        if User.objects.filter(email=value.lower()).exists():
            raise ValidationError(_('Email is taken'))
        return value

    def save(self):
        data = self.validated_data
        user = User.objects.create_user(data['email'], get_guid())
        user.save()
        user.signup()


class SignupStep2CheckSerializer(GuidMixin, SignupMixin, Serializer):
    guid = CharField()
    signup_token = CharField()


class SignupStep2Serializer(GuidMixin, SignupMixin, Serializer):
    guid = CharField()
    signup_token = CharField()
    password = CharField(min_length=8)

    def save(self):
        d = self.validated_data
        user = User.objects.get(guid=d['guid'])

        user.joined_at = timezone.now()
        self.signup_token = ''
        user.set_password(d['password'])
        user.save()
        return {'token': user.get_token().key}


class ResetPasswordSerializer(Serializer):
    email = CharField()

    def validate(self, data):
        user = User.objects.filter(email=data['email']).first()
        if user is None:
            raise ValidationError(_('Email does not exists'))

        data['user'] = user
        return data

    def save(self):
        d = self.validated_data
        d['user'].reset_password()


class ResetPasswordCheckSerializer(ResetPasswordMixin, Serializer):
    user_guid = CharField()
    reset_token = CharField()


class ResetPasswordCompleteSerializer(ResetPasswordMixin, Serializer):
    user_guid = CharField()
    reset_token = CharField()
    password = CharField(min_length=8)

    def save(self):
        d = self.validated_data
        user = d['user']
        user.reset_password_complete(d['password'])
        return {'token': user.get_token().key}
