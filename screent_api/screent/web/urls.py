from django.conf.urls import url
from .import views as v

handler404 = v.error404
handler500 = v.error500


urlpatterns = [
    url(r'^bb_webhook$', v.bitbucket_webhook, name='bitbucket_webhook'),

    url(
        r'^(?P<version>(v1))/auth/signup$',
        v.Signup.as_view(),
        name='signup'),

    url(
        r'^(?P<version>(v1))/auth/signup/step2_check$',
        v.SignupStep2Check.as_view(),
        name='signup_step2_check'),

    url(
        r'^(?P<version>(v1))/auth/signup/step2$',
        v.SignupStep2.as_view(),
        name='signup_step2'),

    url(
        r'^(?P<version>(v1))/auth/login$',
        v.Login.as_view(),
        name='login'),

    url(
        r'^(?P<version>(v1))/auth/reset_password$',
        v.ResetPassword.as_view(),
        name='reset_password'),

    url(
        r'^(?P<version>(v1))/auth/reset_password_check$',
        v.ResetPasswordCheck.as_view(),
        name='reset_password_check'),

    url(
        r'^(?P<version>(v1))/auth/reset_password_complete$',
        v.ResetPasswordComplete.as_view(),
        name='reset_password_complete'),

    url(
        r'^(?P<version>(v1))/profile$',
        v.UserProfile.as_view(),
        name='user_profile'),

    url(
        r'^(?P<version>(v1))/profile/change_password$',
        v.UserProfileChangePassword.as_view(),
        name='user_profile_change_password'),

    url(
        r'^(?P<version>(v1))/profile/update_last_access$',
        v.UpdateLastAccess.as_view(),
        name='update_last_access'),

    url(r'^$', v.home, name='home'),
]
