from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings
from faker import Faker
from core.models import User
from msg.models import Message


class Command(BaseCommand):
    """
    Create fake messages for test user.
    """
    def create_message(self, user):
        fake = Faker()
        dic = dict(
            user=user,
            subject=fake.sentence(),
            text=fake.paragraph())
        msg = Message.objects.create_message(**dic)

        # create items
        for i in range(1, 20):
            dic = dict(
                message=msg,
                text=fake.paragraph(),
                is_client=fake.boolean())
            Message.objects.create_item(**dic)

    @transaction.atomic
    def handle(self, *args, **options):
        user = User.objects.get(email=settings.TEST_USER_EMAIL)
        user.messages.all().delete()

        for i in range(100):
            self.create_message(user)
