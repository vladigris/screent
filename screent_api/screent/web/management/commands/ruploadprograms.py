import boto3
from os.path import join
from os.path import dirname
from os.path import abspath
from django.core.management.base import BaseCommand
from django.conf import settings

from core.models import Version


class Command(BaseCommand):
    help = 'Upload programs to spaces'

    def get_client(self):
        session = boto3.session.Session()
        client = session.client(
            's3',
            region_name='nyc3',
            endpoint_url='https://nyc3.digitaloceanspaces.com',
            aws_access_key_id=settings.SPACES_KEY,
            aws_secret_access_key=settings.SPACES_SECRET)
        return client

    def upload_installer(self):
        version = Version.objects.get_active_version()

        dir = dirname(dirname(abspath(settings.BASE_DIR)))
        file_path = join(dir,
                         'screent_win/installer/bin/Release/installer.exe')

        self.upload(file_path, version.installer_key)

    def upload_worker(self):
        version = Version.objects.get_active_version()

        dir = dirname(dirname(abspath(settings.BASE_DIR)))
        file_path = join(dir,
                         'screent_win/Scr/bin/Release/scr.exe')
        self.upload(file_path, version.worker_key)

    def upload(self, file_path, key):
        client = self.get_client()
        client.upload_file(
            file_path,
            settings.BUCKET_NAME,
            key,
            ExtraArgs={
                'ContentType': 'application/octet-stream',
                'ACL': 'public-read'})

    def handle(self, *args, **options):
        self.upload_installer()
        self.upload_worker()

        version = Version.objects.get_active_version()
        print('Version {} programs have been uploaded!'.format(version.name))
