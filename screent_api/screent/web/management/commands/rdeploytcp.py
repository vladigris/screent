from os.path import dirname
from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import run
from fabric.api import sudo
from fabric.api import put
from fabric.api import env


class Command(BaseCommand):
    help = 'Deploy TCP to remote server'

    def init(self):
        env.host_string = 'vlad@{}'.format(settings.SERVER_APP_IP)

    def deploy(self):
        run('mkdir -p /home/vlad/screent_tcp')

        dir = settings.BASE_DIR + '/'
        local_path = dirname(dirname(dir)) + '/' + 'screent_tcp/bin/screent'
        remote_path = '/home/vlad/screent_tcp/screent'
        sudo('systemctl stop tcpserver')
        put(local_path, remote_path, mode='0755')
        sudo('systemctl start tcpserver')

    def handle(self, *args, **options):
        self.init()
        self.deploy()
