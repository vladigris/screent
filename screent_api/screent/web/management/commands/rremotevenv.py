from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import run
from fabric.api import env
from fabric.api import cd


class Command(BaseCommand):
    help = 'Install virtual environment on remote server'

    def init(self):
        env.host_string = 'vlad@{}'.format(settings.SERVER_APP_IP)

    def handle(self, *args, **options):
        self.init()

        with cd('/home/vlad/screent_api/screent/screent/requirements'):
            cmd = '. /home/vlad/.venvs/screent/bin/activate'
            cmd += ' && pip install -r production.txt'
            run(cmd)
