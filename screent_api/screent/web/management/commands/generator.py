import datetime
import json
from faker import Faker
from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.conf import settings
from django.utils import timezone
from django.db import transaction

from core.models import Computer
from core.models import Keyboard
from core.models import Screen
from core.models import Version
from core.models import User

from msg.models import Notification

from payment.models import Platform
from payment.models import Tariff

from web.models import Lang


class Command(BaseCommand):
    def create_langs(self):
        Lang.objects.create(code='en', name='English')
        print('Langs have been created')

    def create_platforms(self):
        Platform.objects.create('Stripe', 'stripe.com')
        print('Platforms have been created')

    def create_tariffs(self):
        lang = Lang.objects.get(code='en')

        basic = {
            'lang': lang,
            'name': Tariff.SMALL,
            'price': 10,
            'min_interval': 20,
            'max_computers': 1,
            'day_limit': 500,
            'storage': 5000
        }

        small = {
            'lang': lang,
            'name': Tariff.MEDIUM,
            'price': 20,
            'min_interval': 20,
            'max_computers': 2,
            'day_limit': 0,
            'storage': 20000,
            'is_featured': True,
        }

        medium = {
            'lang': lang,
            'name': Tariff.LARGE,
            'price': 30,
            'min_interval': 20,
            'max_computers': 3,
            'day_limit': 0,
            'storage': 30000
        }

        large = {
            'lang': lang,
            'name': Tariff.EXTRA_LARGE,
            'price': 40,
            'min_interval': 20,
            'max_computers': 4,
            'day_limit': 0,
            'storage': 40000
        }

        Tariff.objects.create(**basic)
        Tariff.objects.create(**small)
        Tariff.objects.create(**medium)
        Tariff.objects.create(**large)
        print('Tariffs have been created')

    def create_version(self):
        name = '1.0'
        if Version.objects.filter(name=name).exists():
            raise CommandError('First version exists.')

        v = Version(name=name)
        v.save()
        print('First version has been created')

    def create_users(self):
        user = User.objects.create_user(
            settings.TEST_USER_EMAIL, settings.TEST_USER_PASSWORD)
        user.lang = Lang.objects.get(code='en')
        user.first_name = 'V'
        user.last_name = 'G'
        user.joined_at = timezone.now()
        user.save()

        user = User.objects.create_user(
            settings.TEST_USER2_EMAIL, settings.TEST_USER2_PASSWORD)
        user.lang = Lang.objects.get(code='en')
        user.first_name = 'A'
        user.last_name = 'G'
        user.joined_at = timezone.now()
        user.save()
        print('Users have been created')

    def create_computers(self):
        user = User.objects.get(email=settings.TEST_USER_EMAIL)
        c = Computer.objects.create_computer(user, 'VComputer', 'testprog')
        c.is_installed = True
        c.save()

        c.token = 'testtoken'
        c.interval = 20
        c.day_limit = 100
        c.save()

        user = User.objects.get(email=settings.TEST_USER2_EMAIL)
        c = Computer.objects.create_computer(user, 'AComputer', 'testprog')
        c.guid = settings.TEST_COMPUTER_GUID
        c.is_installed = True
        c.save()

        c.token = 'testtoken2'
        c.interval = 20
        c.day_limit = 100
        c.save()

        print('Computers have been created')

    @transaction.atomic
    def fill_test_computer_screenshots(self):
        c = Computer.objects.get(guid=settings.TEST_COMPUTER_GUID)

        path = '{}/screent/web/management/commands/data/screens.json'\
            .format(settings.BASE_DIR)
        l = json.loads(open(path).read())
        screens = []
        for d in l:
            d['file_key'] = d.pop('spaces_path')
            d.pop('file_path')
            d['computer_id'] = c.id
            d['created_at'] = datetime.datetime.strptime(d['created_at'],
                                                         '%Y-%m-%d %H:%M:%S')
            d['created_at'] = timezone.make_aware(d['created_at'])
            s = Screen(**d)
            screens.append(s)

        Screen.objects.bulk_create(screens)
        print('Screenshots have been filled')

    @transaction.atomic
    def fill_test_computer_keyboards(self):
        c = Computer.objects.get(guid=settings.TEST_COMPUTER_GUID)

        path = '{}/screent/web/management/commands/data/keyboards.json'\
            .format(settings.BASE_DIR)
        l = json.loads(open(path).read())
        keyboards = []
        for d in l:
            d['computer_id'] = c.id
            d['created_at'] = datetime.datetime.strptime(d['created_at'],
                                                         '%Y-%m-%d %H:%M:%S')
            d['created_at'] = timezone.make_aware(d['created_at'])
            k = Keyboard(**d)
            keyboards.append(k)

        Keyboard.objects.bulk_create(keyboards)
        print('Keyboards have been filled')

    def create_notifications(self):
        user = User.objects.get(email=settings.TEST_USER2_EMAIL)
        for _ in range(10):
            Notification.objects.create(user=user, text=Faker().sentence())

    def handle(self, *args, **options):
        self.create_langs()
        self.create_platforms()
        self.create_tariffs()
        self.create_version()
        self.create_users()
        self.create_computers()
        self.fill_test_computer_screenshots()
        self.fill_test_computer_keyboards()
        self.create_notifications()
