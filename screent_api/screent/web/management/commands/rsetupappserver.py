import io
from os.path import dirname
from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import sudo
from fabric.api import run
from fabric.api import env
from fabric.api import put
from fabric.api import cd
from fabric.contrib.project import rsync_project
from web.management.configs.data import config_nginx_backend
from web.management.configs.data import config_nginx_react
from web.management.configs.data import config_systemd_backend
from web.management.configs.data import config_systemd_tcp


class Command(BaseCommand):
    help = 'Install remote App server'

    def init(self):
        env.host_string = 'root@{}'.format(settings.SERVER_APP_IP)

    def setup_initial(self):
        sudo('apt update')
        cmd = (
            'apt install -y build-essential libevent-dev libjpeg-dev nano '
            'htop curl libcurl4-gnutls-dev libgnutls28-dev libghc-gnutls-dev '
            'libxml2-dev libxslt1-dev libpq-dev '
            'git-core debconf-utils'
        )
        sudo(cmd)
        sudo('apt install -y python3-pip')
        sudo('pip3 install virtualenv')

    def setup_nginx(self):
        sudo('apt install -y nginx')
        sudo('systemctl enable nginx')
        sudo('systemctl start nginx')
        sudo('mkdir -p /var/log/nginx/sites/access')
        sudo('mkdir -p /var/log/nginx/sites/error')
        sudo('chown -R www-data:www-data /var/log/nginx/sites')

    def setup_react_project(self):
        run('mkdir -p /home/vlad/screent_react')

        dir = settings.BASE_DIR + '/'
        local_dir = dirname(dirname(dirname(dir)))\
            + '/' + 'screent_react/build/'
        remote_dir = '/home/vlad/screent_react/build/'
        rsync_project(local_dir=local_dir, remote_dir=remote_dir,
                      delete=True,
                      exclude=['.idea', '.git'])

        remote_path = '/etc/nginx/conf.d/screent_react.conf'
        f = io.StringIO()

        content = config_nginx_react \
            % {'server_name': settings.FRONTEND_DOMAIN,
               'folder': 'screent_react'}

        f.write(content)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()
        sudo('systemctl reload nginx')

    def setup_react_stage_project(self):
        run('mkdir -p /home/vlad/screent_react_stage')
        run('chown -R vlad:www-data /home/vlad/screent_react_stage')

        remote_path = '/etc/nginx/conf.d/screent_react_stage.conf'
        f = io.StringIO()

        content = config_nginx_react \
            % {'server_name': 'app-stage.screent.co',
               'folder': 'screent_react_stage'}

        f.write(content)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()
        sudo('systemctl reload nginx')

    def setup_backend_project(self):
        run('mkdir -p /home/vlad/.venvs')
        try:
            run('virtualenv /home/vlad/.venvs/screent --python=/usr/bin/python3')
        except:
            pass
        run('mkdir -p /home/vlad/screent_api')

        local_dir = settings.BASE_DIR + '/'
        remote_dir = '/home/vlad/screent_api/'
        rsync_project(local_dir=local_dir, remote_dir=remote_dir,
                      delete=True,
                      exclude=['__pycache__', '.cache', 'db.dump', '.git'])

        with cd('/home/vlad/screent_api/screent/screent/requirements'):
            cmd = '. /home/vlad/.venvs/screent/bin/activate'
            cmd += ' && pip install -r production.txt'
            run(cmd)

        # Systemd Django
        remote_path = '/etc/systemd/system/backend.service'
        f = io.StringIO()
        f.write(config_systemd_backend)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()

        sudo('systemctl daemon-reload')
        sudo('systemctl enable backend')
        sudo('systemctl start backend')

        # Nginx
        remote_path = '/etc/nginx/conf.d/screent_api.conf'
        f = io.StringIO()

        content = config_nginx_backend \
            % {'server_name': settings.BACKEND_DOMAIN}

        f.write(content)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()

        sudo('systemctl reload nginx')

    def setup_tcp_server(self):
        run('mkdir -p /home/vlad/screent_tcp')

        dir = settings.BASE_DIR + '/'
        local_path = dirname(dirname(dir)) + '/' + 'screent_tcp/bin/screent'
        remote_path = '/home/vlad/screent_tcp/screent'
        put(local_path, remote_path, mode='0755')

        # Systemd
        remote_path = '/etc/systemd/system/tcpserver.service'
        f = io.StringIO()
        f.write(config_systemd_tcp)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()

        sudo('systemctl daemon-reload')
        sudo('systemctl enable tcpserver')
        sudo('systemctl start tcpserver')

    def handle(self, *args, **options):
        self.init()
        # self.setup_initial()
        # self.setup_nginx()
        self.setup_react_project()
        self.setup_react_stage_project()
        # self.setup_backend_project()
        # self.setup_tcp_server()
        print('Server setuped successfully.')
