import io
from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import sudo
from fabric.api import env
from fabric.api import put

from web.management.configs.data import config_pg_hba
from web.management.helpers import get_remote_pg_root_password


class Command(BaseCommand):
    help = 'Install remote Storage server'

    def init(self):
        env.host_string = 'root@{}'.format(settings.SERVER_STORAGE_IP)

    def setup_initial(self):
        sudo('apt update')
        cmd = (
            'apt install -y build-essential libevent-dev libjpeg-dev nano '
            'htop curl libcurl4-gnutls-dev libgnutls28-dev libghc-gnutls-dev '
            'libxml2-dev libxslt1-dev libpq-dev '
            'git-core debconf-utils'
        )
        sudo(cmd)

    def setup_redis(self):
        sudo('apt install -y redis-server')
        sudo('systemctl enable redis-server')
        sudo('systemctl start redis-server')

        cmd = 'echo "bind 0.0.0.0" >> /etc/redis/redis.conf'
        sudo(cmd)
        sudo('systemctl restart redis-server')

    def setup_postgresql(self):
        sudo('apt install -y postgresql-{}'.format(
            settings.POSTGRESQL_VERSION))
        sudo('apt install -y pgadmin3')
        sudo('apt install -y postgresql-contrib')
        sudo('systemctl enable postgresql')
        sudo('systemctl start postgresql')

        remote_path = '/etc/postgresql/{}/main/pg_hba.conf'.format(
            settings.POSTGRESQL_VERSION)

        f = io.StringIO()
        f.write(config_pg_hba)
        put(f, remote_path, use_sudo=True)
        f.close()

        sudo('chown postgres:postgres {}'.format(remote_path))

        sudo('cd /tmp')

        cmd = 'psql -U postgres -d postgres -c "alter user postgres with password \'{}\';"'
        cmd = cmd.format(get_remote_pg_root_password())
        sudo(cmd, user='postgres')

        cmd = 'echo "listen_addresses = \'*\'" >> /etc/postgresql/{}/main/postgresql.conf'\
            .format(settings.POSTGRESQL_VERSION)
        sudo(cmd)
        sudo('systemctl restart postgresql')

    def handle(self, *args, **options):
        self.init()
        self.setup_initial()
        self.setup_redis()
        self.setup_postgresql()
        print('Server setuped successfully.')
