import shutil
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        l = [
            'core',
            'msg',
            'payment',
            'web',
        ]
        for name in l:
            path = '{}/screent/{}/migrations'.format(
                settings.BASE_DIR, name)
            shutil.rmtree(path, ignore_errors=True)

        print('Migrations have been removed.')
