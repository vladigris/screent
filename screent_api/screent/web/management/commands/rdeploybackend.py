from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import run
from fabric.api import put
from fabric.api import env
from fabric.api import cd
from fabric.contrib.project import rsync_project


class Command(BaseCommand):
    help = 'Deploy API to remote server'

    def init(self):
        env.host_string = 'vlad@{}'.format(settings.SERVER_APP_IP)

    def deploy(self):
        run('mkdir -p /home/vlad/screent_api')

        local_dir = settings.BASE_DIR + '/'
        remote_dir = '/home/vlad/screent_api/'
        rsync_project(local_dir=local_dir, remote_dir=remote_dir,
                      delete=True,
                      exclude=['__pycache__', '.cache', 'db.dump', '.git'])

        put('/home/vlad/.scdata', '/home/vlad/.scdata')

        with cd('/home/vlad/screent_api/screent/screent'):
            run('touch reload.ini')

    def handle(self, *args, **options):
        self.init()
        self.deploy()
