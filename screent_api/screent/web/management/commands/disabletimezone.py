from django.core.management.base import BaseCommand
from web.management.helpers import (
    get_local_db_connection, get_local_test_db_connection)


class Command(BaseCommand):
    """
    Currently not used
    """
    help = 'Disable timezone for timestamp columns created_at'

    def handle(self, *args, **options):

        query1 = ('alter table screens alter column created_at '
                  'type timestamp without time zone;')

        query2 = ('alter table keyboards alter column created_at '
                  'type timestamp without time zone;')

        con, cur = get_local_db_connection()

        cur.execute(query1)
        cur.execute(query2)

        con, cur = get_local_test_db_connection()

        cur.execute(query1)
        cur.execute(query2)
