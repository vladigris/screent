from django.core.management.base import BaseCommand
from django.utils.translation import ugettext_lazy as _
from web.models import Email


class Command(BaseCommand):
    def create_signup(self):
        e = Email(code=Email.SIGNUP)
        e.name = _('signup')
        e.subject = _('Signup')
        e.txt = 'file'
        e.html = 'file'
        e.save()

    def create_reset_password(self):
        e = Email(code=Email.RESET_PASSWORD)
        e.name = _('reset password')
        e.subject = _('Reset password')
        e.txt = 'file'
        e.html = 'file'
        e.save()

    def handle(self, *args, **options):
        self.create_signup()
        self.create_reset_password()
        print('Emails have been created.')
