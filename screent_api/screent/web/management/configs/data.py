config_nginx_landing = """

server {
    listen  80;
    server_name %(server_name)s www.%(server_name)s;
    return 301 https://%(server_name)s$request_uri;
}

server {
    server_name %(server_name)s www.%(server_name)s;

    listen 443;
    ssl on;
    ssl_certificate /etc/ssl/screent.co/server.crt;
    ssl_certificate_key /etc/ssl/screent.co/data.key;

    ssl_protocols SSLv3 TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers ALL:!aNULL:!ADH:!eNULL:!LOW:!EXP:RC4+RSA:+HIGH:+MEDIUM;
    ssl_prefer_server_ciphers on;

    charset utf-8;
    access_log  /var/log/nginx/sites/access/%(server_name)s.log;
    error_log   /var/log/nginx/sites/error/%(server_name)s.log crit;

    access_log off;
    root /home/vlad/html/%(server_name)s;

    if ($host = 'www.%(server_name)s' ) {
        rewrite  ^/(.*)$  https://%(server_name)s/$1  permanent;
    }
}
"""


# Used for both app and app-stage
config_nginx_react = """

server {
    listen  80;
    server_name %(server_name)s;
    return 301 https://%(server_name)s$request_uri;
}

server {
    server_name %(server_name)s;

    listen 443;
    ssl on;
    ssl_certificate /etc/ssl/screent.co/server.crt;
    ssl_certificate_key /etc/ssl/screent.co/data.key;

    ssl_protocols SSLv3 TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers ALL:!aNULL:!ADH:!eNULL:!LOW:!EXP:RC4+RSA:+HIGH:+MEDIUM;
    ssl_prefer_server_ciphers on;

    client_max_body_size 20M;
    charset utf-8;
    access_log  /var/log/nginx/sites/access/%(server_name)s.log;
    access_log off;
    error_log   /var/log/nginx/sites/error/%(server_name)s.log crit;

    root /home/vlad/%(folder)s/build;

    location / {
        try_files $uri  $uri/ /index.html;
    }

    # Attempt to load static files, if not found route to @rootfiles
    location ~ (.+)\.(html|json|txt|js|css|jpg|jpeg|gif|png|svg|ico|eot|otf|woff|woff2|ttf)$ {
        try_files $uri @rootfiles;
    }

    # Check for app route "directories" in the request uri and strip "directories"
    # from request, loading paths relative to root.
    location @rootfiles {
        # rewrite ^/(?:some|foo/bar)/(.*) /$1 redirect;
        rewrite ^/(.*)/(.*)  /404 redirect;
    }
}
"""


config_nginx_backend = """

server {
    listen  80;
    server_name %(server_name)s;
    return 301 https://%(server_name)s$request_uri;
}

server {
    server_name %(server_name)s;

    listen 443;
    ssl on;
    ssl_certificate /etc/ssl/screent.co/server.crt;
    ssl_certificate_key /etc/ssl/screent.co/data.key;

    ssl_protocols SSLv3 TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers ALL:!aNULL:!ADH:!eNULL:!LOW:!EXP:RC4+RSA:+HIGH:+MEDIUM;
    ssl_prefer_server_ciphers on;

    client_max_body_size 20M;
    charset utf-8;
    access_log  /var/log/nginx/sites/access/%(server_name)s.log;
    access_log off;
    error_log   /var/log/nginx/sites/error/%(server_name)s.log crit;

    location /static/ {
        autoindex off;
        root /home/vlad/screent_api/;
    }

    location / {
        uwsgi_pass 127.0.0.1:4001;
        include uwsgi_params;
        uwsgi_buffers 8 128k;
    }
}
"""


config_pg_hba = """
local   all   postgres      trust
local   all   all                   peer
host    all   all    127.0.0.1/32   md5
host    all   all    ::1/128        md5
host    all   all    0.0.0.0/0      md5
"""


config_systemd_backend = """
[Unit]
Description=Backend API
After=network.target

[Service]
Type=simple
User=vlad
Group=vlad
WorkingDirectory=/home/vlad/screent_api
ExecStart=/home/vlad/.venvs/screent/bin/uwsgi --ini /home/vlad/screent_api/screent/screent/uwsgi.ini
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
"""

config_systemd_celery = """
[Unit]
Description=Celery
After=network.target

[Service]
Type=simple
User=vlad
Group=vlad
WorkingDirectory=/home/vlad/screent_api/screent
ExecStart=/home/vlad/.venvs/screent/bin/celery worker -A screent -l info -B
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
"""

config_systemd_tcp = """
[Unit]
Description=TCPServer
After=network.target

[Service]
Type=simple
User=vlad
Group=vlad
WorkingDirectory=/home/vlad/screent_tcp
ExecStart=/home/vlad/screent_tcp/screent
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
"""
