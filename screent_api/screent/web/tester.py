import boto3
import json
import psycopg2
import django
import os
import sys

from os.path import abspath
from os.path import dirname
from os.path import join
from os.path import basename

path = abspath(join(dirname(abspath(__file__)), '..'))
sys.path.append(path)
os.environ.setdefault(
    'DJANGO_SETTINGS_MODULE', 'screent.settings.development')
django.setup()
from django.conf import settings


class Temp:
    def get_client(self):
        session = boto3.session.Session()
        client = session.client(
            's3',
            region_name='nyc3',
            endpoint_url='https://nyc3.digitaloceanspaces.com',
            aws_access_key_id=settings.SPACES_KEY,
            aws_secret_access_key=settings.SPACES_SECRET)
        return client

    def get_connection(self):
        dic = {
            'host': '',
            'password': '',
            'user': 'uscreent',
            'db': 'screent'
        }
        cs = "host='{host}' dbname='{db}' user='{user}' password='{password}'"\
            .format(**dic)
        con = psycopg2.connect(cs)
        cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
        # cur = con.cursor()
        return con, cur

    def get_screens_json(self):
        file_json_path = '{}/screens.json'.format(settings.BASE_DIR)

        l = []
        con, cur = self.get_connection()
        cur.execute('select * from screens order by created_at')
        for dic in cur.fetchall():
            computer_guid = settings.TEST_COMPUTER_GUID
            file_name = basename(dic['file_path'])
            new_file_path = '{}/{}'.format(computer_guid, file_name)

            if dic['computer_id'] != 3:
                continue

            d = {'spaces_path': new_file_path}
            for k, v in dic.items():
                if k == 'created_at':
                    v = v.strftime('%Y-%m-%d %H:%M:%S')
                d[k] = v

            d.pop('id')
            d.pop('computer_id')
            l.append(d)

        print(len(l))
        s = json.dumps(l, indent=4, separators=(',', ': '))
        f = open(file_json_path, 'w')
        f.write(s)
        f.close()

    def get_keyboards_json(self):
        file_json_path = '{}/keyboards.json'.format(settings.BASE_DIR)

        l = []
        con, cur = self.get_connection()
        cur.execute('select * from keyboards order by created_at')
        for dic in cur.fetchall():
            if dic['computer_id'] != 3:
                continue

            d = {}
            for k, v in dic.items():
                if k == 'created_at':
                    v = v.strftime('%Y-%m-%d %H:%M:%S')
                d[k] = v

            d.pop('id')
            d.pop('computer_id')
            l.append(d)

        print(len(l))
        s = json.dumps(l, indent=4, separators=(',', ': '))
        f = open(file_json_path, 'w')
        f.write(s)
        f.close()

    def upload_files(self):
        file_json_path = '{}/screens.json'.format(settings.BASE_DIR)
        client = self.get_client()
        l = json.loads(open(file_json_path, 'r').read())
        count = 0
        for d in l:
            count += 1
            client.upload_file(
                d['file_path'], 'screent', d['spaces_path'],
                ExtraArgs={'ContentType': 'image/jpeg', 'ACL': 'public-read'})

            print(count)


if __name__ == '__main__':
    t = Temp()
    # t.get_screens_json()
    # t.get_keyboards_json()
    t.upload_files()
