from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from core.models import User


class GuidMixin:
    def validate_guid(self, value):
        if not User.objects.filter(guid=value).exists():
            raise ValidationError(
                _('Provided link is not correct'))
        return value


class SignupMixin:
    def validate(self, data):
        user = User.objects.get(guid=data['guid'])
        if user.is_signup_completed:
            raise ValidationError(
                _('Registration already has been completed!'))

        if user.signup_token != data['signup_token']:
            raise ValidationError(
                _('Provided token is not correct'))
        return data


class ResetPasswordMixin:
    def validate(self, data):
        user = User.objects.filter(guid=data['user_guid'],
                                   reset_token=data['reset_token']).first()

        if user is None:
            raise ValidationError(_('Incorrect link'))

        data['user'] = user
        return data
