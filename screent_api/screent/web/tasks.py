from fabric.api import lcd
from fabric.api import local
from celery.decorators import periodic_task
from celery.task.schedules import crontab
import datetime
from django.utils import timezone
from screent import capp
from web.models import Emailer
from web.models import Logging


@capp.task
def send_email(id):
    em = Emailer.objects.filter(id=id, sent=False).first()
    if em is None:
        return

    em.send()
    em.refresh_from_db()

    if em.sent is False:
        send_email.apply_async(args=[em.id], countdown=300)


@periodic_task(run_every=crontab(minute=0, hour=0))
def clear_old_logs():
    """
    Removes old rows from Log
    """
    Logging.objects.filter(
        dt__lt=timezone.now() - datetime.timedelta(days=15)).delete()


@capp.task
def react_pull():
    """
    Received from Bitbucket webhook.
    """
    with lcd('/home/vlad/screent_react_stage'):
        local('ls')
        local('git pull')
