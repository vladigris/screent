from django.contrib import admin
from django.db.models import Model
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import IntegerField
from django.db.models import DateTimeField
from django.db.models import BooleanField

from web.services.helpers import get_guid


class TariffUser(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    tariff = ForeignKey('Tariff')
    user = ForeignKey('core.User')
    bill_day = IntegerField(null=True, default=None, blank=True)
    paid_till = DateTimeField(default=None, null=True, blank=True)
    is_active = BooleanField(default=False)
    created_at = DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'payment'
        verbose_name = 'tariff user'
        verbose_name_plural = 'tariff users'
        ordering = ['user', 'tariff']
        db_table = 'tariff_users'

    def __str__(self):
        return '{}: {}'.format(self.user, self.tariff)


@admin.register(TariffUser)
class TariffUserAdmin(admin.ModelAdmin):
    list_display = (
        'tariff',
        'user',
        'bill_day',
        'paid_till',
        'is_active'
    )
