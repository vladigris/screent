from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import BooleanField
from django.db.models import DateTimeField

from web.services.helpers import get_guid


class PlatformManager(Manager):
    def create(self, name, domain):
        p = Platform(name=name, domain=domain)
        p.save()
        return p


class Platform(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    name = CharField(max_length=100)
    domain = CharField(max_length=50)
    is_active = BooleanField(default=True)
    created_at = DateTimeField(auto_now_add=True)

    objects = PlatformManager()

    class Meta:
        app_label = 'payment'
        verbose_name = 'platform'
        verbose_name_plural = 'platforms'
        ordering = ['name']
        db_table = 'platforms'

    def __str__(self):
        return self.name


@admin.register(Platform)
class PlatformAdmin(admin.ModelAdmin):
    list_display = ('name', 'domain', 'is_active')
