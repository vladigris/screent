from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import SmallIntegerField
from django.db.models import TextField
from django.db.models import BooleanField
from django.db.models import DateTimeField
from django.db.models import ForeignKey

from web.services.helpers import get_guid


class TariffManager(Manager):
    pass


class Tariff(Model):
    SMALL = 'Small'
    MEDIUM = 'Medium'
    LARGE = 'Large'
    EXTRA_LARGE = 'Extra Large'

    DEFAULT_INTERVAL = 30
    DEFAULT_DAY_LIMIT = 500

    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    lang = ForeignKey('web.Lang', null=True, default=None, blank=True)
    name = CharField(max_length=100, unique=True)
    price = IntegerField(null=True, blank=True, default=None)
    min_interval = SmallIntegerField(
        help_text='minimum interval in seconds',
        default=DEFAULT_INTERVAL)
    max_computers = SmallIntegerField(
        help_text='maximum number of computers',
        default=1)
    day_limit = IntegerField(verbose_name='Max screens per day')
    storage = IntegerField(verbose_name='Max number of screenshots')
    description = TextField(
        null=True, default=None, blank=True)
    sort = SmallIntegerField(default=0)
    is_featured = BooleanField(default=False)
    is_active = BooleanField(default=True)
    created_at = DateTimeField(auto_now_add=True)

    objects = TariffManager()

    class Meta:
        app_label = 'payment'
        verbose_name = 'Tariff'
        verbose_name_plural = 'Tariffs'
        ordering = ['sort']
        db_table = 'tariffs'

    def __str__(self):
        return self.name


@admin.register(Tariff)
class TariffAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'lang',
        'price',
        'min_interval',
        'max_computers',
        'day_limit',
        'storage',
        'sort',
        'is_featured',
        'is_active'
    )
    list_editable = (
        'sort',
        'is_featured',
    )
    exclude = ('price',)
