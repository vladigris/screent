from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import DateTimeField
from django.db.models import DecimalField

from web.services.helpers import get_guid


class PaymentManager(Manager):
    pass


class Payment(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    user = ForeignKey('core.User')
    tariff = ForeignKey('Tariff')
    platform = ForeignKey('Platform')
    created_at = DateTimeField(auto_now_add=True)
    paid_at = DateTimeField(default=None, null=True, blank=True)
    paid_till = DateTimeField(default=None, null=True, blank=True)
    amount = DecimalField(max_digits=10, decimal_places=2)
    description = CharField(
        max_length=1000, null=True, default=None, blank=True)

    objects = PaymentManager()

    class Meta:
        app_label = 'payment'
        verbose_name = 'payment'
        verbose_name_plural = 'payments'
        ordering = ['created_at']
        db_table = 'payments'

    def __str__(self):
        return '{}: {} {}'.format(self.user, self.tariff, self.created_at)


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'tariff',
        'platform',
        'created_at',
        'paid_at',
        'paid_till',
        'amount',
        'description'
    )
