import base64
import copy
import pickle
from django.contrib import admin
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.template.loader import get_template
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import BooleanField
from django.db.models import ForeignKey

from web.services.helpers import get_guid


class NotificationManager(Manager):
    def get_template(self, code):
        file_path = '{}/screent/screent/templates/msg/notifications/{}.html'\
            .format(settings.BASE_DIR, code)
        return get_template(file_path)

    def process(self, code, user, **kwargs):
        """
        Get template by type and render with kwargs
        all notification types.
        Additional contexts (if needed):
        def <code>_context
        """
        html = self.get_template(code)

        d = copy.deepcopy(kwargs)

        # Add user object for whom notification to context
        d['user'] = user

        addtitional_context = getattr(
            self, '{}_context'.format(code), None)
        if addtitional_context is not None:
            d.update(addtitional_context(**kwargs))

        n = self.model.objects.create(
            user=user,
            type=code,
            text=html.render(d))
        return n


class Notification(Model):
    FREE_TRIAL_STARTED = 'free_trial_started'

    TYPE_CHOICES = (
        (FREE_TRIAL_STARTED, FREE_TRIAL_STARTED),
    )

    user = ForeignKey('core.User')
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    type = CharField(max_length=100, null=True, default=None, blank=True,
                     choices=TYPE_CHOICES)
    text = CharField(max_length=255)
    created_at = DateTimeField(auto_now_add=True)
    is_read = BooleanField(default=False)
    mapping = CharField(
        max_length=1000, null=True, default=None, blank=True,
        help_text='key: value object with contract for particular type')

    objects = NotificationManager()

    class Meta:
        app_label = 'msg'
        verbose_name = 'notification'
        verbose_name_plural = 'notifications'
        ordering = ['-created_at']
        db_table = 'notifications'

    def __str__(self):
        return self.text

    @property
    def ago(self):
        delta = timezone.now() - self.created_at
        if delta.seconds < 60 * 60:
            return '{} {}'.format(int(delta.seconds / 60), _('minutes ago'))

        if delta.seconds < 24 * 60 * 60:
            return '{} {}'.format(int(delta.seconds / 3600), _('hours ago'))

        if delta.days == 1:
            return _('1 day ago')

        return '{} {}'.format(delta.days, _('days ago'))

    @property
    def mapping_contract(self):
        if self.mapping is None:
            return None
        return pickle.loads(base64.b64decode(self.mapping))

    def put_mapping(self, value):
        self.mapping = base64.b64encode(pickle.dumps(value))


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'type',
        'guid',
        'text',
        'created_at',
        'is_read'
    )
    readonly_fields = ('guid',)
