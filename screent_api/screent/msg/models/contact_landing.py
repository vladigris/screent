from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import EmailField
from django.db.models import TextField
from django.db.models import DateTimeField
from django.db.models import BooleanField

from msg.services.slack_alert import SlackAlert
from web.services.helpers import get_guid


class ContactLandingManager(Manager):
    pass


class ContactLanding(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    lang = ForeignKey('web.Lang', null=True, default=None, blank=True)
    name = CharField(max_length=100)
    email = EmailField(max_length=100)
    text = TextField()
    created_at = DateTimeField(auto_now_add=True)
    is_answered = BooleanField(default=False)

    objects = ContactLandingManager()

    class Meta:
        app_label = 'msg'
        verbose_name = 'contact landing'
        verbose_name_plural = 'contacts landing'
        ordering = ['-created_at']
        db_table = 'contactlandings'

    def __str__(self):
        return self.email

    @property
    def alert_text(self):
        text = 'name: {}\n'.format(self.name)
        text += 'email: {}\n'.format(self.email)
        text += '{}'.format(self.text)
        return text


@receiver(post_save, sender=ContactLanding)
def slack_alert_on_receive(sender, instance, created, **kwargs):
    if created and settings.PRODUCTION:
        SlackAlert().send(instance.alert_text)


@admin.register(ContactLanding)
class ContactLandingAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'email', 'name', 'lang', 'is_answered')
    list_filter = ('is_answered',)
    list_editable = ('is_answered',)
