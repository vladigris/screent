from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import DateTimeField
from django.db.models import BooleanField

from msg.services.slack_alert import SlackAlert
from web.services.helpers import get_guid


class ContactManager(Manager):
    pass


class Contact(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    user = ForeignKey('core.User')
    subject = CharField(max_length=255)
    text = TextField()
    created_at = DateTimeField(auto_now_add=True)
    is_answered = BooleanField(default=False)

    objects = ContactManager()

    class Meta:
        app_label = 'msg'
        verbose_name = 'contact'
        verbose_name_plural = 'contacts'
        ordering = ['-created_at']
        db_table = 'contacts'

    def __str__(self):
        return self.email

    @property
    def alert_text(self):
        text = 'name: {}\n'.format(self.user.first_name)
        text = 'subject: {}\n'.format(self.subject)
        text += 'email: {}\n'.format(self.user.email)
        text += '{}'.format(self.text)
        return text


@receiver(post_save, sender=Contact)
def slack_alert_on_receive(sender, instance, created, **kwargs):
    if created and settings.PRODUCTION:
        SlackAlert().send(instance.alert_text)


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'is_answered',
        'user',
        'subject',
        'text',
    )
    list_filter = ('is_answered',)
    list_editable = ('is_answered',)
