from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import DateTimeField

from web.services.helpers import get_guid


class LogManager(Manager):
    pass


class Log(Model):
    DEBUG = 'debug'
    INFO = 'info'
    WARNING = 'warning'
    ERROR = 'error'

    LEVEL_CHOICES = (
        (DEBUG, 'Debug'),
        (INFO, 'Info'),
        (WARNING, 'Warning'),
        (ERROR, 'Error'),
    )

    API = 'api'
    TCP = 'tcp'
    DESKTOP = 'desktop'

    SERVICE_CHOICES = (
        (API, 'API'),
        (TCP, 'TCP'),
        (DESKTOP, 'Desktop'),
    )

    PROD = 'prod'
    STAGE = 'stage'
    DEV = 'dev'

    ENV_CHOICES = (
        (PROD, 'Production'),
        (STAGE, 'Staging'),
        (DEV, 'Development'),
    )

    computer = ForeignKey('core.Computer', related_name='logs')
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    level = CharField(max_length=10, choices=LEVEL_CHOICES)
    service = CharField(max_length=10, choices=SERVICE_CHOICES)
    env = CharField(max_length=10, choices=ENV_CHOICES)
    text = TextField()
    created_at = DateTimeField()

    objects = LogManager()

    def __str__(self):
        return '{}...'.format(self.text[:10])

    class Meta:
        app_label = 'msg'
        ordering = ['-created_at']
        db_table = 'logs'


@admin.register(Log)
class KeyboardAdmin(admin.ModelAdmin):
    list_display = (
        'computer',
        'level',
        'service',
        'env',
        'text',
        'created_at',
    )
