import uuid
from django.contrib import admin
from django.db import transaction
from django.apps import apps
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import DateTimeField
from django.db.models import BooleanField

from msg.models.message_item import MessageItemInline
from web.services.helpers import get_guid


class MessageManager(Manager):
    @transaction.atomic
    def create_message(self, subject, text, user, is_client=True):
        MessageItem = apps.get_model('msg', 'MessageItem')

        msg = Message(subject=subject, user=user)
        msg.save()

        msg_item = MessageItem(message=msg, text=text, is_client=is_client)
        msg_item.save()

        return msg

    @transaction.atomic
    def create_item(self, message, text, is_client):
        """
        message param can be id or message object.
        Returns updated message.
        """
        MessageItem = apps.get_model('msg', 'MessageItem')
        if type(message) == int:
            message = Message.objects.get(id=message)

        item = MessageItem(message=message, text=text, is_client=is_client)
        if is_client:
            message.is_read = True
            message.is_answered = False
        else:
            message.is_read = False
            message.is_answered = True

        item.save()
        message.save()
        return message


class Message(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    user = ForeignKey(
        'core.User', related_name='messages')
    created_at = DateTimeField(auto_now_add=True)
    updated_at = DateTimeField(auto_now_add=True)
    subject = CharField(max_length=255)
    is_answered = BooleanField(
        default=False, help_text='Answered by support')
    is_read = BooleanField(default=True, help_text='Read by client')
    token = CharField(max_length=50, db_index=True, blank=True)

    objects = MessageManager()

    class Meta:
        app_label = 'msg'
        verbose_name = 'message'
        verbose_name_plural = 'messages'
        ordering = ['-updated_at']
        db_table = 'messages'

    def __str__(self):
        return self.subject

    def save(self, *args, **kwargs):
        if not self.pk:
            self.token = uuid.uuid4().hex
        super().save(*args, **kwargs)


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    inlines = [MessageItemInline]
    list_display = (
        'updated_at',
        'guid',
        'user',
        'subject',
        'is_answered',
        'is_read',
        'created_at'
    )
    exclude = ('guid', 'token')
