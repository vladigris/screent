from django.contrib import admin
from django.db.models import Model
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import DateTimeField
from django.db.models import TextField
from django.db.models import BooleanField

from web.services.helpers import get_guid


class MessageItem(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    message = ForeignKey('Message', related_name='items')
    created_at = DateTimeField(auto_now_add=True)
    text = TextField()
    is_client = BooleanField(default=False)

    class Meta:
        app_label = 'msg'
        verbose_name = 'message item'
        verbose_name_plural = 'message items'
        ordering = ['-created_at']
        db_table = 'message_items'

    def __str__(self):
        return str(self.id)


class MessageItemInline(admin.TabularInline):
    model = MessageItem
    fields = (
        'message',
        'text',
        'is_client'
    )
    extra = 0
