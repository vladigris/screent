from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import CharField
from rest_framework.serializers import EmailField

from msg.models import Contact
from msg.models import ContactLanding
from msg.models import Notification


class ContactSerializer(ModelSerializer):
    class Meta:
        model = Contact
        fields = (
            'subject',
            'text',
        )

    def create(self, validated_data):
        d = validated_data
        d['user'] = self.context.get('user')
        return Contact.objects.create(**d)


class ContactLandingSerializer(ModelSerializer):
    name = CharField()
    email = EmailField()
    text = CharField()

    class Meta:
        model = ContactLanding
        fields = (
            'name',
            'email',
            'text',
        )


class NotificationSerializer(ModelSerializer):
    class Meta:
        model = Notification
        fields = (
            'id',
            'type',
            'text',
            'created_at',
            'is_read',
            'mapping_contract'
        )
