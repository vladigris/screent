from django.utils.translation import ugettext_lazy as _
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response

from msg.serializers import ContactSerializer
from msg.serializers import ContactLandingSerializer
from msg.serializers import NotificationSerializer

from msg.models import Notification

from web.views import AuthMixin
from web.views import r400


class Contact(AuthMixin, APIView):
    """
    post:
        <h1>Contact from React</h1>
        subject<br />
        text<br />
        <b>Response code:</b> 201<br />
    """
    def post(self, request, **kwargs):
        s = ContactSerializer(data=request.data,
                              context={'user': request.user})
        s.is_valid(raise_exception=True)
        s.save()
        return Response({'message': 'Your message has been received!'},
                        status=201)


class ContactLanding(APIView):
    """
    post:
        <h1>Contact from landing</h1>
        name<br />
        email<br />
        text<br />
        <b>Response code:</b> 201<br />
    """
    def post(self, request, **kwargs):
        s = ContactLandingSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        s.save()
        return Response({'message': 'Your message has been received!'},
                        status=201)


class Notifications(AuthMixin, ListAPIView):
    """
    get:
        <h1>User can get list of notifications</h1>
        Notification object:<br />
        id<br />
        text<br />
        is_read<br />
        created_at<br />
        <b>Response:</b> array of notification object<br />
        <b>Response code:</b> 200<br />
    """
    serializer_class = NotificationSerializer

    def get_queryset(self):
        return Notification.objects.filter(
            user_id=self.request.user.id, is_read=False)


class MarkNotification(AuthMixin, APIView):
    """
    post:
        <h1>User marks notification as read</h1>
        <b>Input params: </b> No<br />
        <b>Response code:</b> 204<br />
    """
    def post(self, request, id, **kwargs):
        n = Notification.objects\
            .filter(id=id, user_id=request.user.id).first()

        if n is None:
            return r400(_('Incorrect url'))

        n.is_read = True
        n.save()
        return Response(status=204)
