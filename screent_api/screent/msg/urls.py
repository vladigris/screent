from django.conf.urls import url
from .import views as v


urlpatterns = [
    url(
        r'^(?P<version>(v1))/msg/notifications/(?P<id>\d+)/mark$',
        v.MarkNotification.as_view(),
        name='mark_notification'),

    url(
        r'^(?P<version>(v1))/msg/notifications$',
        v.Notifications.as_view(),
        name='notifications'),

    url(
        r'^(?P<version>(v1))/msg/contact$',
        v.Contact.as_view(),
        name='contact'),

    url(
        r'^(?P<version>(v1))/msg/contact/landing$',
        v.ContactLanding.as_view(),
        name='contact_landing'),
]
