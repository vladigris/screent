# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-25 22:11
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import web.services.helpers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('core', '0001_initial'),
        ('web', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.CharField(default=web.services.helpers.get_guid, max_length=50, unique=True)),
                ('subject', models.CharField(max_length=255)),
                ('text', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('is_answered', models.BooleanField(default=False)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'contact',
                'ordering': ['-created_at'],
                'db_table': 'contacts',
                'verbose_name_plural': 'contacts',
            },
        ),
        migrations.CreateModel(
            name='ContactLanding',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.CharField(default=web.services.helpers.get_guid, max_length=50, unique=True)),
                ('name', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=100)),
                ('text', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('is_answered', models.BooleanField(default=False)),
                ('lang', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='web.Lang')),
            ],
            options={
                'verbose_name': 'contact landing',
                'ordering': ['-created_at'],
                'db_table': 'contactlandings',
                'verbose_name_plural': 'contacts landing',
            },
        ),
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.CharField(default=web.services.helpers.get_guid, max_length=50, unique=True)),
                ('level', models.CharField(choices=[('debug', 'Debug'), ('info', 'Info'), ('warning', 'Warning'), ('error', 'Error')], max_length=10)),
                ('service', models.CharField(choices=[('api', 'API'), ('tcp', 'TCP'), ('desktop', 'Desktop')], max_length=10)),
                ('env', models.CharField(choices=[('prod', 'Production'), ('stage', 'Staging'), ('dev', 'Development')], max_length=10)),
                ('text', models.TextField()),
                ('created_at', models.DateTimeField()),
                ('computer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='logs', to='core.Computer')),
            ],
            options={
                'ordering': ['-created_at'],
                'db_table': 'logs',
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.CharField(default=web.services.helpers.get_guid, max_length=50, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
                ('subject', models.CharField(max_length=255)),
                ('is_answered', models.BooleanField(default=False, help_text='Answered by support')),
                ('is_read', models.BooleanField(default=True, help_text='Read by client')),
                ('token', models.CharField(blank=True, db_index=True, max_length=50)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'message',
                'ordering': ['-updated_at'],
                'db_table': 'messages',
                'verbose_name_plural': 'messages',
            },
        ),
        migrations.CreateModel(
            name='MessageItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.CharField(default=web.services.helpers.get_guid, max_length=50, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('text', models.TextField()),
                ('is_client', models.BooleanField(default=False)),
                ('message', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='msg.Message')),
            ],
            options={
                'verbose_name': 'message item',
                'ordering': ['-created_at'],
                'db_table': 'message_items',
                'verbose_name_plural': 'message items',
            },
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.CharField(default=web.services.helpers.get_guid, max_length=50, unique=True)),
                ('type', models.CharField(blank=True, choices=[('free_trial_started', 'free_trial_started')], default=None, max_length=100, null=True)),
                ('text', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('is_read', models.BooleanField(default=False)),
                ('mapping', models.CharField(blank=True, default=None, help_text='key: value object with contract for particular type', max_length=1000, null=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'notification',
                'ordering': ['-created_at'],
                'db_table': 'notifications',
                'verbose_name_plural': 'notifications',
            },
        ),
    ]
