import requests
from django.conf import settings


class SlackAlert:
    def send(self, text):
        url = settings.SLACK_ALERTS_URL
        headers = {'Content-Type': 'application/json'}
        try:
            requests.post(url, json={'text': text}, headers=headers)
        except:
            pass
