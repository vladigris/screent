package conf

type ScData struct {
	SecretKey    string
	SpacesKey    string
	SpacesSecret string
	SpacesRegion string
	SpacesBucket string

	EmailFrom     string
	EmailHost     string
	EmailUser     string
	EmailPassword string
	EmailPort     int

	AppDomain     string
	AppUrl        string
	ApiDomain     string
	ApiUrl        string
	LandingDomain string
	LandingUrl    string

	DbName     string
	DbUser     string
	DbHost     string
	DbPassword string

	ServerAppIp     string
	ServerStorageIp string
	ServerAppIps    []string

	TestUserEmail     string
	TestUserPassword  string
	Test2UserEmail    string
	Test2UserPassword string
	TestComputerGuid  string

	RedisKeyComputerConfig         string
	RedisKeyComputerConfigSnapshot string
	RedisKeyComputer               string
	RedisKeyUserToken              string
	RedisKeyLogData                string
	RedisKeyKeyboardData           string
	RedisKeyScreenData             string
	RedisDb                        int64
	RedisDbTest                    int64
	SlackAlertsUrl                 string

	Versions map[string]Version
}
