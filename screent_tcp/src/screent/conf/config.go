/*
	AppMode is defined by environment variable SCREENT_APP_MODE.
	It can be production/development/test
	If environment variable is not set - considered production.

	IsDebug is defined by environment variable SCREENT_IS_DEBUG
	If environment variable is not set - considered false.

*/

package conf

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"screent/files"
	"screent/helpers"
	"time"

	"gopkg.in/redis.v3"

	"github.com/go-pg/pg"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	AppMode string
	IsDebug bool

	Modes   = []string{"production", "development", "test"}
	showsql = flag.Bool("showsql", false, "Show SQL quesries")
)

func init() {
	AppMode = os.Getenv("SCREENT_APP_MODE")
	// not set
	if len(AppMode) == 0 {
		AppMode = "production"
	}

	IsDebug = false
	var x = os.Getenv("SCREENT_IS_DEBUG")
	if len(x) > 0 {
		IsDebug = true
	}
}

type Config struct {
	ApiDomain                      string
	ApiUrl                         string
	AppDomain                      string
	AppUrl                         string
	LandingDomain                  string
	LandingUrl                     string
	DbUser                         string
	DbPassword                     string
	DbName                         string
	DbTestName                     string
	EmailFrom                      string
	EmailHost                      string
	EmailPassword                  string
	EmailUser                      string
	EmailPort                      int
	EmailUseTls                    bool
	KeyboardSplitter               string
	Lang                           string
	Langs                          []string
	MinimumPasswordLength          int
	RedisDb                        int64
	RedisDbTest                    int64
	RedisKeyComputerConfig         string
	RedisKeyComputerConfigSnapshot string
	RedisKeyComputer               string
	RedisKeyUserToken              string
	RedisKeyLogData                string
	RedisKeyKeyboardData           string
	RedisKeyScreenData             string
	ServerAppIp                    string
	ServerStorageIp                string
	ServerAppIps                   []string
	SlackAlertsUrl                 string
	SpacesKey                      string
	SpacesSecret                   string
	SpacesRegion                   string
	SpacesBucket                   string
	TcpPort                        int
	TcpBufferSize                  int
	TcpHost                        string
	TokenExpireMinutes             int
	Versions                       map[string]Version
	IsProduction                   bool
	IsDevelopment                  bool
	IsTesting                      bool
}

func getScData() *ScData {
	bytes, err := files.Asset("home/vlad/.scdata")
	if err != nil {
		log.Fatal(".scdata not exists")
	}

	data := &ScData{}
	err = json.Unmarshal(bytes, data)
	if err != nil {
		log.Fatal("Can not get json from .scdata")
	}
	return data
}

func NewConfig() *Config {
	if !helpers.SliceHasString(Modes, AppMode) {
		panic("Incorrect AppMode")
	}

	// After changing scdata - don't forget ./run embed
	scData := getScData()

	c := &Config{
		AppDomain:        scData.AppDomain,
		AppUrl:           scData.AppUrl,
		ApiDomain:        scData.ApiDomain,
		ApiUrl:           scData.ApiUrl,
		LandingDomain:    scData.LandingDomain,
		LandingUrl:       scData.LandingUrl,
		DbName:           scData.DbName,
		DbTestName:       "testgo_screentco",
		DbUser:           scData.DbUser,
		DbPassword:       scData.DbPassword,
		EmailFrom:        scData.EmailFrom,
		EmailHost:        scData.EmailHost,
		EmailPassword:    scData.EmailPassword,
		EmailUser:        scData.EmailUser,
		EmailPort:        scData.EmailPort,
		EmailUseTls:      true,
		KeyboardSplitter: "/:/:/",
		Lang:             "en",
		Langs:            []string{"en"},
		MinimumPasswordLength:          8,
		RedisDb:                        scData.RedisDb,
		RedisDbTest:                    scData.RedisDbTest,
		RedisKeyComputerConfig:         scData.RedisKeyComputerConfig,
		RedisKeyComputerConfigSnapshot: scData.RedisKeyComputerConfigSnapshot,
		RedisKeyComputer:               scData.RedisKeyComputer,
		RedisKeyUserToken:              scData.RedisKeyUserToken,
		RedisKeyLogData:                scData.RedisKeyLogData,
		RedisKeyKeyboardData:           scData.RedisKeyKeyboardData,
		RedisKeyScreenData:             scData.RedisKeyScreenData,
		ServerAppIp:                    scData.ServerAppIp,
		ServerStorageIp:                scData.ServerStorageIp,
		ServerAppIps:                   scData.ServerAppIps,
		SlackAlertsUrl:                 scData.SlackAlertsUrl,
		SpacesKey:                      scData.SpacesKey,
		SpacesSecret:                   scData.SpacesSecret,
		SpacesRegion:                   scData.SpacesRegion,
		SpacesBucket:                   scData.SpacesBucket,
		TcpHost:                        "",
		TcpPort:                        5000,
		TcpBufferSize:                  1024,
		// HttpToken
		TokenExpireMinutes: 1000,
		Versions:           scData.Versions,
	}

	if AppMode == "production" {
		c.IsProduction = true
	} else if AppMode == "development" {
		c.IsDevelopment = true
	} else if AppMode == "test" {
		c.IsTesting = true
	}

	if c.IsDevelopment {
		c.ApiUrl = "http://localhost:4000"
	}

	return c
}

func (c *Config) GetDB() *pg.DB {
	var dbName string
	if c.IsTesting {
		dbName = c.DbTestName
	} else {
		dbName = c.DbName
	}

	addr := "127.0.0.1:5432"
	if c.IsProduction {
		addr = fmt.Sprintf("%s:5432", c.ServerStorageIp)
	}

	db := pg.Connect(&pg.Options{
		Addr:     addr,
		User:     c.DbUser,
		Password: c.DbPassword,
		Database: dbName,
	})

	if *showsql {
		db.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
			query, err := event.FormattedQuery()
			if err != nil {
				log.Fatalln(err.Error())
			}
			log.Printf("%s %s", time.Since(event.StartTime), query)
		})
	}
	return db
}

func (c *Config) GetRedisDB() *redis.Client {
	var dbNum int64
	if c.IsTesting {
		dbNum = c.RedisDbTest
	} else {
		dbNum = c.RedisDb
	}

	host := "127.0.0.1"
	if c.IsProduction {
		host = c.ServerStorageIp
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:6379", host),
		Password: "",
		DB:       dbNum,
	})
	return rdb
}

// Used only for testing and tooling
func (c *Config) GetGormDB() *gorm.DB {
	host := "127.0.0.1"
	if c.IsProduction {
		host = c.ServerStorageIp
	}

	cstring := fmt.Sprintf(
		"host=%s user=%s dbname=%s sslmode=disable password=%s",
		host,
		c.DbUser,
		c.DbTestName,
		c.DbPassword,
	)
	db, err := gorm.Open("postgres", cstring)
	if err != nil {
		log.Fatalln(err)
	}
	return db
}

func (c *Config) Now() time.Time {
	return time.Now().UTC()
}

func (c *Config) NowString() string {
	return c.Now().Format("2006-01-02 15:04:05")
}

func (c *Config) BaseDir() string {
	if c.IsProduction {
		return "/home/vlad/screent/"
	}
	return "/home/vlad/dev/web/projects/screent/project/screent_tcp/src/screent/"
}

func (c *Config) GetRedisKeyComputerConfig(token string) string {
	return c.RedisKeyComputerConfig + token
}

func (c *Config) GetRedisKeyComputerConfigSnapshot(token string) string {
	return c.RedisKeyComputerConfigSnapshot + token
}

func (c *Config) GetRedisKeyComputer(token string) string {
	return c.RedisKeyComputer + token
}

func (c *Config) GetRedisKeyUserToken(token string) string {
	return c.RedisKeyUserToken + token
}

// Send alert via Slack.
func (c *Config) SendAlert(text string) {
	var jsonStr = []byte(`{"text":"` + text + `"}`)
	req, err := http.NewRequest("POST", c.SlackAlertsUrl, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	if err != nil {
		fmt.Println("TODO: add to log")
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("TODO: add to log")
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("TODO: add to log")
	}
}

// Clean testing database before each tests run.
func (c *Config) CleanDB() {
	if !c.IsTesting {
		return
	}

	db := c.GetDB()
	q := "SELECT table_name as name FROM information_schema.tables WHERE table_schema='public'"

	var res []struct {
		Name string
	}
	db.Query(&res, q)

	for _, obj := range res {
		var x struct {
			Count int
		}
		query := fmt.Sprintf("select count(*) as count from %s", obj.Name)
		db.QueryOne(&x, query)

		if x.Count > 0 {
			q := fmt.Sprintf("TRUNCATE \"%s\" CASCADE", obj.Name)
			_, err := db.Exec(q)
			if err != nil {
				log.Fatalln("Can not clean test database.")
			}
		}
	}
}
