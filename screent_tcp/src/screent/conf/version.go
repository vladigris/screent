package conf

type Version struct {
	Name           string
	CryptKey       string
	Checksum       string
	Folder         string
	WorkerFilename string
}
