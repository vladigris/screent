package main_test

import (
	"bytes"
	"fmt"
	"net"
	"screent/files"
	"screent/tcpserver"
	"strings"
	"testing"

	. "github.com/franela/goblin"
)

func TestTCP(t *testing.T) {
	config.CleanDB()
	g := Goblin(t)

	// Strip <response> and </response> tags.
	fnStrip := func(s string) string {
		s = strings.Replace(s, "<response>", "", -1)
		s = strings.Replace(s, "</response>", "", -1)
		return s
	}

	computer := new(ComputerFactory).Create(nil)

	g.Describe("TCP commands", func() {

		g.It(`Should parse "getConfig" command`, func() {
			data := ` 
				<root>
					<command>getConfig</command>
					<data>
						<token>some token</token>
					</data>
				</root>
			`
			s := tcpserver.NewServer()
			cmd, err := s.ParseCommand([]byte(data))
			g.Assert(err == nil).IsTrue()
			g.Assert(cmd.Name).Equal("getConfig")
			g.Assert(cmd.Token).Equal("some token")

		})

		g.It(`Should parse "getConfigSnapshot" command`, func() {
			data := ` 
				<root>
					<command>getConfigSnapshot</command>
					<data>
						<token>some token</token>
					</data>
				</root>
			`
			s := tcpserver.NewServer()
			cmd, err := s.ParseCommand([]byte(data))
			g.Assert(err == nil).IsTrue()
			g.Assert(cmd.Name).Equal("getConfigSnapshot")
			g.Assert(cmd.Token).Equal("some token")
		})

		g.It(`Should parse "sendFile" command`, func() {
			data := ` 
				<root>
					<command>sendFile</command>
					<data>
						<token>some token</token>
						<type>some type</type>
						<fileSize>1000</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			s := tcpserver.NewServer()
			cmd, err := s.ParseCommand([]byte(data))
			g.Assert(err == nil).IsTrue()
			g.Assert(cmd.Name).Equal("sendFile")
			g.Assert(cmd.Token).Equal("some token")
			g.Assert(cmd.Type).Equal("some type")
			g.Assert(cmd.FileSize).Equal(1000)
			g.Assert(cmd.IsTest).Equal(true)
		})
	})

	g.Describe("TCP server", func() {

		g.It(`Should get config`, func() {
			s := tcpserver.NewServer()

			data := `
				<root>
					<command>getConfig</command>
					<data>
						<token>%s</token>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token)

			server, client := net.Pipe()
			go func() {
				s.HandleRequest(server)
				server.Close()
			}()

			_, err := client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			result := make([]byte, 5000)
			_, err = client.Read(result)
			g.Assert(err == nil).IsTrue()

			result = bytes.Trim(result, "\x00")
			configString, err := computer.DecryptString(fnStrip(string(result)))
			g.Assert(err).Equal(nil)
			g.Assert(len(configString) > 0).IsTrue()
			client.Close()
		})

		g.It(`Should get config snapshot`, func() {
			s := tcpserver.NewServer()

			data := `
				<root>
					<command>getConfigSnapshot</command>
					<data>
						<token>%s</token>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token)

			server, client := net.Pipe()
			go func() {
				s.HandleRequest(server)
				server.Close()
			}()

			_, err := client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			result := make([]byte, 5000)
			_, err = client.Read(result)
			g.Assert(err == nil).IsTrue()

			result = bytes.Trim(result, "\x00")
			g.Assert(fnStrip(string(result))).Equal(computer.Snapshot)

			client.Close()
		})

		g.It(`Should send screenshot file`, func() {
			s := tcpserver.NewServer()

			b, err := files.Asset("files/pics/example.jpg")
			checkErr(err)

			fileInfo := `
				<screen>
					<time>%s</time>
					<title>Some title</title>
					<program>Some program</program>
				</screen>
			`
			fileInfo = fmt.Sprintf(fileInfo, config.Now().Format("2006-01-02 15:04:05"))

			// File info is saved to file.
			b, err = computer.EncryptFile(b, fileInfo)
			g.Assert(err).Equal(nil)

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>%s</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token, len(b))

			server, client := net.Pipe()
			go func() {
				s.HandleRequest(server)
				server.Close()
			}()

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// First OK message
			buf := make([]byte, 100)
			num, err := client.Read(buf)
			checkErr(err)

			message := string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			// Client sends file to server
			s.SendFile(client, b)

			// Final OK message
			buf = make([]byte, 100)
			num, err = client.Read(buf)
			checkErr(err)

			message = string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			client.Close()

			// TODO: check that screen in Redis and in Static storage
		})

		g.It(`Should send text file`, func() {
			s := tcpserver.NewServer()

			fileContent := `
				<keys>
					<time>%s</time>
					<text>Some keyboard text 1</text>
				</keys>
				<keys>
					<time>%s</time>
					<text>Some keyboard text 2</text>
				</keys>
			`
			fileContent = fmt.Sprintf(
				fileContent,
				"2017-05-02 12:00:00",
				"2017-05-02 12:01:00",
			)

			b, err := computer.Encrypt([]byte(fileContent))
			g.Assert(err).Equal(nil)

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>%s</token>
						<type>text</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token, len(b))

			server, client := net.Pipe()
			go func() {
				s.HandleRequest(server)
				server.Close()
			}()

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// First OK message
			buf := make([]byte, 100)
			num, err := client.Read(buf)
			checkErr(err)

			message := string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			// Client sends file to server
			s.SendFile(client, b)

			// Final OK message
			buf = make([]byte, 100)
			num, err = client.Read(buf)
			checkErr(err)

			message = string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			client.Close()

			// TODO: check that info in Redis
		})

		g.It(`Should not send screenshot file without info`, func() {
			s := tcpserver.NewServer()

			b, err := files.Asset("files/pics/example.jpg")
			checkErr(err)

			b, err = computer.Encrypt(b)
			g.Assert(err).Equal(nil)

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>%s</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token, len(b))

			server, client := net.Pipe()
			go func() {
				s.HandleRequest(server)
				server.Close()
			}()

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// First OK message
			buf := make([]byte, 100)
			num, err := client.Read(buf)
			checkErr(err)

			message := string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			// Client sends file to server
			s.SendFile(client, b)

			// Final OK message even when error
			buf = make([]byte, 100)
			num, err = client.Read(buf)
			checkErr(err)

			message = string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			client.Close()

		})

		g.It(`Should not send screenshot file with incorrect info`, func() {
			s := tcpserver.NewServer()

			b, err := files.Asset("files/pics/example.jpg")
			checkErr(err)

			fileInfo := `
				<screen>
				</screen>
			`
			fileInfo = fmt.Sprintf(fileInfo, config.Now().Format("2006-01-02 15:04:05"))

			// File info is saved to file.
			b, err = computer.EncryptFile(b, fileInfo)
			g.Assert(err).Equal(nil)

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>%s</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token, len(b))

			server, client := net.Pipe()
			go func() {
				s.HandleRequest(server)
				server.Close()
			}()

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// First OK message
			buf := make([]byte, 100)
			num, err := client.Read(buf)
			checkErr(err)

			message := string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			// Client sends file to server
			s.SendFile(client, b)

			// Final OK message even when error
			buf = make([]byte, 100)
			num, err = client.Read(buf)
			checkErr(err)

			message = string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			client.Close()
		})

		g.It(`Should not send screenshot file with incorrect bytes`, func() {
			s := tcpserver.NewServer()

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>%s</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			b := []byte("incorrect file less than 2000 bytes")
			data = fmt.Sprintf(data, computer.Token, len(b))

			server, client := net.Pipe()
			go func() {
				s.HandleRequest(server)
				server.Close()
			}()

			// Send command
			_, err := client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// First OK message
			buf := make([]byte, 100)
			num, err := client.Read(buf)
			checkErr(err)

			message := string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			// Client sends file to server
			s.SendFile(client, b)

			// Final OK message even when error
			buf = make([]byte, 100)
			num, err = client.Read(buf)
			checkErr(err)

			message = string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			client.Close()
		})

		g.It(`Should not send screenshot file with incorrect token`, func() {
			s := tcpserver.NewServer()

			b, err := files.Asset("files/pics/example.jpg")
			checkErr(err)

			fileInfo := `
				<screen>
					<time>%s</time>
					<title>Some title</title>
					<program>Some program</program>
				</screen>
			`
			fileInfo = fmt.Sprintf(fileInfo, config.Now().Format("2006-01-02 15:04:05"))

			// File info is saved to file.
			b, err = computer.EncryptFile(b, fileInfo)
			g.Assert(err).Equal(nil)

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>incorrect</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, len(b))

			server, client := net.Pipe()
			go func() {
				s.HandleRequest(server)
				server.Close()
			}()

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// Server should close connection
			buf := make([]byte, 100)
			_, err = client.Read(buf)
			g.Assert(err != nil).IsTrue()
		})

		g.It(`Debug`, func() {
			v1 := rdb.LLen(config.RedisKeyLogData)
			v2 := rdb.LLen(config.RedisKeyKeyboardData)
			v3 := rdb.LLen(config.RedisKeyScreenData)

			fmt.Println(v1)
			fmt.Println(v2)
			fmt.Println(v3)
		})
	})
}
