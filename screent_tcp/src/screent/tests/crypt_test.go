package main_test

import (
	"screent/helpers"
	m "screent/models"
	"testing"

	. "github.com/franela/goblin"
)

func TestCrypt(t *testing.T) {
	config.CleanDB()
	g := Goblin(t)

	new(VersionFactory).Create()
	cryptKey := "qwertyuiopasdfgh"

	g.Describe("Encryption", func() {

		g.It("Helpers: should encrypt/decrypt bytes", func() {

			bytes := []byte("Some test bytes")
			encBytes, err := helpers.Encrypt(bytes, cryptKey)
			g.Assert(err).Equal(nil)
			decBytes, err := helpers.Decrypt(encBytes, cryptKey)
			g.Assert(err).Equal(nil)
			g.Assert(decBytes).Equal(bytes)
		})

		g.It("ComputerManager: should encrypt/decrypt bytes", func() {
			bytes := []byte("Some test bytes")

			cm := &m.ComputerManager{}
			encBytes, err := cm.Encrypt(bytes, cryptKey)
			g.Assert(err).Equal(nil)
			decBytes, err := cm.Decrypt(encBytes, cryptKey)
			g.Assert(err).Equal(nil)
			g.Assert(decBytes).Equal(bytes)
		})

		g.It("ComputerManager: should encrypt/decrypt string", func() {
			s := "Some test string"

			cm := &m.ComputerManager{}
			encString, err := cm.EncryptString(s, cryptKey)
			g.Assert(err).Equal(nil)
			decString, err := cm.DecryptString(encString, cryptKey)
			g.Assert(err).Equal(nil)
			g.Assert(decString).Equal(s)
		})

		g.It("ComputerManager: should encrypt/decrypt file", func() {
			bytes := []byte("Some file bytes")
			info := "Some info about file"

			cm := &m.ComputerManager{}
			encBytes, err := cm.EncryptFile(bytes, info, cryptKey)
			g.Assert(err).Equal(nil)
			decBytes, decInfo, err := cm.DecryptFile(encBytes, cryptKey)
			g.Assert(err).Equal(nil)
			g.Assert(decBytes).Equal(bytes)
			g.Assert(decInfo).Equal(info)
		})

		g.It("Computer: should encrypt/decrypt bytes", func() {
			bytes := []byte("Some test bytes")

			c := &m.Computer{}
			encBytes, err := c.Encrypt(bytes)
			g.Assert(err).Equal(nil)
			decBytes, err := c.Decrypt(encBytes)
			g.Assert(err).Equal(nil)
			g.Assert(decBytes).Equal(bytes)
		})

		g.It("Computer: should encrypt/decrypt string", func() {
			s := "Some test string"

			c := &m.Computer{}
			encString, err := c.EncryptString(s)
			g.Assert(err).Equal(nil)
			decString, err := c.DecryptString(encString)
			g.Assert(err).Equal(nil)
			g.Assert(decString).Equal(s)
		})

		g.It("Computer: should encrypt/decrypt file", func() {
			bytes := []byte("Some file bytes")
			info := "Some info about file"

			c := &m.Computer{}
			encBytes, err := c.EncryptFile(bytes, info)
			g.Assert(err).Equal(nil)
			decBytes, decInfo, err := c.DecryptFile(encBytes)
			g.Assert(err).Equal(nil)
			g.Assert(decBytes).Equal(bytes)
			g.Assert(decInfo).Equal(info)
		})
	})
}
