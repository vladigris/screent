package main_test

import (
	"fmt"
	"log"
	m "screent/models"
)

type Result struct {
	status int
	json   string
	result map[string]interface{}
	detail string
}

func NewResult(status int, json string, result map[string]interface{}, detail string) *Result {
	return &Result{
		status: status,
		json:   json,
		result: result,
		detail: detail,
	}
}

func getUser() *m.User {
	return new(UserFactory).Create("11111111")
}
func checkErr(err error) {
	if err != nil {
		fmt.Println(err)
		log.Fatalln(err.Error())
	}
}
