// Tests that call remote server

package main_test

import (
	"bytes"
	"fmt"
	"net"
	"screent/files"
	m "screent/models"
	"screent/tcpserver"
	"strings"
	"testing"

	. "github.com/franela/goblin"
)

func TestRemote(t *testing.T) {
	g := Goblin(t)
	config.CleanDB()

	g.Describe("TCP server", func() {
		getComputer := func() *m.Computer {
			computer := new(ComputerFactory).Create(nil)
			computer.Token = "testtoken"
			computer.Save()
			return computer
		}

		// Strip <response> and </response> tags.
		fnStrip := func(s string) string {
			s = strings.Replace(s, "<response>", "", -1)
			s = strings.Replace(s, "</response>", "", -1)
			return s
		}

		tcpString := fmt.Sprintf("%s:%d", config.ServerAppIp, config.TcpPort)
		computer := getComputer()

		g.It(`Should get config`, func() {

			data := ` 
				<root>
					<command>getConfig</command>
					<data>
						<token>%s</token>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token)

			client, err := net.Dial("tcp", tcpString)
			checkErr(err)

			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			result := make([]byte, 5000)
			_, err = client.Read(result)
			g.Assert(err == nil).IsTrue()

			result = bytes.Trim(result, "\x00")
			configString, err := computer.DecryptString(fnStrip(string(result)))
			g.Assert(err).Equal(nil)
			g.Assert(len(configString) > 0).IsTrue()
			client.Close()
		})

		g.It(`Should get config snapshot`, func() {
			data := `
				<root>
					<command>getConfigSnapshot</command>
					<data>
						<token>%s</token>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token)

			client, err := net.Dial("tcp", tcpString)
			checkErr(err)

			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			result := make([]byte, 5000)
			_, err = client.Read(result)
			g.Assert(err == nil).IsTrue()

			result = bytes.Trim(result, "\x00")

			strResult := fnStrip(string(result))
			g.Assert(len(strResult)).Equal(32)
			client.Close()
		})

		g.It(`Should send screenshot file`, func() {
			s := tcpserver.NewServer()
			b, err := files.Asset("files/pics/example.jpg")
			checkErr(err)

			fileInfo := `
				<screen>
					<time>%s</time>
					<title>Some title</title>
					<program>Some program</program>
				</screen>
			`
			fileInfo = fmt.Sprintf(fileInfo, config.Now().Format("2006-01-02 15:04:05"))

			// File info is saved to file.
			b, err = computer.EncryptFile(b, fileInfo)
			g.Assert(err).Equal(nil)

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>%s</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token, len(b))

			client, err := net.Dial("tcp", tcpString)
			checkErr(err)

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// First OK message
			buf := make([]byte, 100)
			num, err := client.Read(buf)
			checkErr(err)

			message := string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			// Client sends file to server
			s.SendFile(client, b)

			// Final OK message
			buf = make([]byte, 100)
			num, err = client.Read(buf)
			checkErr(err)

			message = string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			client.Close()

		})

		g.It(`Should not send screenshot file without info`, func() {
			s := tcpserver.NewServer()
			b, err := files.Asset("files/pics/example.jpg")
			checkErr(err)

			// File info is saved to file.
			b, err = computer.Encrypt(b)
			g.Assert(err).Equal(nil)

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>%s</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token, len(b))

			client, err := net.Dial("tcp", tcpString)
			checkErr(err)

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// First OK message
			buf := make([]byte, 100)
			num, err := client.Read(buf)
			checkErr(err)

			message := string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			// Client sends file to server
			s.SendFile(client, b)

			// Final OK message when error.
			buf = make([]byte, 100)
			num, err = client.Read(buf)
			checkErr(err)

			message = string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)
			client.Close()

		})

		g.It(`Should not send screenshot file with incorrect info`, func() {
			s := tcpserver.NewServer()
			b, err := files.Asset("files/pics/example.jpg")
			checkErr(err)

			fileInfo := `
				<screen>
				</screen>
			`
			fileInfo = fmt.Sprintf(fileInfo, config.Now().Format("2006-01-02 15:04:05"))

			// File info is saved to file.
			b, err = computer.EncryptFile(b, fileInfo)
			g.Assert(err).Equal(nil)

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>%s</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, computer.Token, len(b))

			client, err := net.Dial("tcp", tcpString)
			checkErr(err)

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// First OK message
			buf := make([]byte, 100)
			num, err := client.Read(buf)
			checkErr(err)

			message := string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			// Client sends file to server
			s.SendFile(client, b)

			// Final OK message when error
			buf = make([]byte, 100)
			num, err = client.Read(buf)
			checkErr(err)

			message = string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			client.Close()

		})

		g.It(`Should not send screenshot file with incorrect bytes`, func() {
			s := tcpserver.NewServer()

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>%s</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			b := []byte("incorrect file less than 2000 bytes")
			data = fmt.Sprintf(data, computer.Token, len(b))

			client, err := net.Dial("tcp", tcpString)
			checkErr(err)

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// First OK message
			buf := make([]byte, 100)
			num, err := client.Read(buf)
			checkErr(err)

			message := string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			// Client sends file to server
			s.SendFile(client, b)

			// Final OK message when error
			buf = make([]byte, 100)
			num, err = client.Read(buf)
			checkErr(err)

			message = string(buf[:num])
			g.Assert(fnStrip(message)).Equal(s.OkMessage)

			client.Close()

		})

		g.It(`Should not send screenshot file with incorrect token`, func() {
			b, err := files.Asset("files/pics/example.jpg")
			checkErr(err)

			fileInfo := `
				<screen>
					<time>%s</time>
					<title>Some title</title>
					<program>Some program</program>
				</screen>
			`
			fileInfo = fmt.Sprintf(fileInfo, config.Now().Format("2006-01-02 15:04:05"))

			// File info is saved to file.
			b, err = computer.EncryptFile(b, fileInfo)
			g.Assert(err).Equal(nil)

			data := `
				<root>
					<command>sendFile</command>
					<data>
						<token>incorrect</token>
						<type>image</type>
						<fileSize>%d</fileSize>
						<isTest>true</isTest>
					</data>
				</root>
			`
			data = fmt.Sprintf(data, len(b))

			client, err := net.Dial("tcp", tcpString)
			checkErr(err)

			// Send command
			_, err = client.Write([]byte(data))
			g.Assert(err == nil).IsTrue()

			// Server should close connection
			buf := make([]byte, 100)
			_, err = client.Read(buf)
			g.Assert(err != nil).IsTrue()
		})

	})
}
