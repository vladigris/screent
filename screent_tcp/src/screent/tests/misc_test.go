package main_test

import (
	"encoding/base64"
	"fmt"
	"regexp"
	"screent/helpers"
	"testing"

	. "github.com/franela/goblin"
)

func DecryptString(text string, cryptKey string) (string, error) {
	bytes, err := base64.StdEncoding.DecodeString(text)
	if err != nil {
		return "", err
	}
	result, err := helpers.Decrypt(bytes, cryptKey)
	return string(result), err
}

func TestMisc(t *testing.T) {
	config.CleanDB()
	g := Goblin(t)

	g.Describe("Misc", func() {

		// user := new(UserFactory).Create("11111111")
		// computer := new(ComputerFactory).Create(nil)
		// fmt.Println(computer)
		// fmt.Println(computer.Version)
		// fmt.Println(computer.Version.CryptKey)

		// content, err := files.Asset("files/pics/example.jpg")
		// checkErr(err)

		// st := tcpserver.NewStorage("example3.jpg", content, "image/jpeg", true)
		// err = st.UploadFile()

		// if err == nil {
		// 	fmt.Println("File uploaded")
		// } else {
		// 	fmt.Println(err)
		// }

		g.It("Should", func() {
			s := ` 
				<keys>
					<time>2017-05-02 12:00:00</time>
					<text>Some keyboard text 1</text>
				</keys>
				<keys>
					<time>2017-05-02 12:01:00</time>
					<text>Some keyboard text 2</text>
				</keys>
`
			rx := regexp.MustCompile(`(?s)<keys>.*?</keys>`)

			list := rx.FindAllString(s, -1)
			fmt.Println(len(list))

		})
	})
}
