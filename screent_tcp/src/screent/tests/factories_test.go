package main_test

import (
	"reflect"
	"screent/helpers"
	m "screent/models"
	"strconv"
	"strings"

	"github.com/icrowley/fake"
)

type UserFactory struct{}

func (uf *UserFactory) Create(password string) *m.User {
	user := &m.User{}
	fillFields(user)

	user.Email = strings.ToLower(fake.EmailAddress())
	user.IsActive = true
	user.Password = "11111111"

	err := user.Save()
	checkErr(err)
	return user
}

type VersionFactory struct{}

func (vf *VersionFactory) Create() *m.Version {
	version := &m.Version{Name: "1.0", IsActive: true}
	version.Guid = helpers.GUID()
	version.CreatedAt = config.Now()
	err := version.Save()
	checkErr(err)
	return version
}

type ComputerFactory struct{}

func (cf *ComputerFactory) Create(user *m.User) *m.Computer {
	if user == nil {
		user = new(UserFactory).Create("111")
	}
	computer := &m.Computer{}
	fillFields(computer)

	version := &m.Version{}
	gdb.First(version)
	if version.Id == 0 {
		version = new(VersionFactory).Create()
	}

	computer.Name = fake.DigitsN(10)
	computer.Version = version
	computer.VersionId = version.Id
	computer.UserId = user.Id
	computer.User = user
	computer.IsActive = true
	computer.Token = fake.DigitsN(15)
	err := computer.Save()
	checkErr(err)
	return computer
}

// Fills string, int, float64 with fake data.
// bool set to false and time set to Now.
func fillFields(v interface{}) {
	s := reflect.ValueOf(v).Elem()

	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)

		switch x := f.Type().String(); x {
		case "string":
			f.SetString(fake.CharactersN(10))
		case "bool":
			f.SetBool(false)
		case "float64":
			valInt1, _ := strconv.Atoi(fake.DigitsN(2))
			f.SetFloat(float64(valInt1))
		case "int":
			valInt2, _ := strconv.Atoi(fake.DigitsN(2))
			f.SetInt(int64(valInt2))
		case "time.Time":
			f.Set(reflect.ValueOf(config.Now()))
		}
	}

	f := s.FieldByName("Id")
	f.SetInt(0)

	f = s.FieldByName("Guid")
	f.SetString(helpers.GUID())
}
