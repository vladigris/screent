package main_test

import (
	"os"
	"screent/conf"
	"testing"

	redis "gopkg.in/redis.v3"

	"github.com/jinzhu/gorm"
)

var (
	gdb    *gorm.DB
	rdb    *redis.Client
	config *conf.Config
)

func init() {
	config = conf.NewConfig()
	rdb = config.GetRedisDB()
}

func TestMain(tm *testing.M) {
	config.CleanDB()
	gdb = config.GetGormDB()

	code := tm.Run()
	os.Exit(code)
}

// param := make(url.Values)
// param["some"] = "some"
// param.Encode()
