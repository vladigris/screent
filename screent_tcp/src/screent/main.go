package main

import (
	"flag"
	"screent/tcpserver"
)

var (
	server = flag.String("server", "tcp", "Run tcp server")
)

func main() {
	flag.Parse()
	if *server == "tcp" {
		startTcp()
	} else {
		panic("Incorrect server argument")
	}
}

func startTcp() {
	s := tcpserver.NewServer()
	s.Start()
}
