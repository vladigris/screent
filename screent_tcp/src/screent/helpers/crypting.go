package helpers

import (
	"crypto/aes"
	"crypto/cipher"
	"errors"
)

// Generate random IV (16 bytes) and include it
// at the begin of result.
func Encrypt(bytes []byte, key string) ([]byte, error) {
	const k = 16
	iv := RandString(k)

	ciphertext, err := pkcs7Pad(bytes, k)
	if err != nil {
		return nil, err
	}

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return nil, err
	}

	cbc := cipher.NewCBCEncrypter(block, []byte(iv))
	cbc.CryptBlocks(ciphertext, ciphertext)

	encBytes := []byte{}
	encBytes = append(encBytes, []byte(iv)...)
	encBytes = append(encBytes, ciphertext...)
	return encBytes, nil
}

// First 16 bytes is key IV, the rest is crypted bytes
func Decrypt(bytes []byte, key string) ([]byte, error) {
	const k = 16

	if len(bytes) < aes.BlockSize {
		return nil, errors.New("The IV key is not added to bytes")
	}

	iv := bytes[:k]
	cipherbytes := bytes[k:]

	if len(cipherbytes)%aes.BlockSize != 0 {
		return nil, errors.New("ciphertext is not a multiple of the block size")
	}

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return nil, err
	}

	cbc := cipher.NewCBCDecrypter(block, iv)
	cbc.CryptBlocks(cipherbytes, cipherbytes)

	cipherbytes, err = pkcs7Unpad(cipherbytes, k)
	if err != nil {
		return nil, err
	}
	return cipherbytes, err
}
