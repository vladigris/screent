package tcpserver

import (
	"fmt"
	"log"
	"screent/conf"

	"github.com/go-pg/pg"
	"gopkg.in/redis.v3"
)

const (
	DEBUG   = "DEBUG"
	INFO    = "INFO"
	WARNING = "WARNING"
	ERROR   = "ERROR"
)

var (
	db     *pg.DB
	rdb    *redis.Client
	config *conf.Config
)

func init() {
	config = conf.NewConfig()
	db = config.GetDB()
	rdb = config.GetRedisDB()
}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err.Error())
		log.SetFlags(log.Llongfile | log.Ldate | log.Ltime)
		log.Fatalln(err.Error())
	}
}
