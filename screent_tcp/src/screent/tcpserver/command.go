package tcpserver

import (
	"encoding/xml"
)

type Command struct {
	XMLName  xml.Name `xml:"root"`
	Name     string   `xml:"command"`
	Token    string   `xml:"data>token"`
	Type     string   `xml:"data>type"`
	FileSize int      `xml:"data>fileSize"`
	IsTest   bool     `xml:"data>isTest"`

	// Set later from Redis by Token
	ComputerId   string
	ComputerGuid string
}
