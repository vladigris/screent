package tcpserver

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"time"
)

type ScreenInfo struct {
	XMLName xml.Name `xml:"screen"`
	Time    string   `xml:"time" json:"time"`
	Title   string   `xml:"title" json:"title"`
	Program string   `xml:"program" json:"program"`

	// Set later
	ComputerId   int    `json:"computerId"`
	ComputerGuid string `json:"computerGuid"`
	Guid         string `json:"guid"`
	FileSize     int    `json:"fileSize"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
}

// All xml fields should not be empty
func (si *ScreenInfo) Validate() error {
	_, err := time.Parse("2006-01-02 15:04:05", si.Time)
	if err != nil {
		return err
	}
	if si.Title == "" {
		return errors.New("The title is empty")
	}
	if si.Program == "" {
		return errors.New("The program is empty")
	}
	return nil
}

// Saves file bytes to Spaces
func (si *ScreenInfo) SaveToSpaces(b []byte) error {
	key := ""
	if config.IsTesting {
		key = "test/"
	}

	key += fmt.Sprintf("%s/%s.jpg", si.ComputerGuid, si.Guid)
	st := NewStorage(key, b, "image/jpeg", true)
	err := st.UploadFile()
	return err
}

func (si *ScreenInfo) Save() {
	data, _ := json.Marshal(si)
	key := config.RedisKeyScreenData
	rdb.LPush(key, string(data))
}
