package tcpserver

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"image/jpeg"
	"net"
	"regexp"
	"screent/helpers"
	m "screent/models"
)

// In many error cases during sending files, server
// responds with "ok", this is signal to client, that
// file can be deleted on client.
type Server struct {
	Host         string
	ConnType     string
	Port         int
	BufferSize   int
	ErrorMessage string
	OkMessage    string
}

func NewServer() *Server {
	server := &Server{
		// To listen on all interfaces do not set host
		Host:         "",
		ConnType:     "tcp",
		Port:         config.TcpPort,
		BufferSize:   config.TcpBufferSize,
		ErrorMessage: "error",
		OkMessage:    "ok",
	}
	return server
}

func (s *Server) Start() {
	connString := fmt.Sprintf("%s:%d", s.Host, s.Port)
	l, err := net.Listen(s.ConnType, connString)
	checkErr(err)
	defer l.Close()

	AddLog(INFO, fmt.Sprintf("Listening on %s:%d", s.Host, s.Port), "")
	for {
		conn, err := l.Accept()
		checkErr(err)
		go s.HandleRequest(conn)
	}
}

func (s *Server) ParseCommand(b []byte) (*Command, error) {
	cmd := &Command{}
	if err := xml.Unmarshal(b, cmd); err != nil {
		return nil, err
	}
	return cmd, nil
}

// Returns bytes: <response>...</response>
func (s *Server) CreateResponse(text string) []byte {
	result := fmt.Sprintf("<response>%s</response>", text)
	return []byte(result)
}

func (s *Server) HandleRequest(conn net.Conn) {
	defer conn.Close()

	buf := make([]byte, s.BufferSize)
	// reqLen, err := conn.Read(buf)
	_, err := conn.Read(buf)
	if err != nil {
		AddLog(ERROR, "Error reading buffer", "")
		conn.Close()
		return
	}

	cmd, err := s.ParseCommand(buf)
	if err != nil {
		AddLog(ERROR, err.Error(), "")
		conn.Close()
		return
	}

	switch cmd.Name {
	case "getConfig":
		s.ProcessGetConfig(conn, cmd)
	case "getConfigSnapshot":
		s.ProcessGetConfigSnapshot(conn, cmd)
	case "sendFile":
		s.ProcessSendFile(conn, cmd)
	}
}

func (s *Server) ProcessGetConfig(conn net.Conn, cmd *Command) {
	encConfig, err := new(m.ComputerManager).GetConfigByToken(cmd.Token)
	if err != nil {
		AddLog(ERROR, err.Error(), "")
		conn.Close()
		return
	}
	AddLog(DEBUG, "ProcessGetConfig success.", "")
	conn.Write(s.CreateResponse(encConfig))
	conn.Close()
}

func (s *Server) ProcessGetConfigSnapshot(conn net.Conn, cmd *Command) {
	snapshot, err := new(m.ComputerManager).GetConfigSnapshotByToken(cmd.Token)
	if err != nil {
		AddLog(ERROR, err.Error(), "")
		conn.Close()
		return
	}
	AddLog(DEBUG, "ProcessGetConfigSnapshot success.", "")
	conn.Write(s.CreateResponse(snapshot))
	conn.Close()
}

func (s *Server) ProcessSendFile(conn net.Conn, cmd *Command) {
	cig, err := new(m.ComputerManager).GetIdGuidByToken(cmd.Token)
	if err != nil {
		AddLog(ERROR, err.Error(), "")
		// No OK message here
		conn.Close()
		return
	}

	// Step 1. Respond with "ok"
	conn.Write(s.CreateResponse(s.OkMessage))

	// Start to receive file by chunks
	myBytes := []byte{}
	received := 0

	for received < cmd.FileSize {
		var size int

		if (cmd.FileSize - received) > s.BufferSize {
			size = s.BufferSize
		} else {
			size = cmd.FileSize - received
		}

		buf := make([]byte, size)
		reqLen, err := conn.Read(buf)
		if err != nil {
			AddLog(ERROR, err.Error(), cig.Guid)
		}

		// There is no guarantee, that tcp package will be equal the size.
		// Sometimes number of bytes came by TCP are less than buffer size.
		buf = buf[:reqLen]

		received += reqLen
		myBytes = append(myBytes, buf...)
		// AddLog(DEBUG, fmt.Sprintf("Received chunk: %d %d", len(buf), len(myBytes)), cig.Guid)
	}

	// File received - process file by type.
	if cmd.Type == "text" {
		result, err := new(m.ComputerManager).Decrypt(myBytes, cig.CryptKey)
		if err != nil {
			AddLog(ERROR, err.Error(), cig.Guid)
			// Anyway respond with OK
			conn.Write(s.CreateResponse(s.OkMessage))
			conn.Close()
			return
		}

		rx := regexp.MustCompile(`(?s)<keys>.*?</keys>`)
		list := rx.FindAllString(string(result), -1)
		for _, str := range list {
			ki := &KeyboardInfo{}
			err := xml.Unmarshal([]byte(str), ki)
			if err != nil {
				AddLog(ERROR, err.Error(), cig.Guid)
				continue
			}
			ki.ComputerId = cig.Id
			ki.Save()
		}

		AddLog(DEBUG, fmt.Sprintf("ProcessSendFile (keys %d) success.", len(list)), cig.Guid)
		conn.Write(s.CreateResponse(s.OkMessage))
		conn.Close()

	} else if cmd.Type == "image" {

		fileBytes, fileInfoString, err := new(m.ComputerManager).DecryptFile(myBytes, cig.CryptKey)

		if err != nil {
			AddLog(ERROR, err.Error(), cig.Guid)
			// Anyway respond with OK
			conn.Write(s.CreateResponse(s.OkMessage))
			conn.Close()
			return
		}

		si := &ScreenInfo{}
		err = xml.Unmarshal([]byte(fileInfoString), si)
		if err != nil {
			AddLog(ERROR, err.Error(), cig.Guid)
			// Anyway respond with OK
			conn.Write(s.CreateResponse(s.OkMessage))
			conn.Close()
			return
		}

		err = si.Validate()
		if err != nil {
			AddLog(ERROR, err.Error(), cig.Guid)
			// Anyway respond with OK
			conn.Write(s.CreateResponse(s.OkMessage))
			conn.Close()
			return
		}

		si.ComputerId = cig.Id
		si.ComputerGuid = cig.Guid
		si.Guid = helpers.GUID()
		si.FileSize = len(fileBytes)

		width, height, err := s.GetImageSize(fileBytes)
		if err != nil {
			AddLog(ERROR, "Can not get Image size", cig.Guid)
			// Anyway respond with OK
			conn.Write(s.CreateResponse(s.OkMessage))
			conn.Close()
			return
		}

		si.Width = width
		si.Height = height
		err = si.SaveToSpaces(fileBytes)

		if err != nil {
			AddLog(ERROR, err.Error(), cig.Guid)
			// Anyway respond with OK
			conn.Write(s.CreateResponse(s.OkMessage))
			conn.Close()
			return
		}

		si.Save()

		AddLog(DEBUG, "Screenshot has been saved", cig.Guid)
		conn.Write(s.CreateResponse(s.OkMessage))
		conn.Close()
	}
}

func (s *Server) GetImageSize(b []byte) (int, int, error) {
	reader := bytes.NewReader(b)
	im, err := jpeg.Decode(reader)
	if err != nil {
		return 0, 0, err
	}
	g := im.Bounds()
	width := g.Dx()
	height := g.Dy()
	return width, height, err
}

// Used for testing
func (s *Server) SendFile(conn net.Conn, b []byte) {
	totalBytes := len(b)
	sentBytes := 0
	reader := bytes.NewReader(b)

	for sentBytes < totalBytes {
		var size int

		if (totalBytes - sentBytes) > s.BufferSize {
			size = s.BufferSize
		} else {
			size = totalBytes - sentBytes
		}

		buf := make([]byte, size)
		reqLen, err := reader.Read(buf)
		if err != nil {
			AddLog(ERROR, err.Error(), "")
		}

		sentBytes += reqLen
		_, err = conn.Write(buf)
		checkErr(err)
	}
}
