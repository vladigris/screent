package tcpserver

import (
	"encoding/json"
)

func AddLog(level, message, computerGuid string) {
	ld := &LogData{
		Level:        level,
		Message:      message,
		ComputerGuid: computerGuid,
		CreatedAt:    config.Now().Format("2006-01-01 15:04:05"),
	}
	ld.Save()
}

type LogData struct {
	Level        string `json:"level"`
	Message      string `json:"message"`
	ComputerGuid string `json:"computerGuid"`
	CreatedAt    string `json:"createdAt"`
}

func (ld *LogData) Save() {
	data, _ := json.Marshal(ld)
	key := config.RedisKeyLogData
	rdb.LPush(key, string(data))
}
