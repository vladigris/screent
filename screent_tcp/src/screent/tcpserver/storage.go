// DigitalOcean Spaces

package tcpserver

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"screent/awsauth"
)

// filename is full path bucket key
func NewStorage(filename string, content []byte, contentType string, isPublic bool) *Storage {
	return &Storage{
		Content:     content,
		Filename:    filename,
		ContentType: contentType,
		IsPublic:    isPublic,
	}
}

type Storage struct {
	Content     []byte
	Filename    string
	ContentType string
	IsPublic    bool
}

func (s *Storage) GetUrl() string {
	return fmt.Sprintf(
		"https://%s.%s.digitaloceanspaces.com/screens/%s",
		config.SpacesBucket,
		config.SpacesRegion,
		s.Filename,
	)
}

func (s *Storage) UploadFile() error {
	url := s.GetUrl()
	client := new(http.Client)

	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(s.Content))
	req.Header.Add("Content-Length", fmt.Sprintf("%d", len(s.Content)))
	req.Header.Add("Content-Type", s.ContentType)

	if s.IsPublic {
		req.Header.Add("x-amz-acl", "public-read")
	}

	awsauth.Sign4(req, awsauth.Credentials{
		AccessKeyID:     config.SpacesKey,
		SecretAccessKey: config.SpacesSecret,
	})

	// Look at signed headers
	// fmt.Println(req.Header)

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == 200 {
		return nil
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	return errors.New(string(body))
}
