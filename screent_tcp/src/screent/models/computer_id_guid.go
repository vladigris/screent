package models

import "fmt"

type ComputerIdGuid struct {
	Id       int
	Guid     string
	CryptKey string
}

func (cig *ComputerIdGuid) String() string {
	return fmt.Sprintf("CIG<Id:%d Guid:%s>", cig.Id, cig.Guid)
}
