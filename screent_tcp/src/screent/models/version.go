package models

import (
	"encoding/json"
	"time"
)

type VersionManager struct {
	Items []*Version `json:"items"`
}

func (vm *VersionManager) FromJSON(js string) []*Version {
	json.Unmarshal([]byte(js), vm)
	return vm.Items
}

func (v *Version) Save() error {
	if v.Id == 0 {
		if err := db.Insert(v); err != nil {
			return err
		}
	} else {
		if err := db.Update(v); err != nil {
			return err
		}
	}
	return nil
}

func (vm *VersionManager) ActiveVersion() *Version {
	v := &Version{}
	err := db.Model(v).Where("is_active IS TRUE").Select()
	checkErr(err)
	return v
}

type Version struct {
	Id        int       `json:"id"`
	Guid      string    `json:"guid"`
	Name      string    `json:"name"`
	IsActive  bool      `sql:",notnull" json:"isActive"`
	CreatedAt time.Time `json:"createdAt"`
}

func (v *Version) FromJSON(js string) *Version {
	json.Unmarshal([]byte(js), v)
	return v
}

func (v *Version) CryptKey() string {
	return config.Versions[v.Name].CryptKey
}

func (v *Version) Checksum() string {
	return config.Versions[v.Name].Checksum
}

func (v *Version) Folder() string {
	return config.Versions[v.Name].Folder
}

func (v *Version) WorkerFilename() string {
	return config.Versions[v.Name].WorkerFilename
}

func (v *Version) WorkerKey() string {
	return "programs/" + v.Name + "/" + v.Folder() + "/" + v.WorkerFilename()
}

func (v *Version) WorkerDownloadLink() string {
	return "https://" + config.SpacesBucket + "nyc3.digitaloceanspaces.com/" + v.WorkerKey()
}
