package models

import (
	"fmt"
	"log"
	"screent/conf"

	"gopkg.in/redis.v3"

	"github.com/go-pg/pg"
)

var (
	db     *pg.DB
	rdb    *redis.Client
	config *conf.Config
)

func init() {
	config = conf.NewConfig()
	db = config.GetDB()
	rdb = config.GetRedisDB()
}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err.Error())
		log.SetFlags(log.Llongfile | log.Ldate | log.Ltime)
		log.Fatalln(err.Error())
	}
}

func NewInternalError(message, detail string) *InternalError {
	return &InternalError{Message: message, Detail: detail}
}

type InternalError struct {
	Message string
	Detail  string
}

func (p *InternalError) Error() string {
	return fmt.Sprintf("InternalError<%s, %s>", p.Message, p.Detail)
}
