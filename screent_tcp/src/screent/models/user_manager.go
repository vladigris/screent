package models

import (
	"encoding/json"
	"screent/helpers"
	"strings"
	"time"
)

type UserManager struct {
	Items []*User `json:"items"`
}

func (um *UserManager) Create(email, password, firstName string) (*User, error) {
	user := &User{
		Email:        email,
		FirstName:    firstName,
		LastAccessAt: config.Now(),
		IsActive:     true,
		IsAdmin:      false,
		IsSuperuser:  false,
		JoinedAt:     config.Now(),
		Password:     "11111111",
	}

	user.Guid = helpers.GUID()
	if err := user.Save(); err != nil {
		return nil, NewInternalError("UserManager.Create", err.Error())
	}
	return user, nil
}

func (um *UserManager) FromJSON(js string) []*User {
	json.Unmarshal([]byte(js), um)
	return um.Items
}

func (um *UserManager) GetById(id int) *User {
	u := &User{}
	err := db.Model(u).
		Where("id = ?", id).
		Select()
	if err != nil {
		return nil
	}
	return u
}

func (um *UserManager) GetByGuid(guid string) *User {
	u := &User{}
	err := db.Model(u).
		Where("guid = ?", guid).
		Select()
	if err != nil {
		return nil
	}
	return u
}

func (um *UserManager) First() *User {
	u := &User{}
	db.Model(u).Order("created_at DESC").Limit(1).Select()
	return u
}

func (um *UserManager) Last() *User {
	u := &User{}
	db.Model(u).Order("created_at ASC").Limit(1).Select()
	return u
}

func (u *User) PreSave() {
}

func (u *User) PostSave() {
}

func (u *User) Delete() error {
	u.PreDelete()

	if err := db.Delete(u); err != nil {
		return err
	}

	u.PostDelete()
	return nil
}

func (u *User) PreDelete() {
}

func (u *User) PostDelete() {
}

func (u *User) RefreshFromDb() {
	updated := new(UserManager).GetById(u.Id)
	*u = *updated
}

func (u *User) FromJSON(js string) *User {
	json.Unmarshal([]byte(js), u)
	return u
}

func (um *UserManager) GetByEmail(email string) *User {
	email = strings.ToLower(email)
	user := &User{}
	db.Model(user).Where("email = ?", email).Select()
	if user.Id == 0 {
		return nil
	}
	return user
}

// First check in Redis, if there is no in Redis,
// set to Redis and return.
func (um *UserManager) GetUserByToken(token string) (*User, error) {
	tok := &Token{}

	key := config.GetRedisKeyUserToken(token)
	cmd := rdb.Get(key)
	result, err := cmd.Result()

	if err != nil {
		err := db.Model(&tok).
			Column("token.*", "User").
			Where("key = ?", token).
			Select()
		if err != nil {
			return nil, err
		}
		value, _ := json.Marshal(tok.User)
		rdb.Set(key, value, time.Duration(1)*time.Hour)
		return tok.User, nil
	}
	user := &User{}
	json.Unmarshal([]byte(result), user)
	return user, nil
}
