package models

import "encoding/xml"

type ComputerConfig struct {
	XMLName xml.Name `xml:"config"`

	Lang               string   `xml:"Lang"`
	Version            string   `xml:"Version"`
	WorkerCheksum      string   `xml:"WorkerChecksum"`
	WorkerDownloadLink string   `xml:"WorkerDownloadLink"`
	Ip                 string   `xml:"Ip"`
	Ips                []string `xml:"Ips>Ip"`
	Token              string   `xml:"Token"`
	WorkerName         string   `xml:"WorkerName"`
	WorkerDir          string   `xml:"WorkerDir"`
	WorkerConfName     string   `xml:"WorkerConfName"`
	ScreenDir          string   `xml:"ScreenDir"`
	LogDir             string   `xml:"LogDir"`
	Interval           int      `xml:"Interval"`
	DayLimit           int      `xml:"DayLimit"`
	DayLimitFileName   string   `xml:"DayLimitFileName"`
	Snapshot           string   `xml:"Snapshot"`
	IsActive           bool     `xml:"IsActive"`
	IsPause            bool     `xml:"IsPause"`
	IsRemoved          bool     `xml:"IsRemoved"`
}
