package models

import (
	"fmt"
	"time"
)

type User struct {
	Id           int       `json:"id"`
	Guid         string    `json:"guid"`
	Email        string    `validate:"nonzero" conform:"trim,lower" json:"email"`
	FirstName    string    `validate:"nonzero" conform:"trim" json:"firstName"`
	LastName     string    `conform:"trim" json:"lastName"`
	JoinedAt     time.Time `json:"joinedAt"`
	LastAccessAt time.Time `json:"lastAccessAt"`
	IsActive     bool      `sql:",notnull" json:"isActive"`
	IsAdmin      bool      `sql:",notnull" json:"isAdmin"`
	IsSuperuser  bool      `sql:",notnull" json:"isSuperuser"`
	SignupToken  string    `conform:"trim" json:"signupToken"`
	ResetToken   string    `conform:"trim" json:"resetToken"`
	Password     string    `conform:"trim" json:"password"`
}

func (u User) String() string {
	return fmt.Sprintf("User<%d %s>", u.Id, u.Email)
}

func (u *User) Save() error {
	if u.Id == 0 {
		if err := db.Insert(u); err != nil {
			return err
		}
	} else {
		if err := db.Update(u); err != nil {
			return err
		}
	}
	return nil
}
