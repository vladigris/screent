package models

import (
	"encoding/json"
	"screent/helpers"
	"time"
)

type TokenManager struct {
	Items []*Token `json:"items"`
}

func (tm *TokenManager) FromJSON(js string) []*Token {
	json.Unmarshal([]byte(js), tm)
	return tm.Items
}

func (tm *TokenManager) GetById(id int) *Token {
	t := &Token{}
	err := db.Model(t).
		Where("id = ?", id).
		Select()
	if err != nil {
		return nil
	}
	return t
}

func (tm *TokenManager) GetByGuid(guid string) *Token {
	t := &Token{}
	err := db.Model(t).
		Where("guid = ?", guid).
		Select()
	if err != nil {
		return nil
	}
	return t
}

func (tm *TokenManager) First() *Token {
	t := &Token{}
	db.Model(t).Order("created_at DESC").Limit(1).Select()
	return t
}

func (tm *TokenManager) Last() *Token {
	t := &Token{}
	db.Model(t).Order("created_at ASC").Limit(1).Select()
	return t
}

func (t *Token) Save() error {
	if t.Id == 0 {
		if err := db.Insert(t); err != nil {
			return err
		}
	} else {
		if err := db.Update(t); err != nil {
			return err
		}
	}
	return nil
}

func (tm *TokenManager) Create(userId int, expireMinutes int) (*Token, error) {
	token := &Token{
		Guid:      helpers.GUID(),
		UserId:    userId,
		Key:       helpers.GUID(),
		CreatedAt: config.Now(),
	}
	if expireMinutes > 0 {
		token.ExpireAt = config.Now().Add(time.Duration(expireMinutes) * time.Minute)
	}
	if err := token.Save(); err != nil {
		return nil, NewInternalError("Manager.Create", err.Error())
	}
	return token, nil
}

type Token struct {
	Id        int       `json:"id"`
	Guid      string    `json:"guid"`
	User      *User     `json:"user"`
	UserId    int       `json:"userId"`
	Key       string    `json:"key"`
	CreatedAt time.Time `json:"createdAt"`
	ExpireAt  time.Time `json:"expireAt"`
}

func (t *Token) CreatedAtString() string {
	return helpers.GetDateString(t.CreatedAt)
}

func (t *Token) ExpireAtString() string {
	return helpers.GetDateString(t.ExpireAt)
}
