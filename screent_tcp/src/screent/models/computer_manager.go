package models

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"screent/helpers"
	"time"
)

type ComputerManager struct {
	Items []*Computer `json:"items"`
}

func (cm *ComputerManager) FromJSON(js string) []*Computer {
	json.Unmarshal([]byte(js), cm)
	return cm.Items
}

func (cm *ComputerManager) GetById(id int) *Computer {
	c := &Computer{}
	err := db.Model(c).
		Column("computer.*", "User", "Version").
		Where("computer.id = ?", id).
		Select()
	if err != nil {
		return nil
	}
	return c
}

func (cm *ComputerManager) GetByGuid(guid string) *Computer {
	c := &Computer{}
	err := db.Model(c).
		Column("computer.*", "User", "Version").
		Where("computer.guid = ?", guid).
		Select()
	if err != nil {
		return nil
	}
	return c
}

func (cm *ComputerManager) GetByToken(token string) *Computer {
	c := &Computer{}
	err := db.Model(c).
		Column("computer.*", "User", "Version").
		Where("computer.token = ?", token).
		Select()
	if err != nil {
		return nil
	}
	return c
}

// First check in Redis, if there is no in Redis,
// set to Redis and return result.
// Returns encrypted config.
func (cm *ComputerManager) GetConfigByToken(token string) (string, error) {
	key := config.GetRedisKeyComputerConfig(token)
	cmd := rdb.Get(key)
	result, err := cmd.Result()
	if err != nil {
		computer := &Computer{}
		err := db.Model(computer).Where("token = ?", token).Select()
		if err != nil {
			return "", err
		}
		value := computer.EncryptedConfig()
		cm.SetToRedis(key, value)
		return value, nil
	}
	return result, nil
}

// First check in Redis, if there is no in Redis,
// set to Redis and return result.
// Returns hash of not encrypted config.
func (cm *ComputerManager) GetConfigSnapshotByToken(token string) (string, error) {
	key := config.GetRedisKeyComputerConfigSnapshot(token)
	cmd := rdb.Get(key)
	result, err := cmd.Result()
	if err != nil {
		computer := &Computer{}
		err := db.Model(computer).Where("token = ?", token).Select()
		if err != nil {
			return "", err
		}
		value := computer.Snapshot
		cm.SetToRedis(key, value)
		return value, nil
	}
	return result, nil
}

// First check in Redis, if there is no in Redis,
// set to Redis and return result.
func (cm *ComputerManager) GetIdGuidByToken(token string) (*ComputerIdGuid, error) {
	cig := &ComputerIdGuid{}

	key := config.GetRedisKeyComputer(token)
	cmd := rdb.Get(key)
	result, err := cmd.Result()

	c := &Computer{}
	if err != nil {
		err := db.Model(c).
			Column("computer.id", "computer.guid", "Version").
			Where("computer.token = ?", token).
			Select()
		if err != nil {
			return nil, err
		}

		cig.Id = c.Id
		cig.Guid = c.Guid
		cig.CryptKey = c.Version.CryptKey()

		value, err := json.Marshal(cig)
		if err != nil {
			return nil, err
		}
		cm.SetToRedis(key, value)
		return cig, nil
	}
	err = json.Unmarshal([]byte(result), cig)
	if err != nil {
		return nil, err
	}
	return cig, nil
}

func (cm *ComputerManager) SetToRedis(key string, value interface{}) {
	rdb.Set(key, value, time.Duration(3)*time.Hour)
}

func (cm *ComputerManager) Encrypt(bytes []byte, cryptKey string) ([]byte, error) {
	return helpers.Encrypt(bytes, cryptKey)
}

func (cm *ComputerManager) Decrypt(bytes []byte, cryptKey string) ([]byte, error) {
	return helpers.Decrypt(bytes, cryptKey)
}

// string -> crypted bytes -> base64
func (cm *ComputerManager) EncryptString(text string, cryptKey string) (string, error) {
	b, err := helpers.Encrypt([]byte(text), cryptKey)
	return base64.StdEncoding.EncodeToString(b), err
}

// base64 -> decrypted bytes -> string
func (cm *ComputerManager) DecryptString(text string, cryptKey string) (string, error) {
	bytes, err := base64.StdEncoding.DecodeString(text)
	if err != nil {
		return "", err
	}
	result, err := helpers.Decrypt(bytes, cryptKey)
	return string(result), err
}

// Pack info about file into file.
// First 2000 bytes is info about file.
func (cm *ComputerManager) EncryptFile(b []byte, info string, cryptKey string) ([]byte, error) {
	infoBytes := []byte(info)
	emptyBytes := make([]byte, 2000-len(infoBytes))
	fileBytes := []byte{}
	fileBytes = append(fileBytes, infoBytes...)
	fileBytes = append(fileBytes, emptyBytes...)
	fileBytes = append(fileBytes, b...)
	return cm.Encrypt(fileBytes, cryptKey)
}

// First 2000 bytes is info about file.
// Returns file bytes and info about file (string)
func (cm *ComputerManager) DecryptFile(b []byte, cryptKey string) ([]byte, string, error) {
	b, err := cm.Decrypt(b, cryptKey)
	if err != nil {
		return nil, "", err
	}
	infoBytes := b[:2000]
	infoBytes = bytes.Trim(infoBytes, "\x00")
	fileBytes := b[2000:]
	return fileBytes, string(infoBytes), err
}
