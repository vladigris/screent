package models

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"time"
)

type Computer struct {
	Id               int    `json:"id"`
	Guid             string `json:"guid"`
	Name             string `json:"name"`
	Token            string `json:"token"`
	ProgName         string `json:"progName"`
	DataName         string `json:"dataName"`
	ConfName         string `json:"confName"`
	ScreenDir        string `json:"screenDir"`
	LogDir           string `json:"logDir"`
	Interval         int    `sql:",notnull" json:"interval"`
	DayLimit         int    `sql:",notnull" json:"dayLimit"`
	DayLimitFilename string `json:"dayLimitFileName"`

	SerialMb           string    `json:"serialMb"`
	SerialProc         string    `json:"serialProc"`
	SerialHdd          string    `json:"serialHdd"`
	CreatedAt          time.Time `json:"createdAt"`
	Snapshot           string    `json:"snapshot"`
	IsPause            bool      `sql:",notnull" json:"isPause"`
	IsActive           bool      `sql:",notnull" json:"isActive"`
	IsInstalled        bool      `sql:",notnull" json:"isInstalled"`
	IsRemoved          bool      `sql:",notnull" json:"isRemoved"`
	IsRemovedOnDesktop bool      `sql:",notnull" json:"isRemovedOnDesktop"`

	UserId int   `json:"userId"`
	User   *User `json:"user"`

	VersionId int      `json:"versionId"`
	Version   *Version `json:"version"`
}

func (c *Computer) Save() error {
	if c.Id == 0 {
		if err := db.Insert(c); err != nil {
			return err
		}
	} else {
		if err := db.Update(c); err != nil {
			return err
		}
	}
	return nil
}

func (c *Computer) Delete() error {
	if err := db.Delete(c); err != nil {
		return err
	}
	return nil
}

func (c *Computer) RefreshFromDb() {
	updated := new(ComputerManager).GetById(c.Id)
	*c = *updated
}

func (c *Computer) FromJSON(js string) *Computer {
	json.Unmarshal([]byte(js), c)
	return c
}

/*
	Implementation should be same as in Python
	with the same order

	dic['Lang'] = 'en'
    dic['Version'] = self.version.name
    dic['WorkerChecksum'] = self.version.checksum
    dic['WorkerDownloadLink'] = self.version.worker_link
    dic['Ip'] = settings.SERVER_APP_IP
    dic['Ips'] = settings.SERVER_APP_IPS
    dic['Token'] = self.token
    dic['WorkerName'] = self.prog_name
    dic['WorkerDir'] = self.prog_dir
    dic['WorkerConfName'] = self.conf_name
    dic['ScreenDir'] = self.screen_dir
    dic['LogDir'] = self.log_dir
    dic['Interval'] = self.interval
    dic['DayLimit'] = self.day_limit
    dic['DayLimitFileName'] = self.day_limit_filename
    dic['Snapshot'] = self.snapshot
    dic['IsActive'] = self.is_active
    dic['IsPause'] = self.is_pause
    dic['IsRemoved'] = self.is_removed
*/
func (c *Computer) Config() *ComputerConfig {
	version := new(VersionManager).ActiveVersion()

	cfg := &ComputerConfig{
		Lang:               config.Lang,
		Version:            version.Name,
		WorkerCheksum:      version.Checksum(),
		WorkerDownloadLink: version.WorkerDownloadLink(),
		Ip:                 config.ServerAppIp,
		Ips:                config.ServerAppIps,
		Token:              c.Token,
		WorkerName:         c.ProgName,
		WorkerDir:          c.ProgName,
		WorkerConfName:     c.ConfName,
		ScreenDir:          c.ScreenDir,
		LogDir:             c.LogDir,
		Interval:           c.Interval,
		DayLimit:           c.DayLimit,
		DayLimitFileName:   c.DayLimitFilename,
		Snapshot:           c.Snapshot,
		IsActive:           c.IsActive,
		IsPause:            c.IsPause,
		IsRemoved:          c.IsRemoved,
	}
	return cfg
}

// Returns config as XML string
func (c *Computer) XmlConfig() string {
	cfg := c.Config()

	output, err := xml.MarshalIndent(cfg, "  ", "    ")
	checkErr(err)
	return fmt.Sprintf("<root>%s</root>", string(output))
}

// Returns encrypted XML config
func (c *Computer) EncryptedConfig() string {
	encConfig, err := c.EncryptString(c.XmlConfig())
	checkErr(err)
	return encConfig
}

func (c *Computer) Encrypt(bytes []byte) ([]byte, error) {
	version := new(VersionManager).ActiveVersion()
	return new(ComputerManager).Encrypt(bytes, version.CryptKey())
}

func (c *Computer) Decrypt(bytes []byte) ([]byte, error) {
	version := new(VersionManager).ActiveVersion()
	return new(ComputerManager).Decrypt(bytes, version.CryptKey())
}

func (c *Computer) EncryptString(text string) (string, error) {
	version := new(VersionManager).ActiveVersion()
	return new(ComputerManager).EncryptString(text, version.CryptKey())
}

func (c *Computer) DecryptString(text string) (string, error) {
	version := new(VersionManager).ActiveVersion()
	return new(ComputerManager).DecryptString(text, version.CryptKey())
}

func (c *Computer) EncryptFile(b []byte, info string) ([]byte, error) {
	version := new(VersionManager).ActiveVersion()
	return new(ComputerManager).EncryptFile(b, info, version.CryptKey())
}

func (c *Computer) DecryptFile(b []byte) ([]byte, string, error) {
	version := new(VersionManager).ActiveVersion()
	return new(ComputerManager).DecryptFile(b, version.CryptKey())
}
